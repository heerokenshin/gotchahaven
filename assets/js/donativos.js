var size = ['XS','S','M','L','XL','XXL'];
//var sizeM = ['2','4','6','8','10','12'];
var product = ['gotchahavenlogo.png','parche.png','tshirtbalck.png','shirtblack.png'];
var price = [10,60,200,200];

var sal;
var figs = [];

$(document).on('submit','.verificarForm',function(e){
	e.preventDefault();
	var auto = e.target.autorizacion.value;
	if(auto!=''){
		$.ajax({
			url: "bigboss/operations.php",
			type: "POST",
			data:{'op':2,'id':auto},
			dataType:'json',
			success: function(res){
				if(res.length>0){
					if(res[0].estado=='Activo'){
						sal = res[0].monto;
						$('#saldo').text(sal);
						$('.donativoForm #autorizacion').val(auto);
						$('.verificarForm .text-danger').text('');

						$('.formsCont').addClass('fadeUp');
						$(e.currentTarget).addClass('fadeSlideRight');
					}else $('.verificarForm .text-danger').text('Éste numero ya realizó su pedido.');
				}else $('.verificarForm .text-danger').text('Número no encontrado, favor de confirmarlo...');			
			},
			error: function(err){
				console.log(err);
				$('.verificarForm .text-danger').text('Número no encontrado, favor de confirmarlo...');
			}
		});
	}else{
		/*var $p = $('<p class="help-block text-danger">');
		$p.append('<ul role="alert"><li>Favor de ingresar su número para verificación...</li></ul>');
		$(e.currentTarget).append($p);*/
		$('.verificarForm .text-danger').text('Favor de ingresar su número para verificación...');
	}
});

$(document).on('click','.agregarMerch',function (e) {
	e.preventDefault();
	var $m = $('#mercancia option:selected');

	if(sal>=price[$m.val()]){
		sal = sal - price[$m.val()];
		$('#saldo').text(sal);

		var $figure = $('<figure class="slideLeft">');
		$figure.append('<input type="hidden" name="producto[]" id="producto" value="'+$m.text()+'">');
		$figure.append('<i class="quitar glyphicon glyphicon-remove"></i>');
		$figure.append('<img src="img/'+product[$m.val()]+'">');
		var $aside = $('<aside>'+$m.text()+'</aside>');
		if($m.val()>1){
			var opt;
			for(var i=0; i<size.length;i++)opt= opt+'<option value="'+size[i]+'">'+size[i]+'</option>';
			$aside.append('<select class="form-control" name="talla[]" id="talla">'+opt+'</select>');
		}
		var $qty = $('<div class="qty"><input id="'+price[$m.val()]+'" name="cant[]" class="cant" type="number" min="1" value="1"> X $'+price[$m.val()]+' MXN</div>');
		$aside.append($qty);
		$figure.append($aside);
		$('.articles').append($figure);

		$('.articles .text-danger').remove();
		figs.push(1);

	}else{
		var $p = $('<p class="help-block text-danger">');
		$p.append('<ul role="alert"><li>Saldo insuficiente, elige otro producto.</li></ul>');
		$('.articles').append($p);
	}
});

$(document).on('click','.service-heading button',function(e){
	$('.service-heading button').each(function(){$(this).removeClass('selected')});
	$(e.currentTarget).addClass('selected');
	$('.teesSlider').css('margin-left','-'+$(e.currentTarget).attr('id')+'%');
});

$(document).on('click','.quitar',function(e){
	var $i = $(e.currentTarget).parent().find('input.cant');
	sal = parseInt($i.val()*$i.attr('id'))+sal;
	$('#saldo').text(sal);
	$(e.currentTarget).parent().remove();
	if(sal>=0)$('.articles .text-danger').remove();

	figs.splice($(e.currentTarget).parent().index());
});

$(document).on('change','.cant',function(e){
	var ind = $(e.currentTarget).parent().parent().parent().index();
	var oldInc = figs[ind];
	var c = parseInt($(e.currentTarget).attr('id'));
	var i = $(e.currentTarget).val();

	sal = sal-(c*((i>=oldInc)?i-oldInc:i-oldInc));

	if(sal>=0){
		$('#saldo').text(sal);
		$('.articles .text-danger').remove();
	}else{
		$('.articles .text-danger').remove();
		var $p = $('<p class="help-block text-danger">');
		$p.append('<ul role="alert"><li>Saldo insuficiente, reduzca la cantidad del producto.</li></ul>');
		$('.articles').append($p);
		$(e.currentTarget).val(i-1);
		sal = sal + c;
		i--;
	}
	figs[ind] = i;
});

$(document).on('click','.limpiar',function(e){
	e.preventDefault();
	$('.donativoForm')[0].reset();
	for(var i=$('figure.slideLeft').length-1; i>=0; i--){
		var f = $('figure.slideLeft').get(i);
		var cant = $(f).find('input').attr('id');
		sal = sal + (cant*parseInt(figs[i]));
		figs.splice(i);
	}
	$('#saldo').text(sal);
});

$(document).on('click','.cancelar',function(e){
	e.preventDefault();
	window.location.href='index.php';
});

$(document).on('click','.back',function(){
	$('.formsSlide').css('margin-left','0');
});

$(document).on('click','.checkout',function(e){
	e.preventDefault();
	var dta = $('.donativoForm').serialize()+'&total='+$('.ELTOTAL').text();
	//AQUI ME QUEDE.. VER COMO MEDIFICAR EL OP DE SERIALIZE
	$.ajax({
		url: "bigboss/operations.php",
		type: "POST",
		data:dta,
		success: function(res){
			var p = [], c = [], t = [];
			$('.donativoForm input[name="producto[]"]').each(function(){p.push($(this).val());});
			$('.donativoForm input[name="cant[]"]').each(function(){c.push($(this).val());});
			$('.donativoForm select[name="talla[]"]').each(function(){t.push($(this).val());});
			$.ajax({
				url: "bigboss/operations.php",
				type: "POST",
				data:{'op':4,'desc':p,'cant':c,'talla':t,'auto':$('.donativoForm #autorizacion').val()},
				success: function(res){
					var auto = $('.donativoForm #autorizacion').val();
					$('.formsCont').html('');
					$('.formsCont').append('<h2 class="responseText fadeUp">¡ENHORABUENA!<br><br><p>gotchahaven.com agradece profundamente tu donativo, con tu contribución estamos más cerca de crear el sitio ideal para el gotchero y airsoftero!<br>Pronto tendremos noticias para ti!</p><br><button id="'+auto+'" class="closeAuto btn btn-xl">ACEPTAR</button></h2>');
				},
				error: function(err){
					console.log(err);
					$.ajax({
						url: "bigboss/operations.php",
						type: "POST",
						data:{'op':5,'auto':$('.donativoForm #autorizacion').val()},
						success: function(res){
							console.log(res);
							$('.formsCont').html('');
							$('.formsCont').append('<h3 class="responseText">Parece que tenemos problemas con el servidor.<br>Sugerimos volver a intentar más tarde...<br><a href="index.php" class="btn btn-xl">ACEPTAR</a></h3>');
						},
						error: function(err){
							console.log(err);
							$('.formsCont').html('');
							$('.formsCont').append('<h3 class="responseText">Parece que tenemos problemas con el servidor.<br>Sugerimos volver a intentar más tarde...<br><a href="index.php" class="btn btn-xl">ACEPTAR</a></h3>');
						}
					});
					$('.formsCont').html('');
					$('.formsCont').append('<h3 class="responseText">Parece que tenemos problemas con el servidor.<br>Sugerimos volver a intentar más tarde...<br><a href="index.php" class="btn btn-xl">ACEPTAR</a></h3>');
				}
			});
		},
		error: function(err){
			console.log(err);
			$('.formsCont').html('');
			$('.formsCont').append('<h3 class="responseText">Parece que tenemos problemas con el servidor.<br>Sugerimos volver a intentar más tarde...<br><a href="index.php" class="btn btn-xl">ACEPTAR</a></h3>');
		}
	});
});

$(document).on('click','.closeAuto',function(e){
	e.preventDefault();
	$.ajax({
		url: "bigboss/operations.php",
		type: "POST",
		data:{'op':6,'auto':$(e.currentTarget).attr('id')},
		success: function(res){
			window.location.href="index.php";
		},
		error: function(err){
			console.log(err);
			window.location.href="index.php";
		}
	});
});