var compras = compras || {};

compras.main = (function() {

  return {
  	mostrarFormularioContacto:function(){
  		$(document).on('click','.contact',function(e){
  			e.preventDefault();
  			var $d = $(e.currentTarget).parent().parent().find('.anyForm');
  			var $form = $('<form id="contactForm" name="contactForm" novalidate>');
        var data = $(e.currentTarget).attr('id').split('_');
  			$form.append('<h5 style="margin-top:0;"><b>Para: </b> '+data[0]+'</h5>');
  			$form.append('<input type="hidden" name="to" id="to" value="'+data[0]+'">');
        $form.append('<input type="hidden" name="whatsapp" id="whatsapp" value="'+data[1]+'">');
  			$form.append('<input type="hidden" name="from" id="from" value="'+$('#myMail').val()+'">');
  			$form.append('<div class="form-group" style="margin-bottom:4px;"><textarea class="from-control" name="subject" id="subject" cols="30" rows="3" required data-validation-required-message="Escriba su mensaje."></textarea><p class="help-block text-danger"></p></div>');
  			$form.append('<button type="submit" class="btn btn-primary">Enviar <i class="glyphicon glyphicon-send"></i></button>');
        $form.append('<button class="btn btn-cancel">Cancelar <i class="glyphicon glyphicon-remove"></i></button>');
        $d.append($form);
        $d.addClass('showingAnyForm');
  		});
  	},
    mostrarFormularioCancelar:function(){
      $(document).on('click','.cancel',function(e){
        e.preventDefault();
        var $d = $(e.currentTarget).parent().parent().find('.anyForm');
        var $form = $('<form id="cancelForm" name="cancelForm" novalidate>');
        var data = $(e.currentTarget).attr('id').split('_');
        $form.append('<h5><b>Motivo para cancelar:</b></h5>');
        $form.append('<input type="hidden" name="to" id="to" value="'+data[0]+'">');
        $form.append('<input type="hidden" name="whatsapp" id="whatsapp" value="'+data[1]+'">');
        $form.append('<input type="hidden" name="from" id="from" value="'+$('#myMail').val()+'">');
        $form.append('<div class="form-group"><select class="form-control" name="reason" id="reason"><option value="0">Se me dificulta pagarlo</option><option value="1">Encontré otra opción</option><option value="2">Ya no lo quiero</option></select></div>');
        $form.append('<button type="submit" class="btn btn-primary">Procesar <i class="glyphicon glyphicon-refresh"></i></button>');
        $form.append('<button class="btn btn-cancel">Volver <i class="glyphicon glyphicon-arrow-right"></i></button>');
        $d.append($form);
        $d.addClass('showingAnyForm');
      });
    },
    cancelarFormulario:function(){
      $(document).on('click','#contactForm .btn-cancel, #cancelForm .btn-cancel',function(e){
        e.preventDefault();
        var $div = $(e.currentTarget).parent().parent();
        $div.removeClass('showingAnyForm');
        setTimeout(function(){$div.html('');},100);
      });
    }
  }
})();