var solicitud = solicitud || {};

solicitud.main = (function() {

  return {
  	mostrarFormularioContacto:function(){
  		$(document).on('click','.storeTable .btn-primary,.storeList .btn-primary',function(e){
        $('.contactForm #plan').val($(e.currentTarget).attr('id'));
        $('.lastStep').css('display','block');
        $('.lastStep').addClass('fadeUp');
        var t = 150;
        if(t<481) t = 420;
        else if(t>480&&t<768) t = 100;
        $('html, body').stop().animate({
          scrollTop: ($('#contactForm').offset().top - t)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
  		});
  	}
   
  }
})();