var tienda = tienda || {};

tienda.main = (function() {

  return {
    prepararFormulario:function(){
      $(document).ready(function(){
        Dropzone.autoDiscover = false;
        $("#dZUploadLogo").dropzone({
            url: "uploadLogo",
            tagId:'nImages',
            acceptedFiles: "image/jpeg,image/png,image/jpg",
            maxFiles:1,
            thumbnailWidth: 380,
            addRemoveLinks: true,
            removedfile: function(file){
              $('.tiendaForm #logo').text($('.tiendaForm #logo').text().replace('/'+file.name.replace(/\s/g,'_'),''));
              $('.editTiendaForm #logo').text($('.editTiendaForm #logo').text().replace('/'+file.name.replace(/\s/g,'_'),''));
              $.ajax({
                  url: "removeUploaded",
                  type: "POST",
                  data:{'img':file.name.replace(/\s/g,'_'),'dir':'./assets/img/tiendas/'},
                  success: function(res){
                      var _ref;
                  if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                      _ref.parentNode.removeChild(file.previewElement);
                    }
                  }
                  return this._updateMaxFilesReachedClass();
                  },
                  error: function(err){
                    console.log(err);
                  }
              });
            },
            success: function (file, response) {
                var imgName = response;
                file.previewElement.classList.add("dz-success");
                $('.tiendaForm #logo').text($('.tiendaForm #logo').text()+'/'+imgName.replace(/\s/g,'_'));
                $('.editTiendaForm #logo').text($('.editTiendaForm #logo').text()+'/'+imgName.replace(/\s/g,'_'));
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
      });
    },
  	abrirTiendaForm:function(){
  		$(document).on('click','.abrirTiendaForm',function(e){
        $(e.currentTarget).css('display','none');
        $('#tiendaForm').css('display','block');
        $('#tiendaForm').addClass('fadeUp');
        $('html, body').stop().animate({
          scrollTop: ($('#tiendaForm').offset().top - 150)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
  		});
  	},
    procesarTienda:function(){
      $("#tiendaForm input,#tiendaForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            if($('.tiendaForm #logo').text()!=''){
              var dta = $('.tiendaForm').serialize()+'&logo='+$('.tiendaForm #logo').text();

              $.ajax({
                  url: "crearTienda",
                  type: "POST",
                  data:dta,
                  success: function(res){
                    $('#successTienda').html("<div class='alert alert-success'>");
                    $('#successTienda > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#successTienda > .alert-success').append("<strong>Haz creado tu tienda satisfactoriamente!</strong>");
                    $('#successTienda > .alert-success').append('</div>');
                    /*refreshProfile(dta);
                    $('.perfilCard').css('display','block');
                    $('.perfilForm').css('display','none');*/
                  },
                  error: function(err){
                    console.log(err);
                    $('#successTienda').html("<div class='alert alert-danger'>");
                      $('#successTienda > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#successTienda > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                      $('#successTienda > .alert-danger').append('</div>');
                      //clear all fields
                      $('#tiendaForm').trigger("reset");
                  }
              });
          }else{
            $('#successTienda').html("<div class='alert alert-danger'>");
            $('#successTienda > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
              .append("</button>");
            $('#successTienda > .alert-danger').append("<strong>Falta agregar el logo de tu tienda...");
            $('#successTienda > .alert-danger').append('</div>');
          }
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
    },
    cancelarFormularioTienda:function(){
      $(document).on('click','.tiendaForm .btn-cancel',function(e){
        e.preventDefault();
        $('.tiendaForm')[0].reset();
        Dropzone.forElement('#dZUploadLogo').removeAllFiles(true);
        $('.abrirTiendaForm').css('display','block');
        $('.tiendaForm').css('display','none');
      });
      $(document).on('click','.editTiendaForm .btn-cancel',function(e){
        e.preventDefault();
        $('.editTiendaForm')[0].reset();
        Dropzone.forElement('#dZUploadLogo').removeAllFiles(true);
        $('.tiendaData').css('display','block');
        $('.tiendaData').addClass('fadeUp');
        $('.editTiendaForm').css('display','none');
        $('.editTiendaForm').removeClass('fadeUp');
      });
    },
    limpiarFormularioTienda:function(){
      $(document).on('click','.tiendaForm .reset',function(e){
        e.preventDefault();
        $('.tiendaForm')[0].reset();
        Dropzone.forElement('#dZUpload').removeAllFiles(true);
      });
      $(document).on('click','.editTiendaForm .reset',function(e){
        e.preventDefault();
        $('.editTiendaForm')[0].reset();
        Dropzone.forElement('#dZUpload').removeAllFiles(true);
      });
    },
    refreshTienda:function(){
      $(document).on('click','#successTienda .alert-success .close,#successEditTienda .alert-success .close',function(){
        window.location.reload();
      });
    },
    abrirEditTiendaForm:function(){
      $(document).on('click','.editTienda',function(e){
        $('.tiendaData').css('display','none');
        $('.tiendaData').removeClass('fadeUp');
        $('.editTiendaForm').css('display','block');
        $('.editTiendaForm').addClass('fadeUp');
        $('html, body').stop().animate({
          scrollTop: ($('#editTiendaForm').offset().top - 150)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
      });
    },
    preEliminarImagenEditar:function(){
      $(document).on('click','.deleteLogo',function(e){
        e.preventDefault();
        $(e.currentTarget).parent().html('Proceder?<br> <button class="yes btn btn-wildcard">SI</button><button class="no btn btn-wildcard">NO</button>');
      });
      $(document).on('click','.editTiendaForm figcaption .no',function(e){
        e.preventDefault();
        $(e.currentTarget).parent().html($('.editTiendaForm #oldLogo').val().replace('/','')+'<button class="preEliminarImg btn btn-wildcard">Eliminar <i class="glyphicon glyphicon-trash"></i></button>');
      });
    },
    eliminarImagenVieja:function(){
      $(document).on('click','.editTiendaForm figcaption .yes',function(e){
        e.preventDefault();
        var $img = $(e.currentTarget).parent().parent().find('img');      
        $img.parent().remove();
      });
    },
    procesarEditTienda:function(){
      $("#editTiendaForm input,#editTiendaForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
              var dta = $('.editTiendaForm').serialize()+'&logo='+$('.editTiendaForm #logo').text();

              $.ajax({
                  url: "editarTienda",
                  type: "POST",
                  data:dta,
                  success: function(res){
                    $('#successEditTienda').html("<div class='alert alert-success'>");
                    $('#successEditTienda > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#successEditTienda > .alert-success').append("<strong>Se ha editado tu tienda satisfactoriamente!</strong>");
                    $('#successEditTienda > .alert-success').append('</div>');
                  },
                  error: function(err){
                    console.log(err);
                    $('#successEditTienda').html("<div class='alert alert-danger'>");
                      $('#successEditTienda > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#successEditTienda > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                      $('#successEditTienda > .alert-danger').append('</div>');
                      //clear all fields
                      $('#editTiendaForm').trigger("reset");
                  }
              });
          
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
    },
    cambiarEstadoProducto:function(){
      $(document).on('click','.controls .btn-cancel, .controls .btn-success',function(e){
        var st = ($(e.currentTarget).hasClass('btn-success'))?'VENTA':'DESHABILITADO';
        var id = $(e.currentTarget).attr('id');
        var qty = $(e.currentTarget).parent().parent();
        $(qty).find('.qty').html('<h4>Proceder?</h4><button id="'+id+'-'+st+'" class="si btn btn-wildcard">SI</button><button class="no btn btn-wildcard">NO</button>');
        
      });
      $(document).on('click','.qty .no',function(e){$('.qty').html('');});
      $(document).on('click','.qty .si',function(e){
        var dta = $(e.currentTarget).attr('id').split('-');
        $.ajax({
          url: 'cambiarEstadoTransaccion',
          type: "POST",
          data:{'id_transaccion':dta[0],'estado':dta[1]},
          success: function(res){
            $('.qty').html('<h4>Cambio realizado al producto!</h4>');
            setTimeout(function(){window.location.reload();},500);
          },
          error: function(err){
            console.log(err);
            $('.qty').html('<h4>No se pudo realizar el cambio, intenta de nuevo...</h4>');
          }
        });
      });
    },
    abrirExistenciaForm:function(){
      $(document).on('click','.controls .btn-primary',function(e){
        var qty = $(e.currentTarget).parent().parent();
        var dta = $(e.currentTarget).attr('id').split('-');
        $(qty).find('.qty').html('<form class="qtyForm" id="qtyForm" enctype="multipart/form-data" novalidate><input type="hidden" id="id_tienda" name="id_tienda" value="'+dta[0]+'"><input type="hidden" id="id_producto" name="id_producto" value="'+dta[1]+'"><h4>Cantidad nueva:</h4><input type="number" id="existencia" name="existencia" placeholder="Cantidad" value="<?php echo $p->existencia;?>" required><button type="submit" class="btn btn-primary">Actualizar</button></form>');
      });
    },
    cambiarExistencia:function(){
      $(document).on('submit','.qtyForm',function(e){
        e.preventDefault();
        $.ajax({
          url: 'cambiarExistencia',
          type: "POST",
          data:$(e.currentTarget).serialize(),
          success: function(res){
            $('.qty').html('<h4>Cambio realizado al producto!</h4>');
            setTimeout(function(){window.location.reload();},500);
          },
          error: function(err){
            console.log(err);
            $('.qty').html('<h4>No se pudo realizar el cambio, intenta de nuevo...</h4>');
          }
        });
      });
    },
    changePlan:function(){
      $(document).on('click','.changePlan',function(e){
        $('.anyForm').addClass('showingAnyForm');
        abrirPlanForm($('.anyForm'));
        $('html, body').stop().animate({
          scrollTop: ($('#planForm').offset().top - 150)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
      });
    },
    cancelarChangePlan:function(){
      $(document).on('click','#planForm .btn-cancel',function(e){
        e.preventDefault();
        var $div = $(e.currentTarget).parent().parent();
        $div.removeClass('showingAnyForm');
        setTimeout(function(){$div.html('');},100);
      });
    },
    enviarSolicitudPlan:function(){
      $(document).on('submit','#planForm',function(e){
        e.preventDefault();
        var $div = $(e.currentTarget).parent();
        var dta = $(e.currentTarget).serialize();
        $.ajax({
          url: 'enviarSolicitudPlan',
          type: "POST",
          data:dta,
          success: function(res){
            $div.html('<h6>Solicitud enviada a Gotcha Haven!</h6>');
            setTimeout(function(){$div.removeClass('showingAnyForm');},2000);
          },
          error: function(err){
            console.log(err);
            $div.html('<h6>No se pudo enviar la solicitud, intenta de nuevo...</h6>');
            setTimeout(function(){$div.removeClass('showingAnyForm');},2000);
          }
        });

      });
    }
   
  }
})();

function abrirPlanForm($d){
  $d.html('');
  var $form = $('<form id="planForm" name="planForm" novalidate>');
  $form.append('<input type="hidden" id="id_usuario" name="id_usuario" value="'+$('.editTiendaForm #id_usuario').val()+'">');
  $form.append('<input type="hidden" id="planActual" name="planActual" value="'+$('.tiendaData').attr('id')+'">');
  $form.append('<div class="form-group"><h5>Plan deseado</h5><select name="planNuevo" id="planNuevo" class="form-control"><option value="60">Cabo</option><option value="200">Sargento 2do</option><option value="450">Teniente</option></select></div>');
  $form.append('<div class="form-group"><textarea name="motivo" id="motivo" cols="30" rows="3" class="form-control" placeholder="Motivo del cambio..." required></textarea></div>');
  $form.append('<button type="submit" class="btn btn-primary">Solicitar cambio <i class="glyphicon glyphicon-transfer"></i></button>');
  $form.append('<button class="btn btn-cancel">Volver <i class="glyphicon glyphicon-arrow-right"></i></button>');
  $d.append($form);
}