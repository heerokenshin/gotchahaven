var by = 'RECIENTE';
var ventas = ventas || {};

ventas.main = (function() {

  return {
  	prepararDropzone:function(){
  		$(document).ready(function(){
        refreshProducts(by);

  			Dropzone.autoDiscover = false;
		    $("#dZUpload").dropzone({
		        url: "upload",
            tagId:'nImages',
            acceptedFiles: "image/jpeg,image/png,image/jpg",
            maxFiles:10,
		        addRemoveLinks: true,
		        removedfile: function(file){
		        	$('.productoForm #archivo').text($('.productoForm #archivo').text().replace('/'+file.name.replace(/\s/g,'_'),''));
		        	$.ajax({
			            url: "removeUploaded",
			            type: "POST",
			            data:{'img':file.name.replace(/\s/g,'_'),'dir':'./assets/img/products/'},
			            success: function(res){
			              	var _ref;
					        if (file.previewElement) {
					          if ((_ref = file.previewElement) != null) {
					            _ref.parentNode.removeChild(file.previewElement);
					          }
					        }
					        return this._updateMaxFilesReachedClass();
			            },
			            error: function(err){
			              console.log(err);
			            }
			        });
		        },
		        success: function (file, response) {
		            var imgName = response;
		            file.previewElement.classList.add("dz-success");
		            $('.productoForm #archivo').text($('.productoForm #archivo').text()+'/'+imgName.replace(/\s/g,'_'));
		        },
		        error: function (file, response) {
		            file.previewElement.classList.add("dz-error");
		        }
		    });

        $("#dZVideo").dropzone({
            url: "upload",
            acceptedFiles: "video/mp4",
            maxFiles:1,
            tagId:'nVideo',
            dictRemoveFile: 'Quitar video X',
            addRemoveLinks: true,
            removedfile: function(file){
              $.ajax({
                  url: "removeUploaded",
                  type: "POST",
                  data:{'img':file.name.replace(/\s/g,'_'),'dir':'./assets/img/products/video/'},
                  success: function(res){
                      $('.productoForm #video').val('');
                      var _ref;
                  if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                      _ref.parentNode.removeChild(file.previewElement);
                    }
                  }
                  return this._updateMaxFilesReachedClass();
                  },
                  error: function(err){
                    console.log(err);
                  }
              });
            },
            success: function (file, response) {
                var vidName = response;
                file.previewElement.classList.add("dz-success");
                var $vid = $('<video width="100%" height="auto" controls>');
                $vid.append('<source src="../../assets/img/products/video/'+vidName.replace(/\s/g,'_')+'" type="video/mp4">');
                $('#dZVideo .dz-image').append($vid);
                $('.productoForm #video').val(vidName.replace(/\s/g,'_'));
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
  		});
  	},
  	quitarNombreImagen:function(){
  		$(document).on('click','#dZUpload .dz-remove',function(e){
  			$('.productoForm #archivo').text($('.productoForm #archivo').text().replace('/'+$(e.currentTarget).attr('id').replace(/\s/g,'_'),''));
  		});
      $(document).on('click','#dZUploadEdit .dz-remove',function(e){
        $('.editProdForm #archivo').text($('.editProdForm #archivo').text().replace('/'+$(e.currentTarget).attr('id').replace(/\s/g,'_'),''));
      });
  	},
    quitarNombreVideo:function(){
      $(document).on('click','#dZVideo .dz-remove',function(e){
        $('.productoForm #video').val('');
      });
      $(document).on('click','#dZVideoEdit .dz-remove',function(e){
        $('.editProdForm #video').val('');
      });
    },
  	limpiarForm:function(){
  		$(document).on('click','.productoForm .reset',function(e){
  			e.preventDefault();
  			$('.productoForm')[0].reset();
  			Dropzone.forElement('#dZUpload').removeAllFiles(true);
        Dropzone.forElement('#dZVideo').removeAllFiles(true);
  		});
  		$(document).on('click','.productoForm .btn-cancel',function(e){
  			e.preventDefault();
  			$('.productoForm')[0].reset();
  			Dropzone.forElement('#dZUpload').removeAllFiles(true);
        Dropzone.forElement('#dZVideo').removeAllFiles(true);
  			$('.prodCont').removeClass('fadeUp').addClass('blurUp');
	        setTimeout(function(){$('.prodCont').css('display','none');},500);
	        $('.subirProducto').css('opacity','1');
  		});
      $(document).on('click','.editProdForm .btn-cancel',function(e){
        e.preventDefault();
        Dropzone.forElement('#dZUploadEdit').removeAllFiles(true);
        Dropzone.forElement('#dZVideoEdit').removeAllFiles(true);
        var $row = $(e.currentTarget).parent().parent().parent().parent().parent();
        $row.find('.vedProducto').css('display','block');
        $row.find('.vedTransaccion').css('display','block');
        $('.editFormCont').html('');
        $('#eImages').remove();
        $('#eVideo').remove();
      });
      $(document).on('click','.editProdForm .reset',function(e){
        e.preventDefault();
        $('.editProdForm')[0].reset();
        Dropzone.forElement('#dZUploadEdit').removeAllFiles(true);
        Dropzone.forElement('#dZVideoEdit').removeAllFiles(true);
      });
  	},
  	abrirProductoForm:function(){
  		$(document).on('click','.subirProducto',function(){
  			$('.prodCont').css('display','block');
  			$('.prodCont').removeClass('blurUp').addClass('fadeUp');
	        $('.subirProducto').css('opacity','0');
  		});
  	},
    cambiarSubcategoria:function(){
      $(document).on('change','.productoForm #id_categoria, .editProdForm #id_categoria',function(e){
        var $par = $(e.currentTarget).parent().parent().parent();
        $.ajax({
          url: "cambiarSubcategoria",
          type: "POST",
          data:{id:$(e.currentTarget).val()},
          dataType: 'json',
          success: function(res){
            $('.'+$par.attr('class')+' #id_subcatego').html('');
            $.each(res,function(i,item){
              $('.'+$par.attr('class')+' #id_subcatego').append('<option value="'+item.id_subcatego+'">'+item.nom_subcatego+'</option>');
            });
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
  	procesarProductoFormulario:function(){
  		$("#productoForm input,#productoForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            if($('.productoForm #archivo').text()!=''){
              var dta = $('.productoForm').serialize()+'&archivo='+$('.productoForm #archivo').text();

              $.ajax({
                  url: "subirProducto",
                  type: "POST",
                  data:dta,
                  success: function(res){
                    $('#successProducto').html("<div class='alert alert-success'>");
                    $('#successProducto > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                    $('#successProducto > .alert-success').append("<strong>Se subido tu producto satisfactoriamente!</strong>");
                    $('#successProducto > .alert-success').append('</div>');
                    refreshProducts(by);
                    /*refreshProfile(dta);
                    $('.perfilCard').css('display','block');
                    $('.perfilForm').css('display','none');*/
                  },
                  error: function(err){
                    console.log(err);
                    $('#successProducto').html("<div class='alert alert-danger'>");
                      $('#successProducto > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#successProducto > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                      $('#successProducto > .alert-danger').append('</div>');
                      //clear all fields
                      $('#productoForm').trigger("reset");
                  }
              });
          }else{
            $('#successProducto').html("<div class='alert alert-danger'>");
            $('#successProducto > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
              .append("</button>");
            $('#successProducto > .alert-danger').append("<strong>Falta agregar al menos una foto o imagen al producto...");
            $('#successProducto > .alert-danger').append('</div>');
          }
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
  	},
  	cerrarFormularioProducto:function(){
      $(document).on('click','#successProducto .alert-success .close',function(e){
        e.preventDefault();
        $('.productoForm')[0].reset();
        $('#dZUpload .dz-success').each(function(){$(this).remove()});
        $()
        $('#dZUpload .dz-default.dz-message').css('display','block');
        $('.prodCont').removeClass('fadeUp').addClass('blurUp');
          setTimeout(function(){$('.prodCont').css('display','none');},500);
          $('.subirProducto').css('opacity','1');
      });
      $(document).on('click','#successEditarProducto .alert-success .close',function(e){
        e.preventDefault();
        $('.editFormCont').html('');
        $('#eImages').remove();
        $('#eVideo').remove();
        refreshProducts(by);
      });
    },
    abrirEditarProductoForm:function(){
      $(document).on('click','.btn-editOrange',function(e){
        e.preventDefault();
        var p = $(e.currentTarget).attr('id').split('-');
        var $par = $(e.currentTarget).parent().parent().parent();
        if(p[1]=='VENTA'||p[1]=='DESHABILITADO'){
          $.ajax({
            url: 'abrirEditProdForm',
            type: "POST",
            data:{'id':p[0],'myID':$('#myID').val()},
            success: function(res){
              $par.find('.vedProducto').css('display','none');
              $par.find('.vedTransaccion').css('display','none');
              $par.find('.editFormCont').html(res);
            },
            error: function(err){
              console.log(err);
            }
          });
        }else{
          $(e.currentTarget).parent().parent().append('<h6>No puedes editar debido a que el producto se encuentra en proceso de ser vendido...</h6>');
        }
      });
    },
    preEliminarImagenEditar:function(){
      $(document).on('click','.preEliminarImg',function(e){
        e.preventDefault();
        $(e.currentTarget).parent().html('Proceder?<br> <button class="yes btn btn-wildcard">SI</button><button class="no btn btn-wildcard">NO</button>');
      });
      $(document).on('click','.oldImages figcaption .no',function(e){
        e.preventDefault();
        $(e.currentTarget).parent().html('<button class="preEliminarImg btn btn-wildcard">Eliminar <i class="glyphicon glyphicon-trash"></i></button>');
      });
    },
    eliminarImagenVieja:function(){
      $(document).on('click','.oldImages figcaption .yes',function(e){
        e.preventDefault();
        var $img = $(e.currentTarget).parent().parent().find('img');      
        $img.parent().remove();
        var old = $('.editProdForm #oldArchivo').val().replace('/'+$img.attr('alt'),'');
        $('.editProdForm #oldArchivo').val(old);
        var tobe = $('.editProdForm #toBeRemoved').val();
        $('.editProdForm #toBeRemoved').val(tobe+'/'+$img.attr('alt'));
      });
    },
    preEliminarVideoViejo:function(){
      $(document).on('click','.preRemoveOldVid',function(e){
        $(e.currentTarget).parent().html('Proceder? <button class="yes btn btn-confirmOrange">SI</button><button class="no btn btn-confirmOrange">NO</button>');
      });
      $(document).on('click','.oldVideo .no',function(e){
        e.preventDefault();
        $(e.currentTarget).parent().html('<span class="preRemoveOldVid">Quitar video X</span>');
      });
    },
    eliminarVideoViejo:function(){
      $(document).on('click','.oldVideo .yes',function(e){
        e.preventDefault();
        $('.editProdForm .oldVideo').remove();
      });
    },
    cambiarEstadoTransaccion:function(){
      $(document).on('click','.vedTransaccion .deshabilitar',function(e){
        e.preventDefault();
        abrirEstadoFormCont($(e.currentTarget).parent().parent().find('.anyForm'),$(e.currentTarget).attr('id'),'DESHABILITADO',null);
      });
      $(document).on('click','.vedTransaccion .habilitar',function(e){
        e.preventDefault();
        abrirEstadoFormCont($(e.currentTarget).parent().parent().find('.anyForm'),$(e.currentTarget).attr('id'),'VENTA',null);
      });
      $(document).on('click','.vedTransaccion .retirar',function(e){
        e.preventDefault();
        var dta = $(e.currentTarget).attr('id').split('-');
        abrirEstadoFormCont($(e.currentTarget).parent().parent().find('.anyForm'),dta[1],'CANCELADO',dta[0]);
      });
      $(document).on('click','.vedTransaccion .paqueteria',function(e){
        e.preventDefault();
        abrirEstadoFormCont($(e.currentTarget).parent().parent().find('.anyForm'),$(e.currentTarget).attr('id'),'EN CAMINO',null);
      });
      $(document).on('submit','#stateForm',function(e){
        console.log('YO!')
        e.preventDefault();
        var dta = $(e.currentTarget).serialize();
        $.ajax({
          url: 'cambiarEstadoTransaccion',
          type: "POST",
          data:dta,
          success: function(res){
            var $div = $(e.currentTarget).parent();
            $div.removeClass('showingAnyForm');
            setTimeout(function(){$div.html('');},100);
            refreshProducts(by);
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    cancelarEstadoTransaccion:function(){
      $(document).on('click','#stateForm .btn-cancel,#compradorForm .btn-cancel',function(e){
        e.preventDefault();
        var $div = $(e.currentTarget).parent().parent();
        $div.removeClass('showingAnyForm');
        setTimeout(function(){$div.html('');},100);
      });
    },
    abrirContactarComprador:function(){
      $(document).on('click','.vedTransaccion .btn-default',function(e){
        e.preventDefault();
        var $d = $(e.currentTarget).parent().parent().find('.anyForm');
        var $form = $('<form id="compradorForm" name="compradorForm" novalidate>');
        $form.append('<h6>Contacto al comprador</h6>');
        $form.append('<input type="hidden" id="id_vendedor" name="id_vendedor" value="'+$('#myID').val()+'">');
        $form.append('<input type="hidden" id="id_comprador" name="id_comprador" value="'+$(e.currentTarget).attr('id')+'">');
        $form.append('<div class="form-group"><textarea name="asunto" id="asunto" cols="30" rows="3" class="form-control" placeholder="Escriba el asunto al comprador..." required></textarea></div>');
        $form.append('<button type="submit" class="btn btn-primary">Enviar <i class="glyphicon glyphicon-send"></i></button>');
        $form.append('<button class="btn btn-cancel">Volver <i class="glyphicon glyphicon-arrow-right"></i></button>');
        $d.append($form);
        $d.addClass('showingAnyForm');
      });
    },
    enviarMensajeComprador:function(){
      $(document).on('submit','#compradorForm',function(e){
        e.preventDefault();
        var $div = $(e.currentTarget).parent();
        var dta = $(e.currentTarget).serialize();
        $.ajax({
          url: 'enviarMensajeComprador',
          type: "POST",
          data:dta,
          success: function(res){
            $div.html('<h6>Mensaje enviado a comprador!</h6>');
            setTimeout(function(){$div.removeClass('showingAnyForm');},1000);
          },
          error: function(err){
            console.log(err);
            $div.html('<h6>No se pudo enviar mensaje, intenta de nuevo...</h6>');
            setTimeout(function(){$div.removeClass('showingAnyForm');},1000);
          }
        });
      });
    },
    cambiarOrdenamiento:function(){
      $(document).on('change','#prodSortBy',function(e){
        by = $(e.currentTarget).val();
        refreshProducts(by);
      });
    }

  }
})();

function refreshProducts(by){
  $('.prodList').html('');
  /*var id = ($('.productoForm #id_tienda').length>0)?$('.productoForm #id_tienda').val():$('.productoForm #id_vendedor').val();
  var meth = ($('.productoForm #id_tienda').length>0)?'productosTienda':'productosUsuario';*/
  var id = $('.productoForm #id_vendedor').val(); var meth = 'productosUsuario';
  $.ajax({
    url: meth,
    type: "POST",
    data:{'id':id,'sort':by},
    success: function(res){
      $('.prodList').html(res);
    },
    error: function(err){
      console.log(err);
    }
  });
}

function abrirEstadoFormCont($d,id,state,idcomprador){
  var $form = $('<form id="stateForm" name="stateForm" novalidate>');
  if(state=='DESHABILITADO'||state=='VENTA')$form.append('<h6>Proceder con el cambio?</h6>');
  $form.append('<input type="hidden" id="id_transaccion" name="id_transaccion" value="'+id+'">');
  $form.append('<input type="hidden" id="estado" name="estado" value="'+state+'">');
  switch(state){
    case 'DESHABILITADO':
      $form.append('<button type="submit" class="btn btn-warning">Deshabilitar <i class="glyphicon glyphicon-ban-circle"></i></button>');
      break;
    case 'VENTA':
      $form.append('<button type="submit" class="btn btn-success">Habilitar <i class="glyphicon glyphicon-ok-circle"></i></button>');
      break;
    case 'CANCELADO':
      $form.append('<div class="form-group"><select class="form-control" name="motivo" id="motivo"><option value="Lo vendí por fuera">Lo vendí por fuera</option><option value="No tengo el producto">No tengo el producto</option><option value="Ya no lo quiero vender">Ya no lo quiero vender</option><option value="Otro">Otro</option></select></div>');
      $form.append('<div class="form-group"><textarea name="observaciones" id="observaciones" cols="30" rows="3" placeholder="Detalles de la cancelación..." required></textarea></div>');
      $form.append('<button type="submit" class="btn btn-danger">Cancelar <i class="glyphicon glyphicon-remove"></i></button>');
      break;
    case 'EN CAMINO':
      $form.append('<h6>Datos de envío</h6>');
      var $div = $('<div class="form-group">');
      var $sel = $('<select class="form-control" name="paqueteria" id="paqueteria"></select>');
      $.ajax({
        url: 'listarPaqueterias',
        type: "POST",
        dataType:'json',
        success: function(res){
          $.each(res,function(i,item){
              $sel.append('<option value="'+item.nom_paqueteria+'">'+item.nom_paqueteria+'</option>');
              $div.append($sel);
              $form.append($div);
          });
          $form.append('<div class="form-group"><input class="form-control" type="number" id="guia" name="guia" placeholder="No. de guía" required></div>');
          $form.append('<button type="submit" class="btn btn-primary">Proceder <i class="fa fa-truck"></i></button>');
          $form.append('<button class="btn btn-cancel">Volver <i class="glyphicon glyphicon-arrow-right"></i></button>');
        },
        error: function(err){
          console.log(err);
        }
      });
      break;
  }
  if(state!='EN CAMINO') $form.append('<button class="btn btn-cancel">Volver <i class="glyphicon glyphicon-arrow-right"></i></button>');
  $d.append($form);
  $d.addClass('showingAnyForm');
}