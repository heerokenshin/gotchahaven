var marca = '';
var cat = '';
var sub = '';
var sort ='';
var palabras = '';

var ventas = ventas || {};

ventas.main = (function() {

  return {
    listarProductos:function(){
      $(document).ready(function(){
        $.ajax({
            url: 'listarProductosAlInicio',
            type: "POST",
            success: function(res){
              $('.sortProducts').html(res);
            },
            error: function(err){
              console.log(err);
            }
          });
      });
    },
  	buscarProductos:function(){
  		$(document).on('submit','.searchForm',function(e){
        e.preventDefault();
        palabras = e.target.palabras.value;
        listarProductos();
      });
  	},
    elegirCategoria:function(){
      $(document).on('click','.sortCategorias .catego',function(e){
        if($(e.target).hasClass('catego')){
          $('.sortCategorias .catego').each(function(){$(this).removeClass('active');$(this).find('.subs').html('');});
          $(e.currentTarget).addClass('active');
          var $div = $(e.currentTarget).find('.subs');
          $.ajax({
            url: 'mostrarSubcategorias',
            type: "POST",
            data:{'id':$(e.currentTarget).attr('id')},
            dataType:'json',
            success: function(res){
              $.each(res,function(i,item){
                $div.append('<figure id="'+item.id_subcatego+'" class="subcatego">'+item.nom_subcatego+'</figure>');
              });
            },
            error: function(err){
              console.log(err);
            }
          });
        }else{
          $('.sortCategorias .subcatego').each(function(){$(this).removeClass('active');});
          $(e.target).addClass('active');
          cat = $(e.target).parent().parent().attr('id');
          sub = $(e.target).attr('id');
          listarProductos();
        }
        
      });
    },
    elegirMarca:function(){
      $(document).on('click','.sortCategorias .marca',function(e){
        $('.sortCategorias .marca').each(function(){$(this).removeClass('active');});
        $(e.currentTarget).addClass('active');
        marca = $(e.currentTarget).attr('id');
        listarProductos();
      });
    },
    elegirSort:function(){
      $(document).on('change','.sortCategorias #order',function(e){
        sort = $(e.currentTarget).val();
        listarProductos();
      });
    },
    changeThumb:function(){
      $(document).on('click mouseover','.thumbs img',function(e){
        $('.thumbs img').each(function(){$(this).removeClass('active');});
        $(e.currentTarget).addClass('active');
        $(e.currentTarget).parent().parent().find('.viewer').attr('src',window.location.origin+'/gotchahaven/assets/img/products/'+$(e.currentTarget).attr('alt'));
      });
    },
    agregarAlCarrito:function(){
      $(document).on('click','.addCart',function(){
        $.ajax({
          url: 'listarCarrito',
          type: "POST",
          data:{'prod':$('.description #id_producto').val()},
          dataType:'json',
          success: function(res){
            console.log(res);
            if(res['site']){
              window.location.href = res['site']+window.location.search;
            }else{
              if($('.carrito .dropdown-menu li').length==0){
                $('.carrito .dropdown-menu h6').remove();
                $('.carrito .dropdown-menu').append('<li><a class="toCheckout btn btn-primary" href="'+window.location.origin+'/gotchahaven/index.php/compras/caja">Despachar <i class="fa fa-sign-in"></i></a></li>');
              }
              var $li = $('<li>');
              var $a = $('<a href="'+window.location.origin+'/gotchahaven/index.php/compras/producto?id='+res['id_producto']+'&pr='+res['nom_producto'].replace(/\s/g,'_')+'&tr='+res['id_transaccion']+'">');
              var pic = res['imagenes'].split('/');
              $a.append('<img src="'+window.location.origin+'/gotchahaven/assets/img/products/thumbs/'+pic[1]+'" alt="'+pic[1]+'"> '+res['nom_producto']);
              $li.append($a);
              $li.append('<aside><i id="'+res['id_producto']+'" class="glyphicon glyphicon-remove"></i></aside>');
              $('.carrito .dropdown-menu').prepend($li);
              
            }
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    quitarDelCarrito:function(){
      $(document).on('click','.carrito .glyphicon-remove',function(e){
        var $id = $(e.currentTarget);
        setTimeout(function(){
          $('.carrito').addClass('open');  
        },1);
        $.ajax({
          url: 'quitarProductoCarrito',
          type: "POST",
          data:{'prod':$id.attr('id')},
          success: function(res){
            console.log(res);
            $id.parent().parent().remove();       
            if($('.carrito ul li').length==1) $('.carrito ul').html('<h6>Sin productos aún</h6>');
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    }

  }
})();

function listarProductos(){
  $.ajax({
    url: 'listarProductos',
    type: "POST",
    data:{'cat':cat,'sub':sub,'marca':marca,'sort':sort,'palabras':palabras},
    success: function(res){
      $('.sortProducts').html(res);
    },
    error: function(err){
      console.log(err);
    }
  });
  $('html, body').stop().animate({
    scrollTop: ($('#sortProducts').offset().top - 150)
    }, 1250, 'easeInOutExpo');
          
}