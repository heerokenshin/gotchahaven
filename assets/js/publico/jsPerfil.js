var perfil = perfil || {};

perfil.main = (function() {

  return {
    limpiarForm:function(){
      $(document).on('click','.perfilForm .reset',function(e){
        e.preventDefault();
        $('.perfilForm #nombres').val('');
        $('.perfilForm #apellidos').val('');
        $('.perfilForm #correo').val('');
        $('.perfilForm #whatsapp').val('');
      });
      $(document).on('click','.dirForm .reset,.ctaForm .reset',function(e){
        e.preventDefault();
        $(e.currentTarget).parent()[0].reset();
      });
      $(document).on('click','.editDirForm .reset',function(e){
        e.preventDefault();
        $('.editDirForm input').each(function(){$(this).val('')});
        $('.editDirForm select').val(1);
      });
      $(document).on('click','.editCtaForm .reset',function(e){
        e.preventDefault();
        $('.editCtaForm input').each(function(){$(this).val('')});
        $('.editCtaForm select').val(1);
      });
    },
    procesarFormularioPerfil:function(){
      $("#perfilForm input,#perfilForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "perfil/editarPerfil",
                type: "POST",
                data:dta,
                success: function(res){
                  $('#successPerfilDir').html("<div class='alert alert-success'>");
                  $('#successPerfilDir > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                  $('#successPerfilDir > .alert-success').append("<strong>Se han modificado tus datos satisfactoriamente!</strong>");
                  $('#successPerfilDir > .alert-success').append('</div>');
                  refreshProfile(dta);
                  $('.perfilCard').css('display','block');
                  $('.perfilForm').css('display','none');
                },
                error: function(err){
                  console.log(err);
                  $('#successPerfilDir').html("<div class='alert alert-danger'>");
                    $('#successPerfilDir > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#successPerfilDir > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                    $('#successPerfilDir > .alert-danger').append('</div>');
                    //clear all fields
                    $('#perfilForm').trigger("reset");
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
    },
    editarPerfil:function(){
      $(document).on('click','.editarPerfil',function(e){
        $('.perfilCard').css('display','none');
        $('.perfilForm').css('display','block');
        $(this).css('opacity','0');
      });
    },
    cancelarPerfilEdicion:function(){
      $(document).on('click','.perfilForm .cancel', function(e){
        e.preventDefault();
        $('.perfilForm')[0].reset();
        $('.perfilCard').css('display','block');
        $('.perfilForm').css('display','none');
        $('.editarPerfil').css('opacity','1');
      });
    },
    cancelarAgregarDir:function(){
      $(document).on('click','.dirForm .cancel',function(e){
        e.preventDefault();
        $('.dirForm #id_usuario').val('');
        $('#nuevaDir').removeClass('fadeUp');
        $('#nuevaDir').css('display','none');
        $('.agregarDireccion').css('opacity','1');
      });
      $(document).on('click','.ctaForm .cancel',function(e){
        e.preventDefault();
        $('.ctaForm #id_usuario').val('');
        $('#nuevaCta').removeClass('fadeUp');
        $('#nuevaCta').css('display','none');
        $('.agregarCuenta').css('opacity','1');
      });
    },
    agregarDireccion:function(){
      $(document).on('click','.agregarDireccion',function(e){
        $(this).css('opacity','0');
        $('#nuevaDir').css('display','block');
        $('#nuevaDir').addClass('fadeUp');
        $('.dirForm #id_usuario').val($('.editarPerfil').attr('id'));

        $('html, body').stop().animate({
          scrollTop: ($('#dirForm').offset().top - 150)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
      });
    },
    agregarCuenta:function(){
      $(document).on('click','.agregarCuenta',function(e){
        $(this).css('opacity','0');
        $('#nuevaCta').css('display','block');
        $('#nuevaCta').addClass('fadeUp');
        $('.ctaForm #id_usuario').val($('.editarPerfil').attr('id'));

        $('html, body').stop().animate({
          scrollTop: ($('#ctaForm').offset().top - 150)
          }, 1250, 'easeInOutExpo');
          e.preventDefault();
      });
    },
    procesarFormularioCta:function(){
      $("#ctaForm input,#ctaForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "perfil/agregarNuevaCuenta",
                type: "POST",
                data:dta,
                success: function(res){
                  $('#successCta').html("<div class='alert alert-success'>");
                  $('#successCta > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                  $('#successCta > .alert-success').append("<strong>Cuenta bancaria agregada satisfactoriamente!</strong>");
                  $('#successCta > .alert-success').append('</div>');
                  refreshCuentas();
                  $('#nuevaCta').removeClass('fadeUp');
                  setTimeout(function(){$('#nuevaCta').css('display','none');},100);
                },
                error: function(err){
                  console.log(err);
                  $('#successCta').html("<div class='alert alert-danger'>");
                    $('#successCta > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#successCta > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                    $('#successCta > .alert-danger').append('</div>');
                    //clear all fields
                    $('#ctaForm').trigger("reset");
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
    },
    procesarFormularioDir:function(){
      $("#dirForm input,#dirForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "perfil/agregarNuevaDireccion",
                type: "POST",
                data:dta,
                success: function(res){
                  $('#successDir').html("<div class='alert alert-success'>");
                  $('#successDir > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
                  $('#successDir > .alert-success').append("<strong>Dirección agregada satisfactoriamente!</strong>");
                  $('#successDir > .alert-success').append('</div>');
                  refreshDirecciones();
                  $('.editarPerfil').css('opacity','0');
                  $('#nuevaDir').removeClass('fadeUp');
                  setTimeout(function(){$('#nuevaDir').css('display','none');},100);
                },
                error: function(err){
                  console.log(err);
                  $('#successDir').html("<div class='alert alert-danger'>");
                    $('#successDir > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#successDir > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                    $('#successDir > .alert-danger').append('</div>');
                    //clear all fields
                    $('#dirForm').trigger("reset");
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
    },
    cerrarFormularioDir:function(){
      $(document).on('click','#successDir .close',function(e){
        e.preventDefault();
        $('.dirForm #id_usuario').val('');
        $('#nuevaDir').removeClass('fadeUp');
        $('#nuevaDir').css('display','none');
        $('.agregarDireccion').css('opacity','1');
      });
    },
    cerrarFormularioCta:function(){
      $(document).on('click','#successCta .close',function(e){
        e.preventDefault();
        $('.ctaForm #id_usuario').val('');
        $('#nuevaCta').removeClass('fadeUp');
        $('#nuevaCta').css('display','none');
        $('.agregarCuenta').css('opacity','1');
      });
    },
    abrirFormularioEditarDireccion:function(){
      $(document).on('click','.editarDireccion',function(e){
        var aside = $(e.currentTarget).parent().parent().find('aside');
        $(aside).html('');
        var id = $(e.currentTarget).attr('id');
        $.ajax({
          url: "perfil/formularioEditarDireccion",
          type: "POST",
          data:{'id_usuario':$('.editarPerfil').attr('id'),'id_dir':id},
          success: function(res){
            $(aside).html(res);
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    abrirFormularioEditarCuenta:function(){
      $(document).on('click','.editarCuenta',function(e){
        var aside = $(e.currentTarget).parent().parent().find('aside');
        $(aside).html('');
        var id = $(e.currentTarget).attr('id');
        $.ajax({
          url: "perfil/formularioEditarCuenta",
          type: "POST",
          data:{'id_usuario':$('.editarPerfil').attr('id'),'id_cuenta':id},
          success: function(res){
            $(aside).html(res);
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    procesarFormularioEditarDir:function(){
      console.log('aqui estaba el procesar dir editar');
    },
    preEliminarDireccion:function(){
      $(document).on('click','.borrarDireccion',function(e){
        var title = $(e.currentTarget).parent().find('.title');
        $(title).html('¿Proceder? <button id="'+$(e.currentTarget).attr('id')+'" class="delYes btn btn-cancel">SI</button><button class="delNo btn btn-cancel">NO</button>');
      });
    },
    preEliminarCuenta:function(){
      $(document).on('click','.borrarCuenta',function(e){
        var title = $(e.currentTarget).parent().find('.title');
        $(title).html('¿Proceder? <button id="'+$(e.currentTarget).attr('id')+'" class="delYes btn btn-cancel">SI</button><button class="delNo btn btn-cancel">NO</button>');
      });
    },
    cancelarBorrarDireccion:function(){
      $(document).on('click','.dirFig .delNo',function(e){
        $(e.currentTarget).parent().html('Datos de dirección');
      });
    },
    confirmarBorrarDireccion:function(){
      $(document).on('click','.dirFig .delYes',function(e){
        $.ajax({
          url: "perfil/eliminarDireccion",
          type: "POST",
          data:{'id':$(e.currentTarget).attr('id')},
          success: function(res){
            refreshDirecciones();
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    cancelarBorrarCuenta:function(){
      $(document).on('click','.ctaFig .delNo',function(e){
        $(e.currentTarget).parent().html('Datos de la cuenta');
      });
    },
    confirmarBorrarCuenta:function(){
      $(document).on('click','.ctaFig .delYes',function(e){
        $.ajax({
          url: "perfil/eliminarCuenta",
          type: "POST",
          data:{'id':$(e.currentTarget).attr('id')},
          success: function(res){
            refreshCuentas();
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    cancelarEditarDireccion:function(){
      $(document).on('click','.editDirForm .cancel',function(e){
        e.preventDefault();
        $(e.currentTarget).parent()[0].reset();
        var dta = $(e.currentTarget).parent().serialize().split('&');
        var $div = $(e.currentTarget).parent().parent();
        $div.html('');
        var d = [];
        for(var i=0;i<dta.length;i++){
          var e = dta[i].split('=');
          d[i] = e[1];
        }
        $.ajax({
            url: "perfil/obtenerEstado",
            type: "POST",
            data:{'id':d[6]},
            dataType: 'json',
            success: function(res){
              $div.append('<b>Calle:</b><br>'+d[2]+'<br>');
              $div.append('<b>Colonia:</b><br>'+d[3]+'<br>');
              $div.append('<b>Código postal:</b><br>'+d[4]+'<br>');
              $div.append('<b>Municipio:</b><br>'+d[5]+'<br>');
              $div.append('<b>Estado:</b><br>'+res[0]['nom_estado']+'<br>');
            },
            error: function(err){
              console.log(err);
            }
          });

      });
    },
    cancelarEditarCuenta:function(){
      $(document).on('click','.editCtaForm .cancel',function(e){
        e.preventDefault();
        $(e.currentTarget).parent()[0].reset();
        var dta = decodeURIComponent($(e.currentTarget).parent().serialize()).replace(/\+/g,' ').split('&');
        var $div = $(e.currentTarget).parent().parent();
        $div.html('');
        var d = [];
        for(var i=0;i<dta.length;i++){
          var e = dta[i].split('=');
          d[i] = e[1];
        }
        $div.append('<b>Banco:</b><br>'+d[2]+'<br>');
        $div.append('<b>Titular:</b><br>'+d[3]+'<br>');
        $div.append('<b>No. cuenta:</b><br>'+d[4]+'<br>');
        $div.append('<b>Clabe interbancaria:</b><br>'+d[5]+'<br>');
       
      });
    }

  }
})();

function refreshProfile(dta){
  $('.editarPerfil').css('opacity','1');
  var d = decodeURIComponent(dta).split('&');
  for(var i=0; i<d.length; i++){
    var e = d[i].split('=');
    $('.perfilCard #'+e[0]).html(e[1]);
    $('.perfilForm #'+e[0]).html(e[1]);
  }
}

function refreshDirecciones(){
  $('.dirFig').parent().each(function(){
    if($(this).attr('id')==null) $(this).remove();
    });
    var dir = $('#direcciones .row')[1];
    $.ajax({
      url: "perfil/listarDirecciones",
      type: "POST",
      data:{'id':$('.editarPerfil').attr('id')},
      dataType: 'json',
      success: function(res){
        $.each(res,function(i,item){
          var $col = $('<div style="vertical-align:top;margin-bottom:6px;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">');
          var $fig = $('<figure class="dirFig">');
          $fig.append('<div class="row"><i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;<span class="title">Datos de dirección</span><span id="'+item.id_dir+'" class="borrarDireccion floatRight"><i class="glyphicon glyphicon-trash"></i></span><span id="'+item.id_dir+'" class="editarDireccion floatRight"><i class="glyphicon glyphicon-pencil"></i></span></div>');
          $fig.append('<aside><b>Calle:</b><br>'+item.calle+'<br><b>Colonia:</b><br>'+item.colonia+'<br><b>Código postal:</b><br>'+item.cp+'<br><b>Municipio:</b><br>'+item.municipio+'<br><b>Estado:</b><br>'+item.nom_estado+'<br></aside>');
          $col.append($fig);
          $(dir).prepend($col);
        });
      },
      error: function(err){
        console.log(err);
      }
    });
}

function refreshCuentas(){
  $('.ctaFig').parent().each(function(){
    if($(this).attr('id')==null) $(this).remove();
    });
    var dir = $('#cuentas .row')[1];
    $.ajax({
      url: "perfil/listarCuentas",
      type: "POST",
      data:{'id':$('.editarPerfil').attr('id')},
      dataType: 'json',
      success: function(res){
        $.each(res,function(i,item){
          var $col = $('<div style="vertical-align:top;margin-bottom:6px;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">');
          var $fig = $('<figure class="ctaFig">');
          $fig.append('<div class="row"><i class="fa fa-credit-card"></i>&nbsp;&nbsp;<span class="title">Datos de cuenta</span><span id="'+item.id_cuenta+'" class="borrarCuenta floatRight"><i class="glyphicon glyphicon-trash"></i></span><span id="'+item.id_cuenta+'" class="editarCuenta floatRight"><i class="glyphicon glyphicon-pencil"></i></span></div>');
          $fig.append('<aside><b>Banco:</b><br>'+item.banco+'<br><b>Titular:</b><br>'+item.titular+'<br><b>No. cuenta:</b><br>'+item.no_cuenta+'<br><b>Clabe interbancaria:</b><br>'+item.clabe+'<br></aside>');
          $col.append($fig);
          $(dir).prepend($col);
        });
      },
      error: function(err){
        console.log(err);
      }
    });
}