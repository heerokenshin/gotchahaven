var months = ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

var publico = publico || {};

publico.main = (function() {

  return {
  	loginFacebook:function(){
  		$(document).on('click','.btn-face',function(e){
  			doLogin();
  		});
  	},
    desloguearse: function() {
    	$(document).on('click','.logout',function(e){
        /*console.log(window.location.pathname);
        var r = 'index.php/perfil/logout';
        if(window.location.pathname.indexOf('perfil')>-1){
    		  r = 'perfil/logout';
           doLogout();
        }else {
          doLogout();
        }*/
        var r = 'http://www.gotchahaven.com/zanzibar/index.php/perfil/logout';
        doLogout();
        $.ajax({
          url: r,
          type: "POST",
          success: function(res){
            console.log(res);
            //window.location.href = 'http://127.0.0.1/gotchahaven'; 
            window.location.href = 'http://www.gotchahaven.com/zanzibar'; 
          },
          error: function(err){
            console.log(err);
          }
        });
		  });
    },
    agregarDireccion:function(){
      $(document).on('click','.addDireccion',function(e){
        e.preventDefault();
        $.ajax({
          url: "agregarDireccionForm",
          type: "POST",
          success: function(res){
            $('.direccion').html(res);
          },
          error: function(err){
            console.log(err);
          }
        });
      });
    },
    limpiarForm:function(){
      $(document).on('click','.perfilForm .reset',function(e){
        e.preventDefault();
        console.log($('.perfilForm')[0]);
        $('.perfilForm')[0].reset();
      });
    },
    procesarFormularioPerfil:function(){
      $("#perfilForm input,#perfilForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "agregarPerfilYDireccion",
                type: "POST",
                data:dta,
                success: function(res){
                  var redir = ($('#prodRedir').length>0)?$('#prodRedir').val():'http://www.gotchahaven.com';
                  if(res){
                    $('.firstTime .intro-text').html('');
                    $('.firstTime .intro-text').append('<h2 class="fadeUp" style="text-align:center;">Todo listo!</h2>');
                    $('.firstTime .intro-text').append('<h3 class="fadeUp" style="text-align:center;">Ahora eres parte de Gotcha Haven! Te invitamos a darte una vuelta por el sitio, el refugio del paintball y airsoft.</h3>');
                    $('.firstTime .intro-text').append('<h5 class="fadeUp" style="text-align:center;"><a href="'+redir+'" class="btn btn-primary">ACEPTAR</a></h5>');
                  }else{
                    $('.firstTime .intro-text').html('');
                    $('.firstTime .intro-text').append('<h2 class="fadeUp" style="text-align:center;">Algunos datos fueron guardados, algunos no...</h2>');
                    $('.firstTime .intro-text').append('<h3 class="fadeUp" style="text-align:center;">Ahora eres parte de Gotcha Haven! Tus datos faltantes los puedes agregar más tarde en tu perfil...</h3>');
                    $('.firstTime .intro-text').append('<h5 class="fadeUp" style="text-align:center;"><a href="'+redir+'" class="btn btn-primary">ACEPTAR</a></h5>');
                  }
                },
                error: function(err){
                  console.log(err);
                  $('.firstTime .intro-text').html('');
                  $('.firstTime .intro-text').append('<h2 class="fadeUp" style="text-align:center;">Algunos datos fueron guardados, algunos no...</h2>');
                  $('.firstTime .intro-text').append('<h3 class="fadeUp" style="text-align:center;">Ahora eres parte de Gotcha Haven! Tus datos faltantes los puedes agregar más tarde en tu perfil...</h3>');
                  $('.firstTime .intro-text').append('<h5 class="fadeUp" style="text-align:center;"><a href="'+redir+'" class="btn btn-primary">ACEPTAR</a></h5>');
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });
    }

  }
})();

function fancyDate(date,withYear){
	var d = date.split('-');
	return months[parseInt(d[1])]+' '+parseInt(d[2])+', '+((withYear==true)?d[0]:'');
}

function getSiglas(str){
	var s = str.replace(/ /g,'');
	return s.replace(/[a-z]/g,'');
}