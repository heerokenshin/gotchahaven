$(".login input,.login textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize()+'&op=7';
            console.log(dta);
            $.ajax({
                url: "operations.php",
                type: "POST",
                data:dta,
                success: function(res){
                	console.log(res);
                	if(res.length>0){
                		if(res[0]!=0){
                			$('.onlybigboss').css('display','block');
                			$('.login').css('display','none');
                			refreshFolioTable();
                			refreshPedidoTable();
                		}else $('.login .error').text('Usuario o contraseña incorrecta...');
                	} 
                    else $('.login .error').text('Usuario no encontrado...');
                },
                error: function(err){
                    console.log(err);
                    $('.login .error').text('Usuario no encontrado...');
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

$(document).on('click','.addLine',function(e){
	e.preventDefault();
	var $fig = $('<figure class="linea"><div class="form-group" style="width:45%;display:inline-block;vertical-align:top;"><input type="number" class="form-control" placeholder="Referencia o autorización *" name="refe" id="refe" required data-validation-required-message="Ingresa números."><p class="help-block text-danger"></p></div><div class="form-group" style="width:45%;display:inline-block;vertical-align:top;"><input type="number" class="form-control" placeholder="Monto *" name="monto" id="monto" required data-validation-required-message="Ingresa monto."><p class="help-block text-danger"></p></div><button class="remove btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button></figure>');
	$('.lines').append($fig);
});

$(document).on('click','.remove',function(e){
	e.preventDefault();
	$(e.currentTarget).parent().remove();
});

$(document).on('submit','.addAuto',function(e){
	e.preventDefault();
	var m = [],r = [];
	/*$('.linea').each(function(){
		r.push($(this).find('#refe').val());
		m.push($(this).find('#monto').val());
	});*/
	$.ajax({
		url: "operations.php",
		type: "POST",
		data: {'mon':e.target.monto.value,'refe':e.target.refe.value,'op':e.target.op.value},
		success: function(results){
			refreshFolioTable();
			swal("Lineas enviadas!", "Esperando donativos!", "success");
			
		},
		error: function(){
			swal("Lineas no cargadas!", "Le sugerimos reintentar o volver más tarde...", "error");
			
		}
	});
});

$(document).on('click','.seeMerch',function(e){
	var $m = $(e.currentTarget).parent().find('.merch');
	console.log($m);
	$m.html('');
	$.ajax({
		url: "operations.php",
		type: "POST",
		data:{'op':9,'id':$(e.currentTarget).attr('id')},
		dataType:'json',
		success: function(res){
			console.log(res);
			$.each(res,function(i,item){
				$m.append(item.cantidad+' '+item.descripcion+' '+item.tamanio+' $'+item.precio+' MXN<br>');
			});
		},
		error: function(err){
			console.log(err);
		}
	});
});

function refreshFolioTable(){
	$('.onlybigboss .folio tbody').html('');
	$.ajax({
		url: "operations.php",
		type: "POST",
		data:{'op':1},
		dataType:'json',
		success: function(res){
			$.each(res,function(i,item){
				$('.onlybigboss .folio tbody').append('<tr><td>'+item.id_autorizacion+'</td><td>'+item.monto+'</td><td>'+item.estado+'</td><td>'+item.fecha+'</td></tr>');
			});
		},
		error: function(err){
			console.log(err);
		}
	});
}

function refreshPedidoTable(){
	$('.onlybigboss .pedido tbody').html('');
	$.ajax({
		url: "operations.php",
		type: "POST",
		data:{'op':8},
		dataType:'json',
		success: function(res){
			$.each(res,function(i,item){
				var $tr = $('<tr>');
				$tr.append('<td>'+item.id_donativo+'</td>');
				$tr.append('<td>'+item.autorizacion+'</td>');
				var $btngrp = $('<td><div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+item.nombre+' <span class="caret"></span></button><div class="dropdown-menu">'+item.domicilio+'<br>'+item.colonia+'<br>'+item.cp+'<br>'+item.municipio+'<br>'+item.estado+'</div></div></td>');
				$tr.append($btngrp);
				$tr.append('<td>'+item.correo+'</td>');
				$tr.append('<td>'+item.total+'</td>');
				$btngrp = $('<td><div class="btn-group"><button id="'+item.id_donativo+'" type="button" class="seeMerch btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pedido <span class="caret"></span></button><div class="dropdown-menu merch"></div></div></td>');
				$tr.append($btngrp);
				$('.onlybigboss .pedido tbody').append($tr);
			});
		},
		error: function(err){
			console.log(err);
		}
	});
}