<?php
Class Estados extends CI_Model{
  //en el archivo de application/config/database.php
  //estan los datos para modificar la base de datos que vayas a crear
  public function __construct(){
    parent::__construct();
    //Load the url helper - NO RECUERDO PORQUE LO PUSE PERO POR SI LAS FLAIS
    $this->load->helper('url');
  }

  function getEstados(){
    $this -> db -> select('*');
    $this -> db -> from('estados');

    $query = $this -> db -> get();
    return $query->result();
  }

  function getSingleState($id){
    $this -> db -> select('*');
    $this -> db -> from('estados');
    $this -> db -> where('id_estado', $id);

    $query = $this -> db -> get();
    return $query->result();
  }

}
?>