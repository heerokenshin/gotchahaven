<?php
Class Producto extends CI_Model{
  public function __construct(){
    parent::__construct();

    $this->load->database();
  }

  function insertProducto($prod){
    $res = $this-> db ->insert('productos',$prod);
    if($res){
      $this-> db ->select('id_producto');
      $this-> db -> from('productos');
      $this-> db -> order_by('id_producto','desc');

      $query = $this -> db -> get();
      return $query->row();
    }
    else return false;
  }

  function updateProducto($prod){
    $this-> db ->where("id_producto",$prod['id_producto']);
    return $this-> db ->update("productos",$prod);
  }

  function deleteProducto($id){
    $this-> db ->where('id_producto',$id);
    return $this-> db ->delete('productos');
  }

  function getSingleProduct($id){
    $this -> db -> select('*');
    $this -> db -> from('productos');
    $this -> db -> where('id_producto', $id);
 
    $query = $this -> db -> get();
    return $query->result();
  }

  function getSingleProductWithTransaccion($id){
    $this -> db -> select('*');
    $this -> db -> from('productos');
    $this -> db -> where('productos.id_producto', $id);
    $this -> db -> join('transaccion','transaccion.id_producto=productos.id_producto','left');
 
    $query = $this -> db -> get();
    return $query->result();
  }

  function getRecentProductos($n){
    $this -> db ->select('*');
    $this -> db ->from('productos');
    $this -> db ->join('marcas','marcas.id_marcas=productos.id_marcas','left');
    $this -> db ->join('categorias','categorias.id_categoria=productos.id_categoria','left');
    $this -> db -> order_by('productos.id_producto','desc');
    $this -> db ->limit($n);

    $query = $this -> db -> get();
    return $query->result();
  }

  function getRecentSaleProductos($n){
    $this -> db ->select('*');
    $this -> db ->from('productos');
    $this -> db ->where('transaccion.estado','VENTA');
    $this -> db ->join('transaccion','transaccion.id_producto=productos.id_producto','left');
    $this -> db ->join('marcas','marcas.id_marcas=productos.id_marcas','left');
    $this -> db ->join('categorias','categorias.id_categoria=productos.id_categoria','left');
    $this -> db -> order_by('productos.id_producto','desc');
    $this -> db ->limit($n);

    $query = $this -> db -> get();
    return $query->result();
  }

  function getTotalSaleProducts(){
    $this -> db ->select('count(*) as total');
    $this -> db ->from('productos');
    $this -> db ->where('transaccion.estado','VENTA');
    $this -> db ->join('transaccion','transaccion.id_producto=productos.id_producto','left');

    $query = $this -> db -> get();
    return $query->row();
  }

  function getMarcas(){
  	$this -> db -> select('*');
    $this -> db -> from('marcas');
 
    $query = $this -> db -> get();
 	  return $query->result();
  }

  function getCategorias(){
  	$this -> db -> select('*');
    $this -> db -> from('categorias');
 
    $query = $this -> db -> get();
 	  return $query->result();
  }

  function getSubcategorias($id){
  	$this -> db -> select('*');
    $this -> db -> from('subcategoria');
    $this -> db -> where('id_categoria', $id);
 
    $query = $this -> db -> get();
 	  return $query->result();
  }

  function getSubcategoriasSorter($id){
    $this -> db -> select('*');
    $this -> db -> from('subcategoria');
    $this -> db -> where('id_categoria', $id);
 
    $query = $this -> db -> get();
    return $query->result_array();
  }

}
?>