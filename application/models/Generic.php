<?php
Class Generic extends CI_Model{
  public function __construct(){
    parent::__construct();
  }

  public function sendEmail($data){
       
    $name_comprador = strip_tags(htmlspecialchars($data['comprador'][0]->nombres.' '.$data['comprador'][0]->apellidos));
    $name_vendedor = strip_tags(htmlspecialchars($data['vendedor'][0]->nombres.' '.$data['vendedor'][0]->apellidos));
    $email_address = strip_tags(htmlspecialchars($data['vendedor'][0]->correo));
    $phone = strip_tags(htmlspecialchars($data['vendedor'][0]->whatsapp));
    $message = strip_tags(htmlspecialchars($data['mensaje']));
       
    $email_subject = $data['asunto'].': '. $name_comprador;
    $email_body = "De: $name_vendedor<br><br>Correo: $email_address<br><br>Whatsapp: $phone<br><br>Mensaje:<br>".nl2br($message);
 

    $this->load->library('email'); // load email library
    $this->email->from('contact@gotchahaven.com', 'Gotcha Haven');
    $this->email->to($data['comprador'][0]->correo);
    $this->email->cc($data['vendedor'][0]->correo); 
    $this->email->subject($email_subject);
    $this->email->message($email_body);

    if ($this->email->send()) return true;
    else return false;
  }

  public function requestStorePlan($data){
    $data = array(
        'nombre'=>$this->input->post('name'),
        'whatsapp'=>$this->input->post('phone'),
        'correo'=>$this->input->post('email'),
        'mensaje'=>$this->input->post('message'),
        'plan'=>$this->input->post('plan'),
      );

    $email_address = strip_tags(htmlspecialchars($data['correo']));
    $phone = strip_tags(htmlspecialchars($data['whatsapp']));
    $message = strip_tags(htmlspecialchars($data['mensaje']));
       
    $email_subject = 'Solicitud de plan tienda: '. $data['nombre'];
    $email_body = "De: ".$data['nombre']."<br><br>Correo: $email_address<br><br>Whatsapp: $phone<br><br>Plan: ".$data['plan']."<br><br>Mensaje:<br>".nl2br($message);
    
    $this->load->library('email'); // load email library
    $this->email->from('contact@gotchahaven.com', 'No-reply');
    $this->email->to('bigboss@gotchahaven.com');
    //$this->email->cc($data['vendedor'][0]->correo); 
    $this->email->subject($email_subject);
    $this->email->message($email_body);

    if ($this->email->send()) return true;
    else return false;
  }

  public function contactGotchaHaven($data){
    $email_address = strip_tags(htmlspecialchars($data['correo']));
    $phone = strip_tags(htmlspecialchars($data['whatsapp']));
    $message = strip_tags(htmlspecialchars($data['mensaje']));
       
    $email_subject = 'Contacto GH: '. $data['nombre'];
    $email_body = "De: ".$data['nombre']."<br><br>Correo: $email_address<br><br>Whatsapp: $phone<br><br>Mensaje:<br>".nl2br($message);
    
    $this->load->library('email'); // load email library
    $this->email->from('contact@gotchahaven.com', 'No-reply');
    $this->email->to('bigboss@gotchahaven.com');
    $this->email->cc('contact@gotchahaven.com'); 
    $this->email->subject($email_subject);
    $this->email->message($email_body);

    if ($this->email->send()) return true;
    else return false;
  }

  public function sendChangeRequest($data){
    $name_tendero = strip_tags(htmlspecialchars($data['tendero'][0]->nombres.' '.$data['tendero'][0]->apellidos));
    $email_address = strip_tags(htmlspecialchars($data['tendero'][0]->correo));
    $phone = strip_tags(htmlspecialchars($data['tendero'][0]->whatsapp));
    $message = strip_tags(htmlspecialchars($data['mensaje']));
       
    $email_subject = $data['asunto'].': '. $name_tendero;
    $email_body = "De: $name_tendero<br><br>Correo: $email_address<br><br>Whatsapp: $phone<br><br>Mensaje:<br>".nl2br($message);


    $this->load->library('email'); // load email library
    $this->email->from('contact@gotchahaven.com', 'No-reply');
    $this->email->to('bigboss@gotchahaven.com');
    $this->email->cc('contact@gotchahaven.com');
    $this->email->subject($email_subject);
    $this->email->message($email_body);

    if ($this->email->send()) return true;
    else return false;
  }

}
?>