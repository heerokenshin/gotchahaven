<?php
Class Tienda extends CI_Model{
  public function __construct(){
    parent::__construct();
    $this->load->helper('url');
  }

  public function getSingleTiendaByUserId($id){
    $this -> db -> select('*');
    $this -> db -> from('tienda');
    $this -> db -> where('tienda.id_usuario', $id);
    $this -> db -> join('estados','estados.id_estado=tienda.id_estado','left');
    $this -> db -> limit(1);
 
    $query = $this -> db -> get();
 
    if($query -> num_rows() == 1){
      return $query->result();
    }
    else{
      return false;
    }
  }

  public function insertTienda($data){
    $res = $this-> db ->insert('tienda',$data);
    return $res;
  }

  public function updateTienda($data){
    $this-> db ->where("id_usuario",$data['id_usuario']);
    return $this-> db ->update("tienda",$data);
  }

  public function insertIntoBodega($data){
    $res = $this-> db ->insert('bodega',$data);
    return $res;
  }

  public function getProductosByTienda($id){
    $this -> db ->select('*');
    $this -> db ->from('productos');
    $this -> db ->where('bodega.id_tienda',$id);
    $this -> db ->join('bodega','bodega.id_producto=productos.id_producto','left');
    $this -> db ->join('transaccion','transaccion.id_producto=productos.id_producto','left');
    $this -> db -> order_by('productos.id_producto','desc');

    $query = $this -> db -> get();
    return $query->result();
  }

  public function updateExistencia($data){
    $this -> db ->where('id_tienda',$data['id_tienda']);
    $this -> db ->where('id_producto',$data['id_producto']);
    return $this -> db ->update('bodega',$data);
  }

}
?>