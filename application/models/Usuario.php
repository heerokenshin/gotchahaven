<?php
Class Usuario extends CI_Model{
  //en el archivo de application/config/database.php
  //estan los datos para modificar la base de datos que vayas a crear
  public function __construct(){
    parent::__construct();
    //Load the url helper - NO RECUERDO PORQUE LO PUSE PERO POR SI LAS FLAIS
    $this->load->helper('url');
  }

  function insertUsuario($perfil){
    $res = $this-> db ->insert('usuario',$perfil);
    return $res;
  }

  function getUserById($id){
    $this -> db -> select('*');
    $this -> db -> from('usuario');
    $this -> db -> where('id_usuario', $id);
    $this -> db -> limit(1);
 
    $query = $this -> db -> get();
 
    if($query -> num_rows() == 1){
      return $query->result();
    }else{
      return false;
    }
  }

  function getSingleUserByFacebook($face){
    $this->db->select('id_usuario');
    $this->db->from('usuario');
    $this->db->where('facebook',$face);

    $query = $this->db->get();
    return $query->result_array();
  }

  function updateUsuario($perfil){
    $this->db->where("id_usuario",$perfil['id_usuario']);
    return $this->db->update("usuario",$perfil);
  }

  function getLoggedUser($id){
    $this -> db -> select('*');
    $this -> db -> from('usuario');
    $this -> db -> where('facebook', $id);
    $this -> db -> limit(1);
 
    $query = $this -> db -> get();
 
    if($query -> num_rows() == 1){
      return $query->result();
    }
    else{
      return false;
    }
  }

  function getUserAddress($id_usuario) {
    $this -> db -> select('*');
    $this -> db -> from('direcciones');
    $this -> db -> join('estados', 'estados.id_estado = direcciones.id_estado');
    $this -> db -> where('id_usuario', $id_usuario);

    $query = $this -> db -> get();

    return $query->result_array();
  }

  function getSingleAddress($user,$dir) {
    $this -> db -> select('*');
    $this -> db -> from('direcciones');
    $this -> db -> where('id_dir', $dir);
    $this -> db -> where('id_usuario', $user);

    $query = $this -> db -> get();

    if($query -> num_rows() == 1){
      return $query->result_array();
    }
    else{
      return false;
    }
  }

  function insertDireccion($dir){
    $res = $this-> db ->insert('direcciones',$dir);
    return $res;
  }


  function updateDireccion($dir){
    $this->db->where("id_dir",$dir['id_dir']);
    return $this->db->update("direcciones",$dir);
  }

  function deleteDireccion($id) {
    $this->db->where('id_dir',$id);
    return $this->db->delete('direcciones');
  }


  function insertCuenta($cta){
    $res = $this -> db ->insert('cuentasb',$cta);
    return $res;
  }

  function getUserAccounts($id){
    $this-> db -> select('*');
    $this -> db -> from('cuentasb');
    $this -> db -> where('id_usuario', $id);

    $query = $this->db->get();
    return $query->result_array();
  }

  function getSingleAccount($user,$acc){
    $this -> db -> select('*');
    $this -> db -> from('cuentasb');
    $this -> db -> where('id_cuenta', $acc);
    $this -> db -> where('id_usuario', $user);

    $query = $this -> db -> get();

    if($query -> num_rows() == 1){
      return $query->result_array();
    }
    else{
      return false;
    }
  }

  function updateCuenta($acc){
    $this->db->where("id_cuenta",$acc['id_cuenta']);
    return $this->db->update("cuentasb",$acc);
  }

  function deleteCuenta($id) {
    $this->db->where('id_cuenta',$id);
    return $this->db->delete('cuentasb');
  }
}
?>