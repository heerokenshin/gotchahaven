<?php
Class Staff extends CI_Model{
  //en el archivo de application/config/database.php
  //estan los datos para modificar la base de datos que vayas a crear
  public function __construct(){
    parent::__construct();
    //Load the url helper - NO RECUERDO PORQUE LO PUSE PERO POR SI LAS FLAIS
    $this->load->helper('url');
  }

  public function insertStaff(){
    $data = array(
      'nombres'=>$_POST['nombre'],
      'apellidos'=>$_POST['apellidos'],
      'titulo'=>$_POST['titulo'],
      'puesto'=>$_POST['puesto'],
      'telefono'=>$_POST['telefono'],
      'ext'=>$_POST['ext'],
      'correo'=>$_POST['correo'],
      'foto'=>$this->staff->uploadFILE()
    );
    $res = $this-> db ->insert('staff',$data);
    return $res;
  }

  function getStaffList(){
    $this -> db -> select('id_staff,nombres,apellidos,titulo,puesto,foto');
    $this -> db -> from('staff');
    $this -> db -> order_by('id_staff','desc');
    $this -> db -> limit(5);

    $query = $this -> db -> get();
    return $query->result();
  }

  function getStaffTable(){
    $this -> db -> select('*');
    $this -> db -> from('staff');

    $query = $this -> db -> get();
    return $query->result();
  }

  function getSingleMember($id){
    $this->db->select('*');
    $this->db->from('staff');
    $this->db->where('id_staff',$id);

    $query = $this->db->get();
    return $query->result_array();
  }
  function deleteSingleMember($id){
    $res = $this->getSingleMember($id);
    //borrado de imagenes al momento de eliminar un miembro
    unlink('./assets/images/staff/'.$res[0]['foto']);

    $this->db->where('id_staff',$id);
    return $this->db->delete('staff');
  }
  function editStaff(){
    $foto = ($_FILES['input-file-load']["name"])?$this->staff->uploadFILE():$_POST['oldFoto'];
    if($foto!=$_POST['oldFoto']) unlink('./assets/images/staff/'.$oldFoto);
    $data = array(
      'id_staff'=>$_POST['id_staff'],
      'nombres'=>$_POST['nombre'],
      'apellidos'=>$_POST['apellidos'],
      'titulo'=>$_POST['titulo'],
      'puesto'=>$_POST['puesto'],
      'telefono'=>$_POST['telefono'],
      'ext'=>$_POST['ext'],
      'correo'=>$_POST['correo'],
      'foto'=>$foto
    );
    $this->db->where("id_staff",$data['id_staff']);
    return $this->db->update("staff",$data);
  }

  function uploadFILE(){
    //esto es por origen de almacenamiento de imagenes
    $targetDir = './assets/images/staff/';
    $uploadFile = '';
    $upload_error = 'Error al cargar archivo...';

    if (!empty($_FILES['input-file-load'])) {
      $targetDir = $targetDir . str_replace(" ","_", basename( $_FILES['input-file-load']["name"]));
      $uploadOk=1;  
      // Check if file already exists
      if (file_exists($targetDir . $_FILES['input-file-load']["name"])) {
        //echo "Archivo ya existente...";
        $uploadOk = 0;
      }
            
      // Check file size
      if ($_FILES['input-file-load']["size"] > 60242880) {
        //echo "Documento demasiado grande";
        $uploadOk = 0;
      }
                
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk==0) {
        //echo "El archivo no pudo se subido...";
      // if everything is ok, try to upload file
      } else {
        if (move_uploaded_file($_FILES['input-file-load']["tmp_name"], $targetDir)){
          $uploadFile = $_FILES['input-file-load']["name"];
          //echo json_encode(array('success'=>true,'file'=>$uploadFile));
        }
      }
      /*
        the code for file upload;
        $upload_success – becomes "true" or "false" if upload was unsuccessful;
        $upload_error – an error message of if upload was unsuccessful;
      */
    }//else echo $upload_error;
    return $uploadFile;
  }
}
?>