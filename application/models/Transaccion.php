<?php
Class Transaccion extends CI_Model{
  public function __construct(){
    parent::__construct();

    $this->load->database();
  }

  function insertTransaccion($tran){
    $res = $this-> db ->insert('transaccion',$tran);
    return $res;
  }

  function updateTransaccionState($d){
    $this->db->where("id_transaccion",$d['id_transaccion']);
    return $this->db->update("transaccion",$d);
  }

  function getTransaccionByUser($id,$by){
  	$this->db->select('*');
    $this->db->from('productos');
    if($by!='RECIENTE')$this->db->where('transaccion.estado',$by);
    $this->db->where('transaccion.id_vendedor',$id);
    $this->db->join('transaccion','transaccion.id_producto=productos.id_producto', 'left');
    $this->db->join('usuario','transaccion.id_comprador=usuario.id_usuario', 'left');
    $this->db->join('direcciones','transaccion.id_dir=direcciones.id_dir', 'left');
    $this->db->join('estados','direcciones.id_estado=estados.id_estado', 'left');
    $this->db->order_by('transaccion.id_transaccion','desc');

    $query = $this->db->get();
    return $query->result();
  }

  function getListPaqueteria(){
    $this->db->select('*');
    $this->db->from('paqueterias');

    $query = $this->db->get();
    return $query->result_array();
  }

}
?>