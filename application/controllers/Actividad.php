<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividad extends CI_Controller {

	function __construct() {
		parent::__construct();

		// Load url helper
		$this->load->helper('url');
		// Load Staff Model - modelo donde haras las consultas de base de datos 
		$this->load->model('usuario','',TRUE);
		$this->load->model('producto','',true);
		$this->load->model('transaccion','',true);
		$this->load->model('generic','',true);
		$this->load->model('tienda','',true);
		$this->load->model('estados','',true);
	}

	public function index(){
		if($this->session->userdata('logged_in')){
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    
		    $data['scripts'] = array('publico/jsPublico','actividad/jsActividad','publico/jsFacebook');
		    $data['css'] = array('actividad');
			$this->load->view('template/vHeader',$data);
			if($data['perfil'][0]->tienda > -1){
				$this->load->view('actividad/vSideMenu',$data['perfil']);
				$this->load->view('actividad/vActPanel',$data['perfil']);	
			}else $this->load->view('actividad/vRestructure');
			
			$this->load->view('template/vFooter');
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('../index.php', 'refresh');
	   }
	}
	// INICIO DE METODOS PARA FUNCIONALIDAD EN COMPRAS
	public function compras(){
		if($this->session->userdata('logged_in')){
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    
		    $data['scripts'] = array('publico/jsPublico','actividad/jsCompras','publico/jsFacebook');
		    $data['css'] = array('actividad');
			$this->load->view('template/vHeader',$data);
			$this->load->view('actividad/vSideMenu');
			$this->load->view('actividad/vCompPanel',$data['perfil']);
			$this->load->view('template/vFooter');
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('../index.php', 'refresh');
	   }
	}
	// FIN DE METODOS PARA COMPRAS

	// INICIO DE METODOS PARA FUNCIONALIDAD EN COMPRAS
	public function ventas(){
		if($this->session->userdata('logged_in')){
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    $data['marcas'] = $this->producto->getMarcas();
		    $data['categorias'] = $this->producto->getCategorias();
		    $data['subcategorias'] = $this->producto->getSubcategorias(1);
		    
		    $data['scripts'] = array('publico/jsPublico','actividad/jsVentas','jsDropzone','publico/jsFacebook');
		    $data['css'] = array('actividad');
			$this->load->view('template/vHeader',$data);

			if($data['perfil'][0]->tienda > -1){
				$this->load->view('actividad/vSideMenu');
				if($data['perfil'][0]->tienda > 0) $data['tienda'] = $this->tienda->getSingleTiendaByUserId($data['perfil'][0]->id_usuario);
				$this->load->view('actividad/vVentPanel',$data);
			}else $this->load->view('actividad/vRestructure');

			$this->load->view('template/vFooter');
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('../index.php', 'refresh');
	   }
	}

	public function cambiarSubcategoria(){
		$res = $this->producto->getSubcategorias($this->input->post('id'));
		echo json_encode($res);
	}

	public function subirProducto(){
		$data = array(
			'nom_producto'=>$this->input->post('nom_producto'),
			'descripcion'=>nl2br($this->input->post('descripcion')),
			'precio'=>$this->input->post('precio'),
			'condicion'=>$this->input->post('condicion'),
			'imagenes'=>$this->input->post('archivo'),
			'video'=>$this->input->post('video'),
			'id_subcatego'=>$this->input->post('id_subcatego'),
			'id_categoria'=>$this->input->post('id_categoria'),
			'id_marcas'=>$this->input->post('id_marcas')
		);
		$res = $this->producto->insertProducto($data);
		if($res){
			if(!empty($this->input->post('existencia'))){
				$ware = array(
					'id_tienda'=>$this->input->post('id_tienda'),
					'id_producto'=>$res->id_producto,
					'existencia'=>$this->input->post('existencia'),
					'vendidos'=>'',
					'total'=>'',
					'cancelados'=>'',
					'camino'=>''
				);
				$rr = $this->tienda->insertIntoBodega($ware);
			}
			$dest = array(
				'estado'=>'VENTA',
				'guia'=>'',
				'paqueteria'=>'',
				'id_producto'=>$res->id_producto,
				'id_vendedor'=>$this->input->post('id_vendedor'),
				'id_comprador'=>'',
				'id_dir'=>'',
				'obs_vendedor'=>'',
				'obs_comprador'=>''
			);
			$r = $this->transaccion->insertTransaccion($dest);
			echo $r;
			
		}else echo false;
	}

	public function productosUsuario(){
		$res['productos'] = $this->transaccion->getTransaccionByUser($this->input->post('id'),$this->input->post('sort'));
		echo $this->load->view('actividad/vProdList',$res,true);
	}

	public function abrirEditProdForm(){
		$res['perfil'] = $this->usuario->getUserById($this->input->post('myID'));
		$res['producto'] = $this->producto->getSingleProduct($this->input->post('id'));
		$res['marcas'] = $this->producto->getMarcas();
		$res['categorias'] = $this->producto->getCategorias();
		$res['subcategorias'] = $this->producto->getSubcategorias($res['producto'][0]->id_categoria);
		echo $this->load->view('actividad/vEditProdForm',$res,true);
	}

	public function editarProducto(){
		if($this->input->post('video')=='')$video = $this->input->post('oldVideo');
		else{
			$video = $this->input->post('video');
			unlink('./assets/img/products/video/'.$this->input->post('oldVideo'));
		}
		if($this->input->post('toBeRemoved')!=''){
			$img = explode('/', $this->input->post('toBeRemoved'));
			for ($i=1; $i <count($img); $i++) { 
				unlink('./assets/img/products/'.$img[$i]);
			}
		}
		$data = array(
			'id_producto'=>$this->input->post('id_producto'),
			'nom_producto'=>$this->input->post('nom_producto'),
			'descripcion'=>nl2br($this->input->post('descripcion')),
			'precio'=>$this->input->post('precio'),
			'condicion'=>$this->input->post('condicion'),
			'imagenes'=>$this->input->post('oldArchivo').$this->input->post('archivo'),
			'video'=>$video,
			'id_subcatego'=>$this->input->post('id_subcatego'),
			'id_categoria'=>$this->input->post('id_categoria'),
			'id_marcas'=>$this->input->post('id_marcas')
		);
		$res = $this->producto->updateProducto($data);
		echo $res;
	}

	public function listarPaqueterias(){
		$res = $this->transaccion->getListPaqueteria();
		echo json_encode($res);
	}

	public function cambiarEstadoTransaccion(){
		$data = array(
			'id_transaccion'=>$this->input->post('id_transaccion'),
			'estado'=>$this->input->post('estado')
		);
		switch($this->input->post('estado')){
			case 'CANCELADO':
				$data['obs_vendedor'] = $this->input->post('motivo').' - '.$this->input->post('observaciones');
				break;
			case 'EN CAMINO':
				$data['paqueteria'] = $this->input->post('paqueteria');
				$data['guia'] = $this->input->post('guia');
				break;
		}
		$res = $this->transaccion->updateTransaccionState($data);
		echo $res;
	}

	public function enviarMensajeComprador(){
		// te llegara id_comprador $this->input->post('id_comprador');
		// de ahi saca los datos que necesites para enviar correo y posible whatss
		// quizas sacar remitente...
		$data = array(
			'comprador'=>$this->usuario->getUserById($this->input->post('id_comprador')),
			'vendedor'=>$this->usuario->getUserById($this->input->post('id_vendedor')),
			'mensaje'=>$this->input->post('asunto'),
			'asunto'=>'Contacto con el comprador'
		);
		$res = $this->generic->sendEmail($data);
		echo $res;
	}

	public function removeUploaded(){
		unlink($this->input->post('dir').$this->input->post('img'));
		if(strpos($_FILES['file']['name'], '.mp4')===false)
			unlink($this->input->post('dir').'thumbs/'.$this->input->post('img'));
	}

	public function upload(){

		$targetDir = (strpos($_FILES['file']['name'], '.mp4')===false)?'./assets/img/products/':'./assets/img/products/video/';
	    $uploadFile = '';
	    $upload_error = 'Error al cargar archivo...';

	    if (!empty($_FILES['file'])) {
	      $targetDir = $targetDir . str_replace(" ","_", basename( $_FILES['file']["name"]));
	      $uploadOk=1;  
	      // Check if file already exists
	      if (file_exists($targetDir . $_FILES['file']["name"])) {
	        //echo "Archivo ya existente...";
	        $uploadOk = 0;
	      }          
	      // Check file size
	      if ($_FILES['file']["size"] > 60242880) {
	        //echo "Documento demasiado grande";
	        $uploadOk = 0;
	      }          
	      // Check if $uploadOk is set to 0 by an error
	      if ($uploadOk==0) {
	        //echo "El archivo no pudo se subido...";
	      // if everything is ok, try to upload file
	      } else {
	        if (move_uploaded_file($_FILES['file']["tmp_name"], $targetDir)){
	          $uploadFile = $_FILES['file']["name"];
	          if(strpos($_FILES['file']['name'], '.mp4')===false) $this->makeThumbnails($uploadFile);
	          
	        }
	      }
	    }
	    echo $uploadFile;
	}

	public function uploadLogo(){

		$targetDir = './assets/img/tiendas/';
	    $uploadFile = '';
	    $upload_error = 'Error al cargar archivo...';

	    if (!empty($_FILES['file'])) {
	      $targetDir = $targetDir . str_replace(" ","_", basename( $_FILES['file']["name"]));
	      $uploadOk=1;  
	      // Check if file already exists
	      if (file_exists($targetDir . $_FILES['file']["name"])) {
	        //echo "Archivo ya existente...";
	        $uploadOk = 0;
	      }          
	      // Check file size
	      if ($_FILES['file']["size"] > 60242880) {
	        //echo "Documento demasiado grande";
	        $uploadOk = 0;
	      }          
	      // Check if $uploadOk is set to 0 by an error
	      if ($uploadOk==0) {
	        //echo "El archivo no pudo se subido...";
	      // if everything is ok, try to upload file
	      } else {
	        if (move_uploaded_file($_FILES['file']["tmp_name"], $targetDir)){
	          $uploadFile = $_FILES['file']["name"];
	          if(strpos($_FILES['file']['name'], '.mp4')===false) $this->makeThumbnailsLogo($uploadFile);
	          
	        }
	      }
	    }
	    echo $uploadFile;
	}

	public function makeThumbnailsLogo($img){
		$thumbnail_width = 200;
	    $thumbnail_height = 200;
	    $arr_image_details = getimagesize('./assets/img/tiendas/'.$img);
	    $original_width = $arr_image_details[0];
	    $original_height = $arr_image_details[1];
	    if ($original_width > $original_height) {
	        $new_width = $thumbnail_width;
	        $new_height = intval($original_height * $new_width / $original_width);
	        $thumbnail_height = $new_height;
	    } else {
	        $new_height = $thumbnail_height;
	        $new_width = intval($original_width * $new_height / $original_height);
	        $thumbnail_width = $new_width;
	    }
	    $dest_x = intval(($thumbnail_width - $new_width) / 2);
	    $dest_y = intval(($thumbnail_height - $new_height) / 2);
	    if ($arr_image_details[2] == IMAGETYPE_GIF) {
	        $imgt = "ImageGIF";
	        $imgcreatefrom = "ImageCreateFromGIF";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_JPEG) {
	        $imgt = "ImageJPEG";
	        $imgcreatefrom = "ImageCreateFromJPEG";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_PNG) {
	        $imgt = "ImagePNG";
	        $imgcreatefrom = "ImageCreateFromPNG";
	    }
	    if ($imgt) {
	        $old_image = $imgcreatefrom('./assets/img/tiendas/'.$img);
	        //$new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
	        $imgt($new_image, './assets/img/tiendas/thumbs/'.$img);
	    }
	}

	public function makeThumbnails($img){
		$thumbnail_width = 160;
	    $thumbnail_height = 160;
	    $arr_image_details = getimagesize('./assets/img/products/'.$img);
	    $original_width = $arr_image_details[0];
	    $original_height = $arr_image_details[1];
	    if ($original_width > $original_height) {
	        $new_width = $thumbnail_width;
	        $new_height = intval($original_height * $new_width / $original_width);
	        $thumbnail_height = $new_height;
	    } else {
	        $new_height = $thumbnail_height;
	        $new_width = intval($original_width * $new_height / $original_height);
	        $thumbnail_width = $new_width;
	    }
	    $dest_x = intval(($thumbnail_width - $new_width) / 2);
	    $dest_y = intval(($thumbnail_height - $new_height) / 2);
	    if ($arr_image_details[2] == IMAGETYPE_GIF) {
	        $imgt = "ImageGIF";
	        $imgcreatefrom = "ImageCreateFromGIF";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_JPEG) {
	        $imgt = "ImageJPEG";
	        $imgcreatefrom = "ImageCreateFromJPEG";
	    }
	    if ($arr_image_details[2] == IMAGETYPE_PNG) {
	        $imgt = "ImagePNG";
	        $imgcreatefrom = "ImageCreateFromPNG";
	    }
	    if ($imgt) {
	        $old_image = $imgcreatefrom('./assets/img/products/'.$img);
	        //$new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	        $new_image = imagecreatetruecolor($thumbnail_width, $thumbnail_height);
	        imagecopyresized($new_image, $old_image, $dest_x, $dest_y, 0, 0, $new_width, $new_height, $original_width, $original_height);
	        $imgt($new_image, './assets/img/products/thumbs/'.$img);
	    }
	}
	// FIN DE METODOS PARA VENTAS

	//INICIO DE METODOS PARA FUNCIONALIDAD SOLICITUD TIENDA
	public function solicitudtienda(){
		if($this->session->userdata('logged_in')){
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    
		    $data['scripts'] = array('publico/jsPublico','actividad/jsSolicitudTienda','publico/jsFacebook');
		    $data['css'] = array('actividad');
			$this->load->view('template/vHeader',$data);
			$this->load->view('actividad/vSolicitudTienda',$data);
			$this->load->view('template/vFooter');
	   }
	   else{
	    	//If no session, redirect to login page
	    	redirect('../index.php', 'refresh');
	   }
	}

	public function solicitarPlan(){
		$data = array(
	      	'nombre'=>$this->input->post('name'),
	      	'whatsapp'=>$this->input->post('phone'),
	      	'correo'=>$this->input->post('email'),
	      	'mensaje'=>$this->input->post('message'),
	      	'plan'=>$this->input->post('plan'),
	    );
		$res = $this->generic->requestStorePlan($data);
		echo $res;
	}
	//FIN DE METODOS PARA SOLICITUD TIENDA

	// INICIO DE METODOS PARA TIENDA
	public function tienda(){
		if($this->session->userdata('logged_in')){
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    $data['tienda'] = $this->tienda->getSingleTiendaByUserId($data['perfil'][0]->id_usuario);
		    $data['estados']= $this->estados->getEstados();

		    $data['scripts'] = array('publico/jsPublico','actividad/jsTienda','publico/jsFacebook','jsDropzone');
		    $data['css'] = array('actividad');
			$this->load->view('template/vHeader',$data);

			if($data['perfil'][0]->tienda > -1){
				$this->load->view('actividad/vSideMenu');
				$data['productos'] = $this->tienda->getProductosByTienda($data['tienda'][0]->id_tienda);
				$this->load->view('actividad/vTiendaPanel',$data);
			}else $this->load->view('actividad/vRestructure');
			
			$this->load->view('template/vFooter');
	   }
	   else{
	    	//If no session, redirect to login page
	    	redirect('../index.php', 'refresh');
	   }
	}

	public function crearTienda(){
		$data = array(
			'nom_tienda'=>$this->input->post('nom_tienda'),
			'url'=>$this->input->post('url'),
			'domicilio'=>$this->input->post('domicilio'),
			'telefono'=>$this->input->post('telefono'),
			'facebook'=>$this->input->post('facebook'),
			'logo'=>$this->input->post('logo'),
			'id_usuario'=>$this->input->post('id_usuario'),
			'id_estado'=>$this->input->post('id_estado'),
			'municipio'=>$this->input->post('municipio')
		);
		$res = $this->tienda->insertTienda($data);
		echo $res;
	}

	public function editarTienda(){
		if($this->input->post('logo')!=''){
			unlink('./assets/img/tiendas/'.$this->input->post('oldLogo'));
			unlink('./assets/img/tiendas/thumbs/'.$this->input->post('oldLogo'));
			$logo = $this->input->post('logo');
		}else $logo = $this->input->post('oldLogo');
		$data = array(
			'nom_tienda'=>$this->input->post('nom_tienda'),
			'url'=>$this->input->post('url'),
			'domicilio'=>$this->input->post('domicilio'),
			'telefono'=>$this->input->post('telefono'),
			'facebook'=>$this->input->post('facebook'),
			'logo'=>$logo,
			'id_usuario'=>$this->input->post('id_usuario'),
			'id_estado'=>$this->input->post('id_estado'),
			'municipio'=>$this->input->post('municipio')
		);
		$res = $this->tienda->updateTienda($data);
		echo $res;
	}

	public function cambiarExistencia(){
		$data = array(
			'id_tienda'=>$this->input->post('id_tienda'),
			'id_producto'=>$this->input->post('id_producto'),
			'existencia'=>$this->input->post('existencia')
		);
		$res = $this->tienda->updateExistencia($data);
		echo $res;
	}

	public function enviarSolicitudPlan(){
		$data = array(
			'tendero'=>$this->usuario->getUserById($this->input->post('id_usuario')),
			'mensaje'=>'Plan actual: '.$this->input->post('planActual').'<br>Motivo: '.$this->input->post('motivo'),
			'asunto'=>'Cambio de plan a '.$this->input->post('planNuevo')
		);
		$res = $this->generic->sendChangeRequest($data);
		echo $res;
	}
	//FIN DE METODOS PARA TIENDA
}