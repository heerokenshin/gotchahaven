<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
		$this->load->model('producto','',true);
		/*$this->session->unset_userdata('logged_in');
		   	session_destroy();*/
	}

	// INICIO DE METODOS PARA VENTAS
	public function ventas(){
		if($this->session->userdata('logged_in')){
			$session_data  = $this->session->userdata('logged_in');

			$data['facebook'] = $session_data['facebook'];
			$data['name'] = $session_data['nombres'];
		}
		$data['scripts'] = array('publico/jsPublico','compras/jsVentas','publico/jsFacebook');
		$data['categorias'] = $this->producto->getCategorias();
		$data['marcas'] = $this->producto->getMarcas();

		if($this->session->userdata('cart')) $data['carrito'] = $this->session->userdata('cart');

		$this->load->view('template/vHeader',$data);
		$this->load->view('compras/vBarraBusqueda',$data);
		$this->load->view('compras/vVentas',$data);
		$this->load->view('template/vFooter');
	}

	public function listarProductosAlInicio(){
		$data['total'] = $this->producto->getTotalSaleProducts();
		$data['productos'] = $this->producto->getRecentSaleProductos(30);
		echo $this->load->view('compras/vVentasResList',$data,true);
	}

	public function listarProductos(){
		// te llegaran 5 variables:
		// palabras $this->input->post('palabras'); 
		// OPINO QUE CUANDO SE META NUEVAS PALABRAS, LAS VARIABLES DEBAJO SE RESETEEN, ESPERO TUS COMENTARIOS
		//
		// categoria $this->input->post('cat');
		// subcategoria $this->input->post('sub');
		// marca $this->input->post('marca');
		// sort (bueno no es sort como tal pero se entiende) $this->input->post('sort');
		echo false;
	}

	public function mostrarSubcategorias(){
		$res = $this->producto->getSubcategoriasSorter($this->input->post('id'));
		echo json_encode($res);
	}
	// FIN DE METODOS PARA VENTAS

	public function producto(){
		if($this->session->userdata('logged_in')){
			$session_data  = $this->session->userdata('logged_in');

			$data['facebook'] = $session_data['facebook'];
			$data['name'] = $session_data['nombres'];
		}

		if($this->session->userdata('cart')) $data['carrito'] = $this->session->userdata('cart');
		
		$data['scripts'] = array('publico/jsPublico','compras/jsVentas','publico/jsFacebook');
		$data['categorias'] = $this->producto->getCategorias();
		$data['marcas'] = $this->producto->getMarcas();

		$data['producto'] = $this->producto->getSingleProduct($this->input->get('id'));
		$data['subcategorias'] = $this->producto->getSubcategorias($data['producto'][0]->id_categoria);
		$this->load->view('template/vHeader',$data);
		$this->load->view('compras/vBarraBusqueda',$data);
		$this->load->view('compras/vProducto',$data);
		$this->load->view('template/vFooter');

	}

	public function listarCarrito(){
		if($this->session->userdata('logged_in')){
			$res = $this->producto->getSingleProductWithTransaccion($this->input->post('prod'));
			$data = array(
				'id_producto'=>$res[0]->id_producto,
				'nom_producto'=>$res[0]->nom_producto,
				'id_transaccion'=>$res[0]->id_transaccion,
				'imagenes'=>$res[0]->imagenes
			);
			if($this->session->userdata('cart')){
				$oldCart = $this->session->userdata('cart');
				array_push($oldCart, $data);
				$this->session->set_userdata('cart', $oldCart);
			}else{ 
				$oldCart = array($data);
				$this->session->set_userdata('cart',$oldCart);
			}
			
			echo json_encode($data);
		}else{
			$data = array('site'=>site_url('perfil/registrarse'));
			echo json_encode($data);
		}
	}

	public function quitarProductoCarrito(){
		$cart = $this->session->userdata('cart');
		$clave = array_search($this->input->post('prod'), array_column($cart, 'id_producto'));
		if(count($cart)>1){
			array_splice($cart, $clave, $clave);
			$this->session->unset_userdata('cart');
			$this->session->set_userdata('cart', $cart);
		}else $this->session->unset_userdata('cart');
		echo true;
	}

	public function caja(){

	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo true;
	}
}