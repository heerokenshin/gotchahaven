<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Portada extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
	}
	public function index(){
		//dummy creation of session variables to avoid login - DEV PURPOSES ONLY
		$sess_array = array(
			'facebook' => 1124587847568668,
			'nombres' => 'Heero'
		);
		$this->session->set_userdata('logged_in', $sess_array);
			     

		header("Access-Control-Allow-Origin: localhost");
		header("Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
		header("Access-Control-Allow-Methods: GET, PUT, POST");
		if($this->session->userdata('logged_in')){
			$session_data  = $this->session->userdata('logged_in');

			$data['facebook'] = $session_data['facebook'];
			$data['name'] = $session_data['nombres'];
		}
		$data['scripts'] = array('publico/jsPublico','publico/jsFacebook');
		$this->load->view('template/vHeader',$data);
		$this->load->view('vPortada');
		$this->load->view('publico/vNoticias');
		$this->load->view('publico/vCompra');
		$this->load->view('template/vFooter');
	}
	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo true;
	}
}