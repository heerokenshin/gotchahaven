<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	function __construct() {
		parent::__construct();

		// Load url helper
		$this->load->helper('url');
		// Load Staff Model - modelo donde haras las consultas de base de datos
		$this->load->model('usuario','',TRUE);
		$this->load->model('estados','',true);
	}

	public function index(){
		if($this->session->userdata('logged_in')){
			$data['firstTime'] = false;
		    $session_data = $this->session->userdata('logged_in');

		    $data['facebook'] = $session_data['facebook'];
		    $data['name'] = $session_data['nombres'];
		    $data['perfil'] = $this->usuario->getLoggedUser($session_data['facebook']);
		    $data['direcciones'] = $this->usuario->getUserAddress($data['perfil'][0]->id_usuario);
		    $data['estados'] = $this->estados->getEstados();
		    //nueva linea para obtener cuentas... no se si tengas que crear modelo de cuentas
		    $data['cuentas'] = $this->usuario->getUserAccounts($data['perfil'][0]->id_usuario);
		    $data['scripts'] = array('publico/jsPublico','publico/jsPerfil','publico/jsFacebook');
			$this->load->view('template/vHeader',$data);
			$this->load->view('usuario/vPerfilConfig',$data['perfil']);
			$this->load->view('template/vFooter');
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('../index.php', 'refresh');
	   }
	}
	public function nuevo(){
		$data['id'] = $_GET['id'];
		$data['fname'] = $_GET['fname'];
		$data['lname'] = $_GET['lname'];
		$data['firstTime'] = true;

		$r = $this->usuario->getLoggeduser($data['id']);
		if($r){
				$sess_array = array();
			     foreach($r as $row){
			       $sess_array = array(
			         'facebook' => $row->facebook,
			         'nombres' => $row->nombres
			       );
			       $this->session->set_userdata('logged_in', $sess_array);
			     }
			     if(isset($_GET['idprod'])) redirect(site_url('compras/producto').'?id='.$_GET['idprod'].'&pr='.$_GET['pr'].'&tr='.$_GET['tr'],'refresh');
			     else if(isset($_GET['red'])){
			     	$r = str_replace('-', '?', $_GET['red']);
			     	$r = str_replace('@', '&', $r);
			     	redirect(site_url(str_replace('_', '/', $r)));
			     }else redirect('http://www.gotchahaven.com/zanzibar','refresh');

		}else{
			if(isset($_GET['idprod'])) $data['redirectURL'] = site_url('compras/producto').'?id='.$_GET['idprod'].'&pr='.$_GET['pr'].'&tr='.$_GET['tr'];
			$data['scripts'] = array('publico/jsPublico');
			$this->load->view('template/vHeader',$data);
			$this->load->view('usuario/vPerfilConfig',$data);
			$this->load->view('template/vFooter');
		}
	}

	public function agregarDireccionForm(){
		$res['estados'] = $this->estados->getEstados();
		echo $this->load->view('usuario/vDireccion',$res,true);
	}

	public function listaEstados(){
		$res = $this->estados->getEstados();
		echo json_encode($res);
	}

	public function obtenerEstado(){
		$res = $this->estados->getSingleState($this->input->post('id'));
		echo json_encode($res);
	}

	public function agregarPerfilYDireccion(){
		$perfil = array(
			'nombres'=>$this->input->post('nombres'),
			'apellidos'=>$this->input->post('apellidos'),
			'correo'=>$this->input->post('correo'),
			'whatsapp'=>$this->input->post('whatsapp'),
			'facebook'=>$this->input->post('facebook'),
			'reputacion'=>1,
			'password'=>''
		);
		$res = $this->usuario->insertUsuario($perfil);
		if($this->input->post('calle')){
			if($res){
				$dir = array(
					'calle'=>$this->input->post('calle'),
					'colonia'=>$this->input->post('colonia'),
					'cp'=>$this->input->post('cp'),
					'municipio'=>$this->input->post('municipio'),
					'id_estado'=>$this->input->post('estado')
				);
				$r = $this->usuario->getSingleUserByFacebook($this->input->post('facebook'));
				$dir['id_usuario'] = $r[0]['id_usuario'];
				$result = $this->usuario->insertDireccion($dir);
				echo $result;
			}
		}
	}

	public function editarPerfil(){
		$perfil = array(
			'id_usuario'=>$this->input->post('id_usuario'),
			'nombres'=>$this->input->post('nombres'),
			'apellidos'=>$this->input->post('apellidos'),
			'correo'=>$this->input->post('correo'),
			'whatsapp'=>$this->input->post('whatsapp')
		);
		$res = $this->usuario->updateUsuario($perfil);
		echo $res;
	}

	public function agregarNuevaDireccion(){
		//metodo par aagregar direccion a usuario
		$dir = array(
			'calle'=>$this->input->post('calle'),
			'colonia'=>$this->input->post('colonia'),
			'cp'=>$this->input->post('cp'),
			'municipio'=>$this->input->post('municipio'),
			'id_estado'=>$this->input->post('estados'),
			'id_usuario'=>$this->input->post('id_usuario')
		);
		echo $this->usuario->insertDireccion($dir);
	}

	public function listarDirecciones(){
		//metodo para listar direcciones con el id_usuario
		//recibes post con nombre "id" $this->input->post('id');
		$res = $this->usuario->getUserAddress($this->input->post('id'));
		echo json_encode($res);
	}

	public function formularioEditarDireccion(){
		//no se si aqui tengas que crear un modelo de nombre direccion o en la misma de usuario
		$data['direccion']=$this->usuario->getSingleAddress($this->input->post('id_usuario'),$this->input->post('id_dir'));
		$data['estados']=$this->estados->getEstados();
		echo $this->load->view('usuario/vEditarDireccion',$data,true);
	}

	public function editarDireccion(){
		$dir = array(
			'id_dir'=>$this->input->post('id_dir'),
			'calle'=>$this->input->post('calle'),
			'colonia'=>$this->input->post('colonia'),
			'cp'=>$this->input->post('cp'),
			'municipio'=>$this->input->post('municipio'),
			'id_estado'=>$this->input->post('estados'),
			'id_usuario'=>$this->input->post('id_usuario')
		);
		echo $this->usuario->updateDireccion($dir);
	}

	public function eliminarDireccion(){
		$id = $this->input->post('id');
		echo $this->usuario->deleteDireccion($id);
	}

	public function listarCuentas(){
		//metodo para obtener cuentas del usuario
		$res = $this->usuario->getUserAccounts($this->input->post('id'));
		echo json_encode($res);
	}

	public function agregarNuevaCuenta(){
		$data = array(
			'no_cuenta'=>$this->input->post('no_cuenta'),
			'clabe'=>$this->input->post('clabe'),
			'banco'=>$this->input->post('banco'),
			'titular'=>$this->input->post('titular'),
			'id_usuario'=>$this->input->post('id_usuario')
		);
		$res = $this->usuario->insertCuenta($data);
		echo $res;
	}

	public function formularioEditarCuenta(){
		$data['cuentas'] = $this->usuario->getSingleAccount($this->input->post('id_usuario'),$this->input->post('id_cuenta'));
		echo $this->load->view('usuario/vEditarCuenta',$data,true);
	}

	public function editarCuenta(){
		$data = array(
			'id_cuenta'=>$this->input->post('id_cuenta'),
			'no_cuenta'=>$this->input->post('no_cuenta'),
			'clabe'=>$this->input->post('clabe'),
			'banco'=>$this->input->post('banco'),
			'titular'=>$this->input->post('titular'),
			'id_usuario'=>$this->input->post('id_usuario')
		);

		echo $this->usuario->updateCuenta($data);
	}

	public function eliminarCuenta(){
		$id = $this->input->post('id');
		echo $this->usuario->deleteCuenta($id);
	}

	public function registrarse(){
		$data['prodURL'] = '&idprod='.$this->input->get('id').'&pr='.$this->input->get('pr').'&tr='.$this->input->get('tr');
		$data['scripts'] = array('publico/jsPublico','publico/jsFacebook');
		$this->load->view('usuario/vRegistrarse',$data);
		$this->load->view('template/vFooter');
	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo 'sali de perfil';
	}

	public function dummyForm($aux = 1){
		if($this->session->userdata('logged_in')){
			$session_data = $this->session->userdata('logged_in');
		  $perfil = $this->usuario->getLoggedUser($session_data['facebook'])[0];
		  $dir = $this->usuario->getSingleAddress($aux);
		  $direcciones = $this->usuario->getUserAddress($perfil->id_usuario);
		  echo $dir[0]['id_dir'];
		  /*
			echo '<form action="../editarDireccion" method="post">
					<input type="hidden" value="'.$direcciones[2]['id_dir'].'" name="id_dir">
					<input type="hidden" value="'.$perfil->id_usuario.'" name="id_usuario">
					Calle: <input type="text" value="'.$direcciones[2]['calle'].'" name="calle"><br>
					Colonia: <input type="text" value="'.$direcciones[2]['colonia'].'" name="colonia"><br>
					Municipio: <input type="text" value="'.$direcciones[2]['municipio'].'" name="municipio"><br>
					CP:<input type="text" value="'.$direcciones[2]['cp'].'" name="cp"><br>
					Estado:<input type="text" value="'.$direcciones[2]['id_estado'].'" name="id_estado"><br>
					<button type="submit">Guardar</button>
			</form>';*/
	   }
	   else{
	     //If no session, redirect to login page
	     redirect('../index.php', 'refresh');
	   }
	}
}