<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Noticias extends CI_Controller {

	function __construct() {
		parent::__construct();
		// Load url helper
		$this->load->helper('url');
	}
	public function index(){
		//$this->load->helper('url');
		header("Access-Control-Allow-Origin: localhost");
		header("Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With");
		header("Access-Control-Allow-Methods: GET, PUT, POST");
		if($this->session->userdata('logged_in')){
			$session_data  = $this->session->userdata('logged_in');

			$data['facebook'] = $session_data['facebook'];
			$data['name'] = $session_data['nombres'];
		}
		$data['scripts'] = array('publico/jsPublico','publico/jsFacebook');
		$this->load->view('template/vHeader',$data);
		$this->load->view('publico/vNoticias');
		$this->load->view('template/vFooter');
	}
	public function logout(){
		$this->session->unset_userdata('logged_in');
	   	session_destroy();
	   	echo true;
	}
}