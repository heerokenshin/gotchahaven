<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vender extends CI_Controller {

  function __construct() {
    parent::__construct();

    $this->load->helper('url');

    $this->load->model('producto','',true);
  }

  public function index() {
    $this->productForm();
  }

  public function nuevo_producto() {
    if(isset($_GET["nombre"])) {
      foreach($_GET as $key => $value){
        echo $key . ": " . $value . "<br />\r\n";
      }
    }
    else {
      echo '<h2>Please enter a valid product</h2>';
    }

    echo '<a href=".">Volver</a>';
  }

  public function editarProducto(){
    $prod = array(
      'id_producto'=>$this->input->post('id_producto'),
      'nom_producto'=>$this->input->post('nom_producto'),
      'descripcion'=>$this->input->post('descripcion'),
      'precio'=>$this->input->post('precio'),
      'condicion'=>$this->input->post('condicion'),
      'imagenes'=>$this->input->post('imagenes'),
      'video'=>$this->input->post('video'),
      'id_subcatego'=>$this->input->post('id_subcatego'),
      'id_categoria'=>$this->input->post('id_categoria'),
      'id_marcas'=>$this->input->post('id_marcas')
    );

    $this->producto->updateProducto($prod);
  }

  public function eliminarProducto(){
    $this->producto->deleteProducto($this->input->post('id_producto'));
  }

  function productForm() {
    echo '
    <div>
      <h2>Crea un nuevo producto</h2>
      <form action="vender/editarProducto" method="post">
      <input type="hidden" value="4" name="id_producto">
      Nombre: <input type="text" name="nom_producto"><br>
      Descripcion: <textarea name="descripcion"></textarea><br>
      Precio: <input type="number" name="precio"><br>
      Condiciones: <textarea name="condicion"></textarea><br>
      Imagenes: <input type="text" name="imagenes" value="/10481968_796988963716836_147673611644678625_o.jpg"><br>
      Video: <input type="file" name="video"><br>
      Marca: <select name="id_marcas">
            <option value="1">Patito</option></select><br>
      Categoria: <select name="id_categoria">
            <option value="1">Marcadoras</option></select><br>
      Subcategoria: <select name="id_subcatego">
            <option value="1">Mamalona</option></select><br>

      <input type="submit" value="Guardar">
      </form>
      <form action="vender/eliminarProducto" method="post">
      <input type="hidden" value="4" name="id_producto">
      <input type="submit" value="Borrar">
      </form>
    </div>
    ';
  }
}

?>