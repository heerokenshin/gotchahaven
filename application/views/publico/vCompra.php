<section id="compra">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Servicios</h2>
                <h3 class="section-subheading text-muted">Gotcha Haven ofrece los siguientes servicios:</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-sm-4 col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Artículos en venta</h4>
                <p class="text-muted">Busca marcadoras, protecciones, pintura, accesorios y más para paintball y airsoft.<br> Los artículos vienen de gotcheros para gotcheros!</p>
                <a href="<?php echo site_url('compras/ventas');?>" class="btn btn-primary">ENTRAR</a>
            </div>
            <div class="col-sm-4 col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-gavel fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Subastas</h4>
                <p class="text-muted">Encontrarás listados de artículos en negociación, quizá una buena ganga!<br>¡QUE GANE EL MEJOR POSTOR!</p>
                <a href="<?php echo site_url('compras/subastas');?>" class="btn btn-primary">ENTRAR</a>
            </div>
            <div class="col-sm-4 col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fa fa-circle fa-stack-2x text-primary"></i>
                    <i class="fa fa-ticket fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">Sorteos y rifas</h4>
                <p class="text-muted">Participa para llevarte artículos con las rifas hechas por gotcheros y tiendas.<br>¡QUE LA SUERTE ESTÉ DE TU LADO!</p>
                <p class="text-danger"><i class="fa fa-wrench"></i> En construcción <i class="fa fa-wrench"></i></p>
            </div>
        </div>
    </div>
</section>