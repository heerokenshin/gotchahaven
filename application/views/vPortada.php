<!-- Header -->
    <header>
        <div class="shade">
            <div class="container">
                <div class="intro-text">
                    <div class="intro-lead-in">El refugio para paintball y airsoft</div>
                    <div class="intro-heading">COMPRA Y VENTA DE ARTÍCULOS PARA AMBOS DEPORTES!</div>
                    <a href="#noticias" class="page-scroll btn btn-xl">Conocer más</a>
                </div>
            </div>
        </div>
    </header>
    <?php echo $_SERVER["REQUEST_URI"]; ?>

	<section id="donativos">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Donativos</h2>
                    <h3 class="section-subheading text-muted">
                        Al dia de hoy, gotchahaven.com aún se encuentra en desarrollo.<br><br>
                        Nos encontramos recaudando fondos para seguir trabajando,<br> tu puedes contribuir comprando nuestra mercancía!
                    </h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <img style="display:block;width:100%;margin-bottom:50px;" src="<?php echo base_url();?>assets/img/gotchahavenlogo.png" alt="">
                    <h4 class="service-heading">Calcomanías</h4>
                    <p class="text-muted">Con un tamaño de 15 x 5 cm con el logotipo de nuestro sitio. <br><b>Precio: $10 MXN c/u</b></p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <img style="display:block;width:100%;margin-bottom:50px;" src="<?php echo base_url();?>assets/img/parche.png" alt="">
                    <h4 class="service-heading">Parches</h4>
                    <p class="text-muted">Con un tamaño de 15 x 5 cm con el logotipo del nuestro sitio. <br><b>Precio: $60 MXN c/u</b></p>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="teesCont">
                        <div class="teesSlider">
                            <img style="display:inline-block;vertical-align:top;" src="<?php echo base_url();?>assets/img/tshirtbalck.png" alt="">
                            <img style="display:inline-block;vertical-align:top;" src="<?php echo base_url();?>assets/img/shirtblack.png" alt="">
                        </div>
                    </div>
                    
                    <h4 class="service-heading">
                        Playeras 
                        <button id="0" class="btn btn-default selected">H</button>
                        <button id="100" class="btn btn-default">M</button>
                    </h4>
                    <p class="text-muted">Playera negra con el texto enfrente y el logo detrás, disponible en S, M, L, XL, XXL.<br><b>Precio: $200 MXN c/u</b></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h3 class="section-subheading text-muted" style="margin-bottom:20px;">
                        Todo donativo sera recibido en la siguiente cuenta:<br>
                        <img style="display:block;margin:0 auto;" src="<?php echo base_url();?>assets/img/santandergrande.jpg" alt=""><br>
                        No. Cuenta: 5579 1000 4288 2553<br><br>
                        Una vez realizado el pago, inserta el número de referencia, folio ó de autorizacíón aquí:
                    </h3>
                    <form style="margin-bottom:20px;" class="verificarForm" name="verificar" id="verificarForm" method="post" novalidate>
                        <input style="margin:0 auto;display:inline-block;height:44px;" type="number" class="form-control" placeholder="Número de referencia, folio ó de autorización (aparece en el voucher)" id="autorizacion" name="autorizacion" required>
                        <button type="submit" class="voucher btn btn-xl" style="padding:10px 16px;display:inline-block;">Verificar</button>
                        <br><h3><span class="text-danger"></span></h3>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="formsCont">
                    <div class="formsSlide">
                        <form class="donativoForm" name="donativo" id="donativoForm" novalidate>
                            <h3>Saldo: $<span id="saldo"></span> MXN</h3>
                            <input type="hidden" id="autorizacion" name="autorizacion">
                            <input type="hidden" id="op" name="op" value="3">
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nombre Completo *" name="nombre" id="nombre" required data-validation-required-message="Ingresa tu nombre completo.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Domicilio (calle y número) *" name="domicilio" id="domicilio" required data-validation-required-message="Ingresa tu domicilio (calle y número).">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Colonia *" name="colonia" id="colonia" required data-validation-required-message="Ingresa tu colonia.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="number" class="form-control" placeholder="Código Postal *" name="cp" id="cp" required data-validation-required-message="Ingresa tu código postal (solo números).">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Municipio *" name="municipio" id="municipio" required data-validation-required-message="Ingresa tu municipio.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="estado" id="estado">
                                        <option value="Aguascalientes">Aguascalientes</option>
                                        <option value="Baja California">Baja California</option>
                                        <option value="Baja California Sur">Baja California Sur</option>
                                        <option value="Campeche">Campeche</option>
                                        <option value="Coahuila">Coahuila</option>
                                        <option value="Colima">Colima</option>
                                        <option value="Chiapas">Chiapas</option>
                                        <option value="Chihuahua">Chihuahua</option>
                                        <option value="CDMX">CDMX</option>
                                        <option value="Durango">Durango</option>
                                        <option value="Edo. de México">Edo. de México</option>
                                        <option value="Guanajuato">Guanajuato</option>
                                        <option value="Guerrero">Guerrero</option>
                                        <option value="Hidalgo">Hidalgo</option>
                                        <option value="Jalisco">Jalisco</option>
                                        <option value="Michoacán">Michoacán</option>
                                        <option value="Morelos">Morelos</option>
                                        <option value="Nayarit">Nayarit</option>
                                        <option value="Nuevo León">Nuevo León</option>
                                        <option value="Oaxaca">Oaxaca</option>
                                        <option value="Puebla">Puebla</option>
                                        <option value="Querétaro">Querétaro</option>
                                        <option value="Quintana Roo">Quintana Roo</option>
                                        <option value="San Luis Potosí">San Luis Potosí</option>
                                        <option value="Sinaloa">Sinaloa</option>
                                        <option value="Sonora">Sonora</option>
                                        <option value="Tabasco">Tabasco</option>
                                        <option value="Tamaulipas">Tamaulipas</option>
                                        <option value="Tlaxcala">Tlaxcala</option>
                                        <option value="Veracruz">Veracruz</option>
                                        <option value="Yucatán">Yucatán</option>
                                        <option value="Zacatecas">Zacatecas</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Correo electrónico *" name="correo" id="correo" required data-validation-required-message="Ingresa tu correo electrónico.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-6 col-sm-6">
                                 <div class="form-group merch">
                                    <select class="form-control" name="mercancia" id="mercancia">
                                        <option value="0">Calcomania</option>
                                        <option value="1">Parche</option>
                                        <option value="2">Playera (H)</option>
                                        <option value="3">Playera (M)</option>
                                    </select>
                                    <button class="agregarMerch btn btn-primary"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>
                                <div class="form-group articles"></div>
                                 <div class="form-group">
                                    <button class="limpiar btn btn-default">Limpiar campos</button>
                                    <button class="cancelar btn btn-danger">Cancelar pedido</button>
                                    <button type="submit" class="submission btn btn-primary">Realizar pedido</button>
                                </div>
                            </div>
                        </form>
                        <article class="confirmarPedido"></article>
                    </div>
                </div>
            </div>
        </div>
    </section>

<!-- Contact Section -->
    <section id="contacto">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contactanos</h2>
                    <h3 class="section-subheading text-muted">Dudas, sugerencias, ideas. Todo para mejorar Gotcha Haven!!!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nombre Completo *" id="name" required data-validation-required-message="Ingresa tu nombre completo.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Correo Electrónico *" id="email" required data-validation-required-message="Ingresa tu correo electrónico.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Teléfono *" id="phone" required data-validation-required-message="Ingresa tu número telefónico.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Escribe tu comentario *" id="message" required data-validation-required-message="Ingresa tu comentario."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Enviar Mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>