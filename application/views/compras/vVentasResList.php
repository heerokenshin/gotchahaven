<?php foreach ($productos as $p) { $pic = explode('/', $p->imagenes);?>
	<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 producto">
		<a href="<?php echo site_url('compras/producto').'?id='.$p->id_producto.'&pr='.str_replace(' ','_',$p->nom_producto).'&tr='.$p->id_transaccion;?>">
			<img src="<?php echo base_url();?>assets/img/products/thumbs/<?php echo $pic[1];?>" alt="<?php echo $pic[1];?>">
		</a>
		<aside>
			<h5><a href="<?php echo site_url('compras/producto').'?id='.$p->id_producto.'&pr='.str_replace(' ','_',$p->nom_producto).'&tr='.$p->id_transaccion;?>"><?php echo $p->nom_producto;?></a></h5>
			<span class="text-muted"><?php echo $p->nom_marca.' - '.$p->nom_categoria;?></span>
			<h6><span class="text-danger">Precio: $ <?php echo $p->precio;?> MXN</span>&nbsp;&nbsp;<?php echo $p->condicion;?></h6>
		</aside>
	</div>
<?php }
	if($total->total>30){?>

<div class="row">
	<div class="col-lg-12 col-md-12 pageControls">
		<nav aria-label="Page navigation">
		  <ul class="pagination">
		    <li>
		      <a href="#" aria-label="Previous">
		        <span aria-hidden="true"><i class="glyphicon glyphicon-triangle-left"></i></span>
		      </a>
		    </li>
		    <?php for ($i=1; $i < ($total->total)/30; $i++) {?>
		    	<li><a href="#"><?php echo ($i);?></a></li>
		    <?php }
		    	if($total->total%30!=0){echo '<li><a href="#">'.((int)($total->total/30)+1).'</a></li>';}
		    ?>
		    <li>
		      <a href="#" aria-label="Next">
		        <span aria-hidden="true"><i class="glyphicon glyphicon-triangle-right"></i></span>
		      </a>
		    </li>
		  </ul>
		</nav>
	</div>
</div>
<?php }?>