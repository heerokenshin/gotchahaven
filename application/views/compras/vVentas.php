<!--<div class="container-fluid searchContainer">
	<div class="row">
		<div class="container">
			<div class="col-lg-10 col-md-10 col-sm-8 col-xs-12">
				<form class="searchForm" id="searchForm" name="searchForm" novalidate>
					<div class="input-group">
						<input type="text" class="form-control" id="palabras" name="palabras" placeholder="Palabras clave (marcadora, careta, pods)" required>
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-serach"></i> BUSCAR</button>
						</span>
					</div>
				</form>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
				carrito de compra
			</div>
		</div>
	</div>
</div>-->
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 sortCategorias">
			<h3>Ordenar por:</h3>
			<div class="form-group">
				<select class="form-control" name="order" id="order">
					<option value="reciente">Reciente</option>
					<option value="condicion">Condición</option>
					<option value="bajo">Precio: bajo</option>
					<option value="alto">Precio: alto</option>
				</select>
			</div>
			<h3>Categorias</h3>
			<?php foreach ($categorias as $c) {?>
				<figure id="<?php echo $c->id_categoria;?>" class="catego">
					<?php echo $c->nom_categoria;?><span class="floatRight"><i class="glyphicon glyphicon-menu-right"></i></span>
					<aside class="subs"></aside>
				</figure>
			<?php }?>
			<h3>Marcas</h3>
			<?php foreach ($marcas as $m) {?>
				<figure id="<?php echo $m->id_marcas;?>" class="marca"><?php echo $m->nom_marca;?></figure>
			<?php }?>
			<div class="ads">Ads aqui</div>
		</div>
		<div id="sortProducts" class="col-lg-8 col-md-7 col-sm-6 col-xs-12 sortProducts"></div>
		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 ads">ads aqui</div>
	</div>
</div>

<script>
	ventas.main.listarProductos();
	ventas.main.buscarProductos();
	ventas.main.elegirCategoria();
	ventas.main.elegirMarca();
	ventas.main.elegirSort();
</script>
