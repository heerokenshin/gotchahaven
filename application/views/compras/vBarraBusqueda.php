<div class="container-fluid searchContainer">
	<div class="row">
		<div class="container">
			<div class="col-lg-10 col-md-10 col-sm-9 col-xs-12">
				<form class="searchForm" id="searchForm" name="searchForm" novalidate>
					<div class="input-group">
						<input type="text" class="form-control" id="palabras" name="palabras" placeholder="Palabras clave (marcadora, careta, pods)" required>
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-serach"></i> BUSCAR</button>
						</span>
					</div>
				</form>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
				<div class="btn-group carrito">
				  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <i class="fa fa-shopping-cart"></i> Carrito <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				  	<?php if(isset($carrito)){
						foreach ($carrito as $c) { $pic = explode('/', $c['imagenes']);?>
						<li>
							<a href="<?php echo site_url('compras/producto').'?id='.$c['id_producto'].'&pr='.str_replace(' ','_',$c['nom_producto']).'&tr='.$c['id_transaccion'];?>">
								<img src="<?php echo base_url();?>assets/img/products/thumbs/<?php echo $pic[1];?>" alt="<?php echo $pic[1];?>">
								<span class="wrapped"><?php echo $c['nom_producto'];?></span>
							</a>
							<aside><i id="<?php echo $c['id_producto'];?>" class="glyphicon glyphicon-remove"></i></aside>
						</li>
					<?php  }echo '<li><a class="toCheckout btn btn-primary" href="'.site_url('compras/caja').'">Despachar <i class="fa fa-sign-in"></i></a></li>'; }else echo '<h6>Sin productos aún</h6>';?>
				  </ul>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	ventas.main.quitarDelCarrito();
</script>