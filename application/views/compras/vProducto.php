<div class="container-fluid">
	<div class="row">
	<?php foreach ($producto as $p) { $tbs = explode('/', $p->imagenes);?>
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 prodPreview">
			<div class="row"><a href="#"><i class="glyphicon glyphicon-menu-left"></i> Volver a resultados con "algo"</a></div>
			<aside class="thumbs">
				<?php for ($i=1; $i < count($tbs); $i++) {
					$class='';
					if($i==1)$class=' class="active"';
					echo '<img'.$class.' src="'.base_url().'assets/img/products/thumbs/'.$tbs[$i].'" alt="'.$tbs[$i].'">';
				}?>
			</aside>
			<img class="viewer" src="<?php echo base_url();?>assets/img/products/<?php echo $tbs[1];?>" alt="<?php echo $tbs[1];?>">
		</div>
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 description">
			<h4 style="color:#FF8D00;"><?php echo $p->nom_producto;?></h4>
			<input type="hidden" id="id_producto" name="id_producto" value="<?php echo $p->id_producto;?>">
			<?php foreach ($marcas as $m) {
					if($m->id_marcas==$p->id_marcas) echo '<h5>Marca: <span class="text-muted">'.$m->nom_marca.'</span><span class="floatRight">'.$p->condicion.'</span></h5>';
				}
				foreach ($categorias as $c) {
					if($c->id_categoria==$p->id_categoria) echo 'Categoria: <span class="text-muted">'.$c->nom_categoria.' > ';
				}
				foreach ($subcategorias as $s) {
					if($s->id_subcatego==$p->id_subcatego) echo $s->nom_subcatego.'</span>';
				}
			?>
			<br><br>
			<ul class="nav nav-tabs prodData">
				<li class="active"><a data-toggle="tab" href="#descripcion">Descripción</a></li>
				<?php if($p->video!='') echo '<li><a data-toggle="tab" href="#video">Video</a></li>';?>
			</ul>
			<div class="tab-content">
				<div id="descripcion" class="tab-pane fade in active"><?php echo $p->descripcion;?></div>
				<?php if($p->video!=''){?>
					<div id="video" class="tab-pane fade">
						<video width="90%" height="auto" controls>
							<source src="<?php echo base_url();?>assets/img/products/video/<?php echo $p->video;?>" type="video/mp4">
						</video>
					</div>
				<?php }?>
			</div>

		</div>
		<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 toCart">
			<aside class="sociales">
				<b>Compartir </b>
				<i class="fa fa-facebook-square"></i>
				<i class="fa fa-twitter-square"></i>
			</aside>
			<aside>
				<button class="addCart btn btn-primary">Agregar al <i class="glyphicon glyphicon-shopping-cart"></i></button>
			</aside>
			<div class="ads">
				ads aqui
			</div>
		</div>
	<?php }?>
	</div>
	<div class="row">
		<div class="container ads">
			ads aqui
		</div>
	</div>
</div>

<script>
	ventas.main.changeThumb();
	ventas.main.agregarAlCarrito();
</script>