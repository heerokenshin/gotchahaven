    <footer>
        <?php $isReg = strpos($_SERVER["REQUEST_URI"], 'registrarse');?>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright" <?php if($isReg>-1)echo 'style="color: #ddd;"';?>>Copyright Gotchahaven.com 2016</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a target="_blank" href="https://twitter.com/GotchaHaven"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a target="_blank" href="https://www.facebook.com/gotchahaven/?fref=ts"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a target="_blank" href="https://www.youtube.com/channel/UCI0VIuJrYuKmY_Sv4oJZRDg"><i class="fa fa-youtube"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Politica de Privacidad</a>
                        </li>
                        <li><a href="#">Terminos de uso</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Theme JavaScript -->
    <script src="<?php echo base_url();?>assets/js/agency.js"></script>
</body>
</html>