<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sitio de información, compra y venta de artículos de gotcha, paintball y airsoft">
    <meta name="keywords" content="gotcha,paintball,airsoft,compra,venta,eventos,noticias,tienda,gotchahaven,haven">
    <meta name="author" content="gotcha haven staff">

    <title>Gotcha Haven</title>
    <link rel="icon" href="<?php echo base_url();?>favicon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->


    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/gotcha.css" rel="stylesheet">
    
    <?php if (isset($css)): foreach ($css as $c):?>
        <link href="<?php echo base_url()."assets/css/{$c}.css";?>" rel="stylesheet">
    <?php endforeach; endif;?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script id="facebook-jssdk" src="//connect.facebook.net/en_US/sdk.js"></script>
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url();?>assets/js/contact_me.js"></script>
    <script src="<?php echo base_url();?>assets/js/donativos.js"></script>

    <?php if (isset($scripts)): foreach ($scripts as $js):?>
        <script src="<?php echo base_url()."assets/js/{$js}.js ";?>" type="text/javascript"></script>
    <?php endforeach; endif;?> 

    <script>
        publico.main.desloguearse();
        publico.main.loginFacebook();
        //publico.main.desenfoque();
    </script>

</head>

<body id="page-top" class="index">
    <?php $flag = strlen (str_replace('/gotchahaven/', '', $_SERVER["REQUEST_URI"]));?>
    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<?php if($flag==0)echo '#page-top';else echo base_url();?>">
                    <img class="gotchalogo" src="<?php echo base_url();?>assets/img/logos/logo.png" alt="">
                </a>
            </div>
            
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <?php if($flag==0){ ?>
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#noticias">Noticias</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#eventos">Eventos</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#compra">Compra</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contacto">Contacto</a>
                        </li>
                    <?php }else{ ?>
                        <li class="hidden">
                            <a href="../zanzibar/"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo site_url('noticias');?>">Noticias</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo site_url('eventos');?>">Eventos</a>
                        </li>
                        <li class="btn-group">
                            <a id="login" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Compra <span class="caret"></span></a>
                            <ul class="dark loginMenu dropdown-menu">
                                <li><a href="<?php echo site_url('compras/ventas');?>">Ventas</a></li>
                                <li><a href="<?php echo site_url('compras/subastas');?>">Subastas</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="page-scroll" href="<?php echo site_url('contacto');?>">Contacto</a>
                        </li>
                    <?php } ?>
                    <li class="btn-group loginGroup">
                        <a id="login" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="glyphicon glyphicon-user"></i> <span class="user"><?php if(isset($name))echo $name;else echo 'Usuario';?></span> <span class="caret"></span>
                        </a>
                        <?php if(isset($facebook)){?>
                            <ul class="dropdown-menu loginMenu dark">
                                <li><a href="<?php echo site_url('perfil');?>"><i class="glyphicon glyphicon-user"></i> Perfil</a></li>
                                <li><a href="<?php echo site_url('actividad');?>"><i class="glyphicon glyphicon-tasks"></i> Actividad</a></li>
                                <li><a class="logout"><i class="glyphicon glyphicon-remove"></i> Salir</a></li>
                                <input id="faceSes" type="hidden" value="<?php echo $facebook;?>">
                            </ul>
                        <?php }else{?>
                            <div class="dropdown-menu loginMenu">
                                <button class="btn btn-face"><i class="fa fa-facebook-f"></i> Ingresar con Facebook</button>
                            </div> 
                        <?php }?>
                    </li>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>