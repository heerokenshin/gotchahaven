<?php $bancos = array(
	"Banamex",
	"Bancomer",
	"Banorte",
	"Banco Bajio",
	"Santander",
	"Serfin",
	"HSBC",
	"Saldazo Oxxo",
	"Scotiabank",
	"Inbursa",
	"IXE",
	"Citibank"
);?>
<form id="editCtaForm" class="editCtaForm" novalidate>
	<?php foreach ($cuentas as $c) {?>
		<input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $c['id_usuario'];?>">
		<input type="hidden" name="id_cuenta" id="id_cuenta" value="<?php echo $c['id_cuenta'];?>">
		<div class="form-group">
			<select class="form-control" name="banco" id="banco">
			<?php 
				foreach ($bancos as $b) {
					$selected = ($b==$c['banco'])?' selected':'';
					echo '<option value="'.$b.'"'.$selected.'>'.$b.'</option>';
				}
			?>	
			</select>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Titular" name="titular" id="titular" value="<?php echo $c['titular'];?>" required data-validation-required-message="Ingresa el titular de la cuenta.">
			<p class="help-block text-danger"></p>
		</div>
		<div class="form-group">
			<input type="number" class="form-control" placeholder="No. cuenta" name="no_cuenta" id="no_cuenta" value="<?php echo $c['no_cuenta'];?>" required data-validation-required-message="Ingresa el número de cuenta.">
			<p class="help-block text-danger"></p>
		</div>
		<div class="form-group">
			<input type="number" class="form-control" placeholder="Clabe interbancaria (opcional)" name="clabe" id="clabe" value="<?php echo $c['clabe'];?>" required data-validation-required-message="Ingresa la clabe interbancaria.">
			<p class="help-block text-danger"></p>
		</div>
	<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar</button>
	<button class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
	<?php }?>
</form>

<script type="text/javascript">
	$("#editCtaForm input,#editCtaForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "perfil/editarCuenta",
                type: "POST",
                data:dta,
                success: function(res){
                  refreshCuentas();
                },
                error: function(err){
                  console.log(err);
                    //clear all fields
                    $('#editCtaForm').trigger("reset");
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
</script>