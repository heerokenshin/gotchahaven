<?php if($firstTime){ ?>
<div class="container firstTime">
	<?php if(isset($redirectURL)){?>
		<input type="hidden" id="prodRedir" name="prodRedir" value="<?php echo $redirectURL;?>">
	<?php }?>
	<div class="intro-text">
		<h2>Bienvenido <?php echo $fname.' '.$lname;?> <br>al refugio del paintball y airsoft!</h2>
		<h3>Para ser parte de esta gran comunidad, ingresa tus datos faltantes a este formulario.</h3>
		<form class="perfilForm" name="perfil" id="perfilForm" novalidate>
			<input type="hidden" id="facebook" name="facebook" value="<?php echo $id;?>">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group">
	                    <input type="text" class="form-control" placeholder="Nombre(s)" name="nombres" id="nombres" value="<?php echo $fname;?>" required data-validation-required-message="Ingresa tu(s) nombre(s).">
	                    <p class="help-block text-danger"></p>
	                </div>
	                <div class="form-group">
	                    <input type="email" class="form-control" placeholder="Correo electrónico" name="correo" id="correo" required data-validation-required-message="Ingresa tu correo electrónico.">
	                    <p class="help-block text-danger"></p>
	                </div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="form-group">
	                    <input type="text" class="form-control" placeholder="Apellidos" name="apellidos" id="apellidos" value="<?php echo $lname;?>" required data-validation-required-message="Ingresa tus apellidos.">
	                    <p class="help-block text-danger"></p>
	                </div>
	                <div class="form-group">
	                    <input type="text" class="form-control" placeholder="Teléfono o celular. Ej. (454) 543 7858" name="whatsapp" id="whatsapp" required data-validation-required-message="Ingresa un número telefónico.">
	                    <p class="help-block text-danger"></p>
	                </div>
				</div>
			</div>
			<div class="row direccion">
				<h4>¿Quieres agregar una dirección? (La puedes agregar más tarde...) <span class="floatRight"><button type="button" class="addDireccion btn btn-info">Agregar dirección</button></span></h4>
			</div>
			<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar datos</button><button type="submit" class="btn btn-primary">Confirmar datos</button>
		</form>
	</div>
</div>

<script>
	publico.main.limpiarForm();
	publico.main.agregarDireccion();
	publico.main.procesarFormularioPerfil();
</script>
<?php }else{ ?>
	<div class="container firstTime">
		<div class="intro-text">
			<h2>Configuración del perfil de usuario</h2>
		</div>
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#perfil">Datos de perfil</a></li>
			<li><a data-toggle="tab" href="#direcciones">Direcciones</a></li>
			<li><a data-toggle="tab" href="#cuentas">Cuentas</a></li>
		</ul>
		<div class="tab-content">
			<div id="perfil" class="tab-pane fade in active">
				<?php 
					foreach ($perfil as $p) { ?>
						<div class="row">
							<span id="<?php echo $p->id_usuario;?>" class="editarPerfil floatRight">Editar <i class="glyphicon glyphicon-pencil"></i></span>
						</div>
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								<aside style="width:100%;height:auto;background:gray;">Reputación: <?php echo $p->reputacion;?></aside>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="perfilCard">
									<b>Nombre(s): </b> <span><?php echo $p->nombres;?></span id="nombres"><br>
									<b>Apellidos: </b> <span><?php echo $p->apellidos;?></span id="apellidos"><br>
									<b><i class="glyphicon glyphicon-envelope"></i> Correo electrónico:</b> <span id="correo"><?php echo $p->correo;?></span><br>
									<b><i class="glyphicon glyphicon-phone"></i> Teléfono: </b> <span id="whatsapp"><?php echo $p->whatsapp;?></span>
								</div>
								<form style="display:none;" class="perfilForm" name="perfil" id="perfilForm" novalidate>
									<input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $p->id_usuario;?>">
									<div class="form-group">
							            <input type="text" class="form-control" placeholder="Nombre(s)" name="nombres" id="nombres" value="<?php echo $p->nombres;?>" required data-validation-required-message="Ingresa tu(s) nombre(s).">
							            <p class="help-block text-danger"></p>
							        </div>
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="Apellidos" name="apellidos" id="apellidos" value="<?php echo $p->apellidos;?>" required data-validation-required-message="Ingresa tus apellidos.">
							            <p class="help-block text-danger"></p>
							        </div>
							        <div class="form-group">
							            <input type="email" class="form-control" placeholder="Correo electrónico" name="correo" id="correo" value="<?php echo $p->correo;?>" required data-validation-required-message="Ingresa tu correo electrónico.">
							            <p class="help-block text-danger"></p>
							        </div>	
							        <div class="form-group">
							            <input type="text" class="form-control" placeholder="Teléfono o celular. Ej. (454) 543 7858" name="whatsapp" id="whatsapp" value="<?php echo $p->whatsapp;?>" required data-validation-required-message="Ingresa un número telefónico.">
							            <p class="help-block text-danger"></p>
							        </div>
									<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar datos</button>
									<button class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
									<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
								</form>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 text-center">
								<div id="successPerfil"></div>
							</div>
						</div>
				<?php }	?>
			</div>
			<div id="direcciones" class="tab-pane fade">
				<div class="row">
					<span class="agregarDireccion floatRight">Agregar dirección</span>
				</div>
				<div class="row">
				<?php if(isset($direcciones)){
					foreach ($direcciones as $d) {?>
						<div style="vertical-align:top;margin-bottom:10px;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<figure class="dirFig">
								<div class="row">
									<i class="glyphicon glyphicon-home"></i>&nbsp;&nbsp;<span class="title">Datos de dirección</span>
									<span id="<?php echo $d['id_dir'];?>" class="borrarDireccion floatRight"><i class="glyphicon glyphicon-trash"></i></span>
									<span id="<?php echo $d['id_dir'];?>" class="editarDireccion floatRight"><i class="glyphicon glyphicon-pencil"></i></span>
								</div>
								<aside>
									<b>Calle:</b><br>
									<!--<?php echo $d->calle;?>-->
									<?php echo $d['calle'];?><br>
									<b>Colonia:</b><br>
									<!--<?php echo $d->colonia;?>-->
									<?php echo $d['colonia'];?><br>
									<b>Código postal:</b><br>
									<?php echo $d['cp'];?><br>
									<b>Municipio:</b><br>
									<!--<?php echo $d->municipio;?>-->
									<?php echo $d['municipio'];?><br>
									<b>Estado:</b><br>
									<!--<?php echo $d->nom_estado;?>-->
									<?php echo $d['nom_estado'];?><br>
								</aside>
							</figure>
						</div>
				
				<?php } }?>
					<div id="nuevaDir" style="margin-bottom: 10px;display:none;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<figure class="dirFig">
							<div class="row">Nueva dirección</div>
							<aside>
								<form id="dirForm" class="dirForm" novalidate>
									<input type="hidden" name="id_usuario" id="id_usuario">
									<div class="form-group"><input type="text" class="form-control" placeholder="Calle y número" name="calle" id="calle" required data-validation-required-message="Ingresa calle y número."><p class="help-block text-danger"></p></div>
									<div class="form-group"><input type="text" class="form-control" placeholder="Colonia" name="colonia" id="colonia" required data-validation-required-message="Ingresa la colonia."><p class="help-block text-danger"></p></div>
									<div class="form-group"><input type="number" class="form-control" placeholder="Código postal" name="cp" id="cp" required data-validation-required-message="Ingresa tu código postal."><p class="help-block text-danger"></p></div>
									<div class="form-group"><input type="text" class="form-control" placeholder="Municipio" name="municipio" id="municipio" requires data-validation-required-message="Ingresa el municipio."><p class="help-block text-danger"></p></div>
									<div class="form-group">
										<select class="form-control" name="estados" id="estados">
										<?php 
											foreach ($estados as $e) {
												echo '<option value="'.$e->id_estado.'">'.$e->nom_estado.'</option>';
											}
										?>	
										</select>
									</div>
									<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar</button>
									<button class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
									<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
								</form>
								<div class="row">
									<div class="col-lg-12 text-center">
										<div id="successDir"></div>
									</div>
								</div>
							</aside>
						</figure>
					</div>
				</div>
			</div>
			<div id="cuentas" class="tab-pane fade">
				<div class="row">
					<span class="agregarCuenta floatRight">Agregar cuenta</span>
				</div>
				<div class="row">
					<?php if(isset($cuentas)){
						foreach ($cuentas as $c) { ?>
						<div style="vertical-align:top;margin-bottom:10px;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<figure class="ctaFig">
								<div class="row">
									<i class="glyphicon glyphicon-card"></i>&nbsp;&nbsp;<span class="title">Datos de la cuenta</span>
									<span id="<?php echo $c['id_cuenta'];?>" class="borrarCuenta floatRight"><i class="glyphicon glyphicon-trash"></i></span>
									<span id="<?php echo $c['id_cuenta'];?>" class="editarCuenta floatRight"><i class="glyphicon glyphicon-pencil"></i></span>
								</div>
								<aside>
									<b>Banco:</b><br>
									<?php echo $c['banco'];?><br>
									<b>Titular:</b><br>
									<!--<?php echo $d->municipio;?>-->
									<?php echo $c['titular'];?><br>
									<b>Número de cuenta:</b><br>
									<!--<?php echo $d->calle;?>-->
									<?php echo $c['no_cuenta'];?><br>
									<b>Clabe interbancaria:</b><br>
									<!--<?php echo $d->colonia;?>-->
									<?php echo $c['clabe'];?><br>
									
								</aside>
							</figure>
						</div>
					<?php } }?>
					<div id="nuevaCta" style="margin-bottom:10px;display:none;" class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
						<figure class="ctaFig">
							<div class="row"><i class="fa fa-credit-card"></i> Nueva cuenta</div>
							<aside>
								<form id="ctaForm" class="ctaForm" novalidate>
									<input type="hidden" name="id_usuario" id="id_usuario">
									<div class="form-group"><input type="number" class="form-control" placeholder="No. cuenta" name="no_cuenta" id="no_cuenta" required data-validation-required-message="Ingresa número de cuenta bancaria."><p class="help-block text-danger"></p></div>
									<div class="form-group"><input type="number" class="form-control" placeholder="Clabe Interbancaria (opcional)" name="clabe" id="clabe"><p class="help-block text-danger"></p></div>
									<div class="form-group"><input type="text" class="form-control" placeholder="Titluar de la cuenta" name="titluar" id="titluar" required data-validation-required-message="Ingresa el titular de la cuenta."><p class="help-block text-danger"></p></div>
									<div class="form-group">
										<select class="form-control" name="banco" id="banco">
											<option value="Banamex">Banamex</option>
											<option value="Bancomer">Bancomer</option>
											<option value="Banorte">Banorte</option>
											<option value="Banco Bajio">Banco Bajio</option>
											<option value="Santander">Santander</option>
											<option value="Serfin">Serfin</option>
											<option value="HSBC">HSBC</option>
											<option value="Saldazo Oxxo">Saldazo Oxxo</option>
											<option value="Scotiabank">Scotiabank</option>
											<option value="Inbursa">Inbursa</option>
											<option value="IXE">IXE</option>
											<option value="Citibank">Citibank</option>
										</select>
									</div>
									<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar</button>
									<button class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
									<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
								</form>
								<div class="row">
									<div class="col-lg-12 text-center">
										<div id="successCta"></div>
									</div>
								</div>
							</aside>
						</figure>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		perfil.main.editarPerfil();
		perfil.main.limpiarForm();
		perfil.main.procesarFormularioPerfil();
		perfil.main.cancelarPerfilEdicion();
		perfil.main.cancelarAgregarDir();
		perfil.main.agregarDireccion();
		perfil.main.procesarFormularioDir();
		perfil.main.cerrarFormularioDir();
		perfil.main.abrirFormularioEditarDireccion();
		perfil.main.preEliminarDireccion();
		perfil.main.cancelarBorrarDireccion();
		perfil.main.confirmarBorrarDireccion();
		perfil.main.cancelarEditarDireccion();
		perfil.main.agregarCuenta();
		perfil.main.procesarFormularioCta();
		perfil.main.abrirFormularioEditarCuenta();
		perfil.main.cancelarEditarCuenta();
		perfil.main.preEliminarCuenta();
		perfil.main.cancelarBorrarCuenta();
		perfil.main.confirmarBorrarCuenta();
	</script>
<?php } ?>