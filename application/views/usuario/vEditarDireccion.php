<form id="editDirForm" class="editDirForm" novalidate>
	<?php 
	$est='';
	foreach ($direccion as $d) {?>
		<input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $d['id_usuario'];?>">
		<input type="hidden" name="id_dir" id="id_dir" value="<?php echo $d['id_dir'];?>">
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Calle y número" name="calle" id="calle" value="<?php echo $d['calle'];?>" required data-validation-required-message="Ingresa calle y número.">
			<p class="help-block text-danger"></p>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Colonia" name="colonia" id="colonia" value="<?php echo $d['colonia'];?>" required data-validation-required-message="Ingresa la colonia.">
			<p class="help-block text-danger"></p>
		</div>
		<div class="form-group">
			<input type="number" class="form-control" placeholder="Código postal" name="cp" id="cp" value="<?php echo $d['cp'];?>" required data-validation-required-message="Ingresa tu código postal.">
			<p class="help-block text-danger"></p>
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Municipio" name="municipio" id="municipio" value="<?php echo $d['municipio'];?>" requires data-validation-required-message="Ingresa el municipio.">
			<p class="help-block text-danger"></p>
		</div>
	<?php $est = $d['id_estado']; }?>
	
	<div class="form-group">
		<select class="form-control" name="estados" id="estados">
		<?php 
			foreach ($estados as $e) {
				$selected = ($e->id_estado==$est)?' selected':'';
				echo '<option value="'.$e->id_estado.'"'.$selected.'>'.$e->nom_estado.'</option>';
			}
		?>	
		</select>
	</div>
	<button class="reset btn btn-default"><i class="glyphicon glyphicon-erase"></i> Limpiar</button>
	<button class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
	<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Guardar</button>
</form>

<script type="text/javascript">
	$("#editDirForm input,#editDirForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            
            var dta = $form.serialize();
            $.ajax({
                url: "perfil/editarDireccion",
                type: "POST",
                data:dta,
                success: function(res){
                  refreshDirecciones();
                },
                error: function(err){
                  console.log(err);
                    //clear all fields
                    $('#editDirForm').trigger("reset");
                }
            });
        },
        filter: function() {
            return $(this).is(":visible");
        },
      });
</script>