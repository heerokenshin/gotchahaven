<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Sitio de información, compra y venta de artículos de gotcha, paintball y airsoft">
    <meta name="keywords" content="gotcha,paintball,airsoft,compra,venta,eventos,noticias,tienda,gotchahaven,haven">
    <meta name="author" content="gotcha haven staff">

    <title>Gotcha Haven</title>
    <link rel="icon" href="<?php echo base_url();?>favicon.ico">

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>-->


    <!-- Theme CSS -->
    <link href="<?php echo base_url();?>assets/css/gotcha.css" rel="stylesheet">
    
    <?php if (isset($css)): foreach ($css as $c):?>
        <link href="<?php echo base_url()."assets/css/{$c}.css";?>" rel="stylesheet">
    <?php endforeach; endif;?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo base_url();?>assets/js/contact_me.js"></script>
    <script src="<?php echo base_url();?>assets/js/donativos.js"></script>

    <?php if (isset($scripts)): foreach ($scripts as $js):?>
        <script src="<?php echo base_url()."assets/js/{$js}.js ";?>" type="text/javascript"></script>
    <?php endforeach; endif;?> 

</head>
<body style="background:#222222;">
	<div class="container" style="margin-top:30px;">
		<div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
			<a href="<?php echo base_url();?>">
				<img style="width:100%;" src="<?php echo base_url();?>assets/img/logos/logo.png" alt="logo.png">
			</a>
			<div class="signIn">
				<h3 style="text-align:center;color:#ddd;">Registrarse</h3>
				<button style="width:100%;" class="btn btn-face"><i class="fa fa-facebook-f"></i> Ingresar con Facebook</button>
				<input type="hidden" id="redir" name="redir" value="<?php echo $prodURL;?>">
			</div>
		</div>
	</div>

	<script>
		publico.main.loginFacebook();
	</script>

</body>