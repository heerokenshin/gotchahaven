<div class="container">
	<h4>Datos de la dirección</h4>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
	        <input type="text" class="form-control" placeholder="Calle y número" name="calle" id="calle" required data-validation-required-message="Ingresa calle y número.">
	        <p class="help-block text-danger"></p>
	    </div>
	    <div class="form-group">
	        <input type="number" class="form-control" placeholder="Código postal" name="cp" id="cp" required data-validation-required-message="Ingresa tu código postal.">
	        <p class="help-block text-danger"></p>
	    </div>
	    <div class="form-group">
	    	<select class="form-control" name="estado" id="estado">
	    		<?php 
	    			foreach ($estados as $edo) {
	    				echo '<option value="'.$edo->id_estado.'">'.$edo->nom_estado.'</option>';
	    			}
	    		?>
	    	</select>
	    </div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		<div class="form-group">
	        <input type="text" class="form-control" placeholder="Colonia" name="colonia" id="colonia" required data-validation-required-message="Ingresa la colonia.">
	        <p class="help-block text-danger"></p>
	    </div>
	    <div class="form-group">
	        <input type="text" class="form-control" placeholder="Municipio" name="municipio" id="municipio" requires data-validation-required-message="Ingresa el municipio.">
	        <p class="help-block text-danger"></p>
	    </div>
	</div>
</div>