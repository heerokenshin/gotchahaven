<div class="container-fluid actividadBoard">
	<?php foreach ($perfil as $p) {
		echo '<input type="hidden" name="myID" id="myID" value="'.$p->id_usuario.'">';
		//$tienda = $p->tienda;
	}?>
	<h2><button class="btn btn-cancel floatLeft subirProducto">Nuevo <i class="fa fa-archive"></i> <i class="glyphicon glyphicon-plus"></i></button><span class="slideRight"><i class="fa fa-archive"></i> Ventas y productos</span></h2>
	<div class="row">
		<div class="col-lg-12 col md-12 col-sm-12 col-xs-12 prodCont">
			<h4>Producto nuevo</h4>
			<form class="productoForm" id="productoForm" enctype="multipart/form-data" novalidate>
				<?php foreach ($perfil as $p) {
					if($p->tienda>0) echo '<input type="hidden" name="id_tienda" id="id_tienda" value="'.$tienda[0]->id_tienda.'">';
					echo '<input type="hidden" name="id_vendedor" id="id_vendedor" value="'.$p->id_usuario.'">';
				}?>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Nombre del producto" name="nom_producto" id="nom_producto" required data-validation-required-message="Ingresa nombre de tu producto.">
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group">
						<textarea class="form-control" cols="30" rows="5" placeholder="Descripción del producto" name="descripcion" id="descripcion" required data-validation-required-message="Ingresa una descripción."></textarea>
						<p class="help-block text-danger"></p>
					</div>
					<div class="form-group half">
						<span class="sideText">$</span> <input style="width:70%;" type="number" class="form-control" placeholder="Precio" name="precio" id="precio" required data-validation-required-message="Ingresa el precio del producto."> <span class="sideText">MXN</span>
						<p class="help-block text-danger"></p>
					</div>
					<?php if($p->tienda>0){?>
						<div class="form-group half">
							<span class="sideText">Cantidad:</span> <input style="width:50%;" type="number" min="1" class="form-control" placeholder="Cant." name="existencia" id="existencia" required data-validation-required-message="Ingresa la cantidad.">
							<p class="help-block text-danger"></p>
						</div>
					<?php }?>
					<div class="form-group half">
						<?php if($p->tienda>0){ echo '<span class="sideText">Condición: </span>';}?>
						<select class="form-control" name="condicion" id="condicion">
							<option value="Nuevo">Nuevo</option>
							<option value="Seminuevo">Semiuevo</option>
							<option value="Usado">Usado</option>
						</select>
					</div>
					<div class="form-group half">
						<span class="sideText">Marca: </span><select name="id_marcas" id="id_marcas" class="form-control">
							<?php foreach ($marcas as $m) {echo '<option value="'.$m->id_marcas.'">'.$m->nom_marca.'</option>';}?>
						</select>
					</div>
					<div class="form-group half">
						<span class="sideText">Categoria: </span><select name="id_categoria" id="id_categoria" class="form-control">
							<?php foreach ($categorias as $cat) {echo '<option value="'.$cat->id_categoria.'">'.$cat->nom_categoria.'</option>';}?>
						</select>
					</div>
					<div class="form-group half">
						<span class="sideText">Subcategoria: </span><select name="id_subcatego" id="id_subcatego" class="form-control">
							<?php foreach ($subcategorias as $sub) {echo '<option value="'.$sub->id_subcatego.'">'.$sub->nom_subcatego.'</option>';}?>
						</select>
					</div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
					<div id="dZUpload" class="dropzone">
					    <div class="dz-default dz-message">
					    	<i class="glyphicon glyphicon-picture"></i> Arrastra la imagenes ó da click aquí
					    </div>
					</div>
					<input type="hidden" id="archivo" name="archivo">
					<br><br>
					<div class="dropzone" id="dZVideo">
						<div class="dz-default dz-message">
					    	<i class="glyphicon glyphicon-facetime-video"></i> Arrastra un video o da click aquí (opcional)
					    </div>
					</div>
					<input type="hidden" id="video" name="video">
					<div class="row botones">
						<button class="reset btn btn-default">Limpiar <i class="glyphicon glyphicon-erase"></i></button>
						<button class="btn btn-cancel">Cancelar <i class="glyphicon glyphicon-remove"></i></button>
						<button type="submit" class="btn btn-primary">Subir <i class="glyphicon glyphicon-open"></i></button>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 text-center">
						<div id="successProducto"></div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div>
		<h2>	
			<span class="floatLeft prodSorter">
				Ver por:
				<select name="prodSortBy" id="prodSortBy">
					<option value="RECIENTE">RECIENTE</option>
					<option value="VENTA">VENTA</option>
					<option value="APARTADO">APARTADO</option>
					<option value="EN CAMINO">EN CAMINO</option>
					<option value="CANCELADO">CANCELADO</option>
					<option value="VENDIDO">VENDIDO</option>
					<option value="DESHABILITADO">DESHABILITADO</option>
				</select>
			</span>
			<span class="slideRight"><i class="fa fa-bookmark"></i> Tus productos</span>
		</h2>
		<div class="prodList"></div>
	</div>
	
</div>

<script>
	ventas.main.prepararDropzone();
	ventas.main.limpiarForm();
	ventas.main.quitarNombreImagen();
	ventas.main.quitarNombreVideo();
	ventas.main.abrirProductoForm();
	ventas.main.cambiarSubcategoria();
	ventas.main.procesarProductoFormulario();
	ventas.main.cerrarFormularioProducto();
	ventas.main.abrirEditarProductoForm();
	ventas.main.preEliminarImagenEditar();
	ventas.main.eliminarImagenVieja();
	ventas.main.preEliminarVideoViejo();
	ventas.main.eliminarVideoViejo();
	ventas.main.cambiarEstadoTransaccion();
	ventas.main.cancelarEstadoTransaccion();
	ventas.main.abrirContactarComprador();
	ventas.main.cambiarOrdenamiento();
	ventas.main.enviarMensajeComprador();
</script>