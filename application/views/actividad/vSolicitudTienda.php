<section style="padding-bottom:0;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Crea y administra tu propia tienda</h2>
				<h3 class="section-subheading text-muted">
					Si tu ya tienes una tienda física y/o quieres crear una virtual, Gotcha Haven te facilita las herramientas para llevarlo acabo.
				</h3>
			</div>
		</div>
	</div>
</section>
<section class="bg-light-gray">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Funciones incluidas para tu tienda</h2>
				<h3 class="section-subheading text-muted">
					Algunos de las utilidades y beneficios que podrían ser de gran ayuda para tus ventas
				</h3>
			</div>
			<div class="text-center">
	            <div class="col-sm-4 col-md-4">
	                <img class="features" src="<?php echo base_url();?>assets/img/levelup.png" alt="">
	                <h4 class="service-heading">Aumento de rango</h4>
	                <p class="text-muted">Sube de prestigio y reputación para ser de las tiendas recomendadas!</p>
	                
	            </div>
	            <div class="col-sm-4 col-md-4">
	                <img class="features" src="<?php echo base_url();?>assets/img/inventario.png" alt="">
	                <h4 class="service-heading">Control de inventario</h4>
	                <p class="text-muted">Consulta tus ventas y cantidades de tus productos así como tus ganacias.</p>
	                
	            </div>
	            <div class="col-sm-4 col-md-4">
	                <img class="features" src="<?php echo base_url();?>assets/img/publicidad.png" alt="">
	                <h4 class="service-heading">Publicidad en Gotcha Haven</h4>
	                <p class="text-muted">Banners, productos sugeridos y tu logotipo apareciendo alrededor del sitio!</p>
	                
	            </div>
	        </div>
		</div>
	</div>
</section>
<section class="storePrices">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">Plan de comercio</h2>
			</div>
		</div>
		<div class="row storeTable">
			<table>
				<thead>
					<tr>
						<th><h2>Elige el plan para tu tienda!</h2></th>
						<th><img src="<?php echo base_url();?>assets/img/ranks/2.png" alt=""><h5><b>Cabo</b></h5><button id="0" class="btn btn-primary">GRATIS</button></th>
						<th><img src="<?php echo base_url();?>assets/img/ranks/4.png" alt=""><h5><b>Sargento 2do</b></h5><button id="200" class="btn btn-primary">200 MXN / mes</button></th>
						<th><img src="<?php echo base_url();?>assets/img/ranks/6.png" alt=""><h5><b>Teniente</b></h5><button id="450" class="btn btn-primary">450 MXN / mes</button></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Puntos iniciales (reputación)</td>
						<td>60 pts</td>
						<td>200 pts</td>
						<td>450 pts</td>
					</tr>
					<tr>
						<td>Enlaces a tu propia tienda y fan page (facebook)</td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td>Control de inventario</td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td>Logotipo afiliado en la portada de Gotcha Haven</td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td>Sugerencias de sus productos al ver similares</td>
						<td></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td>Logotipo de tienda en areas de COMPRA (ventas, subastas, rifas)</td>
						<td></td>
						<td></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td>Banner de anuncios y promociones en portada de Gotcha Haven</td>
						<td></td>
						<td></td>
						<td><i class="glyphicon glyphicon-ok"></i></td>
					</tr>
					<tr>
						<td></td>
						<td><button id="0" class="btn btn-primary">GRATIS</button></td>
						<td><button id="200" class="btn btn-primary">200 MXN / mes</button></td>
						<td><button id="450" class="btn btn-primary">450 MXN / mes</button></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="row storeList">
			<div class="col-lg-12 text-center">
				<h3 class="section-heading">Elige el plan para tu tienda!</h3>
			</div>
			<div class="col-xs-12 col-sm-12 plan">
				<div class="planLogo">
					<img src="<?php echo base_url();?>assets/img/ranks/2.png" alt="">
					<aside>
						<h4><b>Cabo</b></h4>
						<h5>60 puntos iniciales</h5>
						<button id="0" class="btn btn-primary">GRATIS</button>
					</aside>
				</div>
				<div class="planFeatures">
					<h4>Beneficios</h4>
					<ul>
						<li>Enlaces a tu propia tienda y fan page (facebook)</li>
						<li>Control de inventario</li>
						<li>Logotipo de afiliado en la portada de Gotcha Haven</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 plan">
				<div class="planLogo">
					<img src="<?php echo base_url();?>assets/img/ranks/4.png" alt="">
					<aside>
						<h4><b>Sargento 2do</b></h4>
						<h5>200 puntos iniciales</h5>
						<button id="200" class="btn btn-primary">200 MXN / mes</button>
					</aside>
				</div>
				<div class="planFeatures">
					<h4>Beneficios</h4>
					<ul>
						<li>Enlaces a tu propia tienda y fan page (facebook)</li>
						<li>Control de inventario</li>
						<li>Logotipo de afiliado en la portada de Gotcha Haven</li>
						<li>Sugerencia de sus productos al ver similares</li>
					</ul>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 plan">
				<div class="planLogo">
					<img src="<?php echo base_url();?>assets/img/ranks/6.png" alt="">
					<aside>
						<h4><b>Teniente</b></h4>
						<h5>450 puntos iniciales</h5>
						<button id="450" class="btn btn-primary">450 MXN / mes</button>
					</aside>
				</div>
				<div class="planFeatures">
					<h4>Beneficios</h4>
					<ul>
						<li>Enlaces a tu propia tienda y fan page (facebook)</li>
						<li>Control de inventario</li>
						<li>Logotipo de afiliado en la portada de Gotcha Haven</li>
						<li>Sugerencia de sus productos al ver similares</li>
						<li>Logotipo de tienda en areas de COMPRA (ventas, subastas, rifas)</li>
						<li>Banner de anuncios y promociones en portada de Gotcha Haven</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row lastStep">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">A un paso de comenzar!</h2>
				<h3 class="section-subheading" style="color:white;">Envíanos alguna duda o comentario para asesorarte.</h3>
				<form class="contactForm" name="sentMessage" id="contactForm" novalidate>
                    <div class="row">
                    	<input type="hidden" id="plan" name="plan">
                    	<?php foreach ($perfil as $p) {?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Nombre Completo *" id="name" value="<?php echo $p->nombres.' '.$p->apellidos;?>" required data-validation-required-message="Ingresa tu nombre completo.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Correo Electrónico *" id="email" value="<?php echo $p->correo;?>" required data-validation-required-message="Ingresa tu correo electrónico.">
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Teléfono *" id="phone" value="<?php echo $p->whatsapp;?>" required data-validation-required-message="Ingresa tu número telefónico.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <?php }?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Escribe tu comentario *" id="message" required data-validation-required-message="Ingresa tu comentario."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button type="submit" class="btn btn-xl">Enviar solicitud</button>
                        </div>
                    </div>
                </form>
			</div>
		</div>
	</div>
</section>

<script>
	solicitud.main.mostrarFormularioContacto();
</script>
