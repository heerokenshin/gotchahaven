<?php $flag = str_replace('/gotchahaven/index.php/actividad', '', $_SERVER["REQUEST_URI"]);?>
<div class="wrapper">
	<div class="side-wrapper">
		<ul class="sidebar-nav">
			<li <?php if($flag=='')echo 'class="active"';?>><a href="<?php echo site_url('actividad');?>"><i class="fa fa-dashboard"></i> General</a></li>
			<li <?php if($flag=='/compras')echo 'class="active"';?>><a href="<?php echo site_url('actividad/compras');?>"><i class="fa fa-shopping-cart"></i> Compras</a></li>
			<li <?php if($flag=='/ventas')echo 'class="active"';?>><a href="<?php echo site_url('actividad/ventas');?>"><i class="fa fa-archive"></i> Ventas</a></li>
			<li <?php if($flag=='/subastas')echo 'class="active"';?>><a href="<?php echo site_url('actividad/subastas');?>"><i class="fa fa-gavel"></i> Subastas</a></li>
			<?php foreach ($perfil as $p) {
				if($p->tienda!=0){?>
				<li <?php if($flag=='/tienda')echo 'class="active"';?>><a href="<?php echo site_url('actividad/tienda');?>"><i class="fa fa-shopping-basket"></i> Tienda</a></li>
			<?php }else{ ?>
				<li <?php if($flag=='/solicitudtienda')echo 'class="active"';?>><a href="<?php echo site_url('actividad/solicitudtienda');?>"><i class="fa fa-lock"></i> Tienda</a></li>
			<?php } }?>
		</ul>
	</div>
	<div class="up-wrapper">
		<ul class="upbar-nav">
			<li <?php if($flag=='')echo 'class="active"';?>><a href="<?php echo site_url('actividad');?>"><i class="fa fa-dashboard"></i> General</a></li>
			<li <?php if($flag=='/compras')echo 'class="active"';?>><a href="<?php echo site_url('actividad/compras');?>"><i class="fa fa-shopping-cart"></i> Compras</a></li>
			<li <?php if($flag=='/ventas')echo 'class="active"';?>><a href="<?php echo site_url('actividad/ventas');?>"><i class="fa fa-archive"></i> Ventas</a></li>
			<li <?php if($flag=='/subastas')echo 'class="active"';?>><a href="<?php echo site_url('actividad/subastas');?>"><i class="fa fa-gavel"></i> Subastas</a></li>
			<?php foreach ($perfil as $p) {
				if($p->tienda!=0){?>
				<li <?php if($flag=='/tienda')echo 'class="active"';?>><a href="<?php echo site_url('actividad/tienda');?>"><i class="fa fa-shopping-basket"></i> Tienda</a></li>
			<?php }else{ ?>
				<li <?php if($flag=='/solicitudtienda')echo 'class="active"';?>><a href="<?php echo site_url('actividad/solicitudtienda');?>"><i class="fa fa-lock"></i> Tienda</a></li>
			<?php } }?>
		</ul>
	</div>
</div>
