
<?php if(isset($productos)){
	foreach ($productos as $prod) { $fPic = explode('/', $prod->imagenes);?>
	<div class="row venta fadeUp">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vedProducto">
			<span class="cornerRightTop">
				<button id="<?php echo $prod->id_producto.'-'.$prod->estado;?>" class="btn btn-editOrange">Editar <i class="glyphicon glyphicon-pencil"></i></button>
			</span>
			<img src="<?php echo base_url();?>assets/img/products/<?php echo $fPic[1];?>" alt="<?php echo $fPic[1];?>">
			<aside>
				<?php echo '<b>Producto:</b><br>'.$prod->nom_producto.'<br><b>Condición:</b> '.$prod->condicion.'<br><b>Precio: </b> $ '.$prod->precio.' MXN<br>';if($prod->video!='')echo '<b><i class="glyphicon glyphicon-picture"></i> Imágenes: '.(count($fPic)-1).'</b><br><b><i class="glyphicon glyphicon-facetime-video"></i> Tiene video</b>';?>
			</aside>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 vedTransaccion">
			<aside>
				<b>Status: </b> <?php echo $prod->estado;?><br>
				<b>No. guía: </b> <?php echo ($prod->guia!='')?$prod->guia:'POR DEFINIR'; echo '<br>'?>
				<b>Paquetería: </b> <?php echo ($prod->paqueteria)?$prod->paqueteria:'POR DEFINIR';?><br>
				<?php if($prod->id_comprador!=0){ 
					echo '<b>Comprador: </b> '.$prod->nombres.' '.$prod->apellidos.'<br>';
					echo '<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dirección <span class="caret"></span></button>
						<div class="dropdown-menu">
							<b>Calle:</b><br>'.$prod->calle.'<br><b>Colonia:</b><br>'.$prod->colonia.'<br><b>CP:</b><br>'.$prod->cp.'<br>
							<b>Municipio:</b><br>'.$prod->municipio.'<br><b>Estado:</b><br>'.$prod->nom_estado.'
						</div><div>';
				}?>
			</aside>
			<aside>
				<?php if($prod->estado=='DESHABILITADO'){?>
					<button id="<?php echo $prod->id_transaccion;?>" class="habilitar btn btn-success"><i class="glyphicon glyphicon-ok-circle"></i> Habilitar producto</button>
				<?php }else if($prod->estado=='APARTADO'){?>
					<button id="<?php echo $prod->id_transaccion;?>" class="paqueteria btn btn-primary"><i class="fa fa-truck running"></i> Hacer envío</button>
					<button id="<?php echo $prod->id_comprador.'-'.$prod->id_transaccion;?>" class="retirar cancel btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Cancelar venta</button>
				<?php	}else if($prod->estado=='VENTA'){?>
					<button id="<?php echo $prod->id_transaccion;?>" class="deshabilitar cancel btn btn-cancel"><i class="glyphicon glyphicon-ban-circle"></i> Deshabilitar producto</button>
				<?php } if($prod->id_comprador!=0){?>
					<button id="<?php echo $prod->id_comprador;?>" class="contact btn btn-default"><i class="glyphicon glyphicon-envelope"></i> Contactar comprador</button>	
				<?php }?>
				
			</aside>
			<div id="anyForm" class="anyForm"></div>
		</div>
		<div class="editFormCont"></div>
	</div>
<?php }	}else echo '<h3 style="text-align:center;">No tienes producto aún...</h3>';?>