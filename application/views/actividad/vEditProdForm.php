<h4>Edición del producto</h4>
	<form class="editProdForm" id="editProdForm" enctype="multipart/form-data" novalidate>
		<?php foreach ($perfil as $per) {$tienda = $per->tienda;} 
			foreach ($producto as $p) {?>
		<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
			<input type="hidden" id="id_producto" name="id_producto" value="<?php echo $p->id_producto;?>">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Nombre del producto" name="nom_producto" id="nom_producto" value="<?php echo $p->nom_producto;?>" required data-validation-required-message="Ingresa nombre de tu producto.">
				<p class="help-block text-danger"></p>
			</div>
			<div class="form-group">
				<textarea class="form-control" cols="30" rows="5" placeholder="Descripción del producto" name="descripcion" id="descripcion" required data-validation-required-message="Ingresa una descripción."><?php echo $p->descripcion;?></textarea>
				<p class="help-block text-danger"></p>
			</div>
			<div class="form-group half">
				<span class="sideText">$</span> <input style="width:70%;" type="number" class="form-control" placeholder="Precio" name="precio" id="precio" value="<?php echo $p->precio;?>" required data-validation-required-message="Ingresa el precio del producto."> <span class="sideText">MXN</span>
				<p class="help-block text-danger"></p>
			</div>
			<?php if($tienda==1){?>
				<div class="form-group half">
					<span class="sideText">Cantidad:</span> <input style="width:50%;" type="number" min="1" class="form-control" placeholder="Cant." name="existencia" id="existencia" required data-validation-required-message="Ingresa la cantidad.">
					<p class="help-block text-danger"></p>
				</div>
			<?php }?>
			<div class="form-group half">
				<?php if($tienda==1){ echo '<span class="sideText">Condición: </span>';}?>
				<select class="form-control" name="condicion" id="condicion">
					<option value="Nuevo">Nuevo</option>
					<option value="Seminuevo">Semiuevo</option>
					<option value="Usado">Usado</option>
				</select>
			</div>
			<div class="form-group half">
				<span class="sideText">Marca: </span><select name="id_marcas" id="id_marcas" class="form-control">
					<?php foreach ($marcas as $m) {
						$sel = ($m->id_marcas==$p->id_marcas)?' selected':'';
						echo '<option value="'.$m->id_marcas.'"'.$sel.'>'.$m->nom_marca.'</option>';
					}?>
				</select>
			</div>
			<div class="form-group half">
				<span class="sideText">Categoria: </span><select name="id_categoria" id="id_categoria" class="form-control">
					<?php foreach ($categorias as $cat) {
						$sel = ($cat->id_categoria==$p->id_categoria)?' selected':'';
						echo '<option value="'.$cat->id_categoria.'"'.$sel.'>'.$cat->nom_categoria.'</option>';
					}?>
				</select>
			</div>
			<div class="form-group half">
				<span class="sideText">Subcategoria: </span><select name="id_subcatego" id="id_subcatego" class="form-control">
					<?php foreach ($subcategorias as $sub) {
						$sel = ($sub->id_subcatego==$p->id_subcatego)?' selected':'';
						echo '<option value="'.$sub->id_subcatego.'"'.$sel.'>'.$sub->nom_subcatego.'</option>';
					}?>
				</select>
			</div>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
			<div class="oldImages">
				<h6 style="margin-top:0;">Imágenes originales (si eliminas una imagen, la puedes reinsertar abajo)</h6>
				<?php $oldImg = explode('/', $p->imagenes);
					for ($i=1; $i < count($oldImg); $i++) { ?>
						<figure>
							<img src="<?php echo base_url().'assets/img/products/'.$oldImg[$i];?>" alt="<?php echo $oldImg[$i];?>">
							<figcaption>
								<button class="preEliminarImg btn btn-wildcard">Eliminar <i class="glyphicon glyphicon-trash"></i></button>
							</figcaption>
						</figure>
				<?php }?>
			</div>
			<div id="dZUploadEdit" class="dropzone">
			    <div class="dz-default dz-message">
			    	<i class="glyphicon glyphicon-picture"></i> Arrastra la imagenes ó da click aquí
			    </div>
			</div>
			<input type="hidden" id="oldArchivo" name="oldArchivo" value="<?php echo $p->imagenes;?>">
			<input type="hidden" id="toBeRemoved" name="toBeRemoved">
			<input type="hidden" id="archivo" name="archivo">
			<br>
			<?php if($p->video!='')echo '<h6 class="oldVideo"><i class="glyphicon glyphicon-facetime-video"></i> Video original: '.$p->video.' <span class="floatRight"><span class="preRemoveOldVid">Quitar video X</span></span></h6>';?>
			<div class="dropzone" id="dZVideoEdit">
				<div class="dz-default dz-message">
			    	<i class="glyphicon glyphicon-facetime-video"></i> Arrastra un video o da click aquí (opcional)
			    </div>
			</div>
			<input type="hidden" id="oldVideo" name="oldVideo" value="<?php echo $p->video;?>">
			<input type="hidden" id="video" name="video">
			<?php }?>
			<div class="row botones">
				<button class="reset btn btn-default">Limpiar <i class="glyphicon glyphicon-erase"></i></button>
				<button class="btn btn-cancel">Cancelar <i class="glyphicon glyphicon-remove"></i></button>
				<button type="submit" class="btn btn-primary">Modificar <i class="glyphicon glyphicon-refresh"></i></button>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 text-center">
				<div id="successEditarProducto"></div>
			</div>
		</div>
	</form>

<script type="text/javascript">
	$("#editProdForm input,#editProdForm textarea").jqBootstrapValidation({
        preventSubmit: true,
        submitError: function($form, event, errors) {
            // additional error messages or events
        },
        submitSuccess: function($form, event) {
            event.preventDefault(); // prevent default submit behaviour
            // get values from FORM
            if($('.editProdForm #archivo').text()!='' || $('.editProdForm #oldArchivo').val()!=''){
	            var dta = $form.serialize()+'&archivo='+$('.editProdForm #archivo').text();
	            $.ajax({
	                url: "editarProducto",
	                type: "POST",
	                data:dta,
	                success: function(res){
	                	$('#successEditarProducto').html("<div class='alert alert-success'>");
	                    $('#successEditarProducto > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;").append("</button>");
	                    $('#successEditarProducto > .alert-success').append("<strong>Se modificó tu producto satisfactoriamente!<br>Cierra este cuadro para continuar...</strong>");
	                    $('#successEditarProducto > .alert-success').append('</div>');
	                  	//refreshProducts();
	                },
	                error: function(err){
	                  console.log(err);
	                  $('#successEditarProducto').html("<div class='alert alert-danger'>");
                      $('#successEditarProducto > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                          .append("</button>");
                      $('#successEditarProducto > .alert-danger').append("<strong>Lo sentimos, parece que no hay servicio. Por favor intenta mas tarde...");
                      $('#successEditarProducto > .alert-danger').append('</div>');
	                    //clear all fields
	                    $('#editDirForm').trigger("reset");
	                }
	            });
	        }else{
	            $('#successEditarProducto').html("<div class='alert alert-danger'>");
	            $('#successEditarProducto > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
	              .append("</button>");
	            $('#successEditarProducto > .alert-danger').append("<strong>Falta agregar al menos una foto o imagen al producto...");
	            $('#successEditarProducto > .alert-danger').append('</div>');
	          }
        },
        filter: function() {
            return $(this).is(":visible");
        },
    });

	Dropzone.autoDiscover = false;
		    $("#dZUploadEdit").dropzone({
		        url: "upload",
            acceptedFiles: "image/jpeg,image/png,image/jpg",
            tagId:'eImages',
            maxFiles:10,
		        addRemoveLinks: true,
		        removedfile: function(file){
		        	$('.editProdForm #archivo').text($('.editProdForm #archivo').text().replace('/'+file.name.replace(/\s/g,'_'),''));
		        	$.ajax({
			            url: "removeUploaded",
			            type: "POST",
			            data:{'img':file.name.replace(/\s/g,'_'),'dir':'./assets/img/products/'},
			            success: function(res){
			              	var _ref;
					        if (file.previewElement) {
					          if ((_ref = file.previewElement) != null) {
					            _ref.parentNode.removeChild(file.previewElement);
					          }
					        }
					        return this._updateMaxFilesReachedClass();
			            },
			            error: function(err){
			              console.log(err);
			            }
			        });
		        },
		        success: function (file, response) {
		            var imgName = response;
		            file.previewElement.classList.add("dz-success");
		            $('.editProdForm #archivo').text($('.editProdForm #archivo').text()+'/'+imgName.replace(/\s/g,'_'));
		        },
		        error: function (file, response) {
		            file.previewElement.classList.add("dz-error");
		        }
		    });

        $("#dZVideoEdit").dropzone({
            url: "upload",
            acceptedFiles: "video/mp4",
            maxFiles:1,
            tagId:'eVideo',
            dictRemoveFile: 'Quitar video X',
            addRemoveLinks: true,
            removedfile: function(file){
              $.ajax({
                  url: "removeUploaded",
                  type: "POST",
                  data:{'img':file.name.replace(/\s/g,'_'),'dir':'./assets/img/products/video/'},
                  success: function(res){
                      $('.editProdForm #video').val('');
                      var _ref;
                  if (file.previewElement) {
                    if ((_ref = file.previewElement) != null) {
                      _ref.parentNode.removeChild(file.previewElement);
                    }
                  }
                  return this._updateMaxFilesReachedClass();
                  },
                  error: function(err){
                    console.log(err);
                  }
              });
            },
            success: function (file, response) {
                var vidName = response;
                file.previewElement.classList.add("dz-success");
                var $vid = $('<video width="100%" height="auto" controls>');
                $vid.append('<source src="../../assets/img/products/video/'+vidName.replace(/\s/g,'_')+'" type="video/mp4">');
                $('#dZVideoEdit .dz-image').append($vid);
                $('.editProdForm #video').val(vidName.replace(/\s/g,'_'));
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
</script>