<div class="container-fluid actividadBoard">
	<h2><span class="slideRight"><i class="fa fa-dashboard"></i> General</span></h2>
	<div class="row">
		<?php foreach ($perfil as $p) {?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeUp">
				<figure class="generalFig">
					<h4>Reputación</h4>
					<img src="<?php echo base_url();?>assets/img/reputacion.png" alt="">
					<span class="userData">
						<b>Nombre:</b><br>
						<?php echo $p->nombres;?><br>
						<b>Apellidos:</b><br>
						<?php echo $p->apellidos;?><br>
						<b>Rango:</b><br>
						Soldado Razo
					</span>
					<aside>
						<h6>Puntaje: 1 pts<span class="floatRight"><?php echo $p->reputacion;?> / 10 pts</span></h6>
						<div class="progress">
						  <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
						    <span class="sr-only">60% Complete</span>
						  </div>
						</div>
					</aside>
				</figure>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeUp">
				<figure class="generalFig">
					<h4>Compras</h4>
					<i class="actIcon fa fa-shopping-cart"></i>
					<span class="userData">
						<b>Comprados:</b><br>
						15<br>
						<b>En camino:</b><br>
						5<br>
						<b>Cancelados:</b><br>
						1
					</span>
					<aside>
						<h6>Ratio<span class="floatRight">13 compras</span></h6>
						<div class="progress">
						  <div class="progress-bar progress-bar-success" style="width: 55%">
						    <span class="sr-only">35% Complete (success)</span>
						  </div>
						  <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 35%">
						    <span class="sr-only">20% Complete (warning)</span>
						  </div>
						  <div class="progress-bar progress-bar-danger" style="width: 10%">
						    <span class="sr-only">10% Complete (danger)</span>
						  </div>
						</div>
					</aside>
				</figure>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeUp">
				<figure class="generalFig">
					<h4>Ventas</h4>
					<i class="actIcon fa fa-archive"></i>
					<span class="userData">
						<b>Vendidos:</b><br>
						10<br>
						<b>En camino:</b><br>
						2<br>
						<b>Cancelados:</b><br>
						1
					</span>
					<aside>
						<h6>Ratio<span class="floatRight">13 ventas</span></h6>
						<div class="progress">
						  <div class="progress-bar progress-bar-success" style="width: 55%">
						    <span class="sr-only">35% Complete (success)</span>
						  </div>
						  <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 35%">
						    <span class="sr-only">20% Complete (warning)</span>
						  </div>
						  <div class="progress-bar progress-bar-danger" style="width: 10%">
						    <span class="sr-only">10% Complete (danger)</span>
						  </div>
						</div>
					</aside>
				</figure>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeUp">
				<figure class="generalFig">
					<h4>Subastas</h4>
					<i class="actIcon fa fa-gavel"></i>
					<span class="userData">
						<b>Subastados:</b><br>
						10<br>
						<b>En camino:</b><br>
						2<br>
						<b>Sin atender:</b><br>
						1
					</span>
					<aside>
						<h6>Ratio<span class="floatRight">13 subastas</span></h6>
						<div class="progress">
						  <div class="progress-bar progress-bar-success" style="width: 55%">
						    <span class="sr-only">35% Complete (success)</span>
						  </div>
						  <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 35%">
						    <span class="sr-only">20% Complete (warning)</span>
						  </div>
						  <div class="progress-bar progress-bar-danger" style="width: 10%">
						    <span class="sr-only">10% Complete (danger)</span>
						  </div>
						</div>
					</aside>
				</figure>
			</div>

		<?php }?>
	</div>
	<h2><span class="slideRight"><i class="fa fa-clock-o"></i> Historial reciente</span></h2>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 historial">
			<figure class="slideLeft"><i class="fa fa-shopping-cart"></i> Compraste producto: <b>Marcadora Tippman Sierra One</b> <span class="floatRight"><i class="glyphicon glyphicon-calendar"></i> 2016-05-23</span></figure>
			<figure class="slideLeft"><i class="fa fa-shopping-cart"></i> Compraste producto: <b>Coderas Condor Tan Medianas</b> <span class="floatRight"><i class="glyphicon glyphicon-calendar"></i> 2016-05-18</span></figure>
			<figure class="slideLeft"><i class="fa fa-archive"></i> Vendiste producto: <b>Careta Sly ACU</b> <span class="floatRight"><i class="glyphicon glyphicon-calendar"></i> 2016-05-01</span></figure>
			<figure class="slideLeft"><i class="fa fa-gavel"></i> Subastaste producto: <b>Uniforme ACU Talla M</b> <span class="floatRight"><i class="glyphicon glyphicon-calendar"></i> 2016-03-18</span></figure>
			<figure class="slideLeft"><i class="fa fa-shopping-cart"></i> Compraste producto: <b>Caja de pintura Draxxus Frostbite</b> <span class="floatRight"><i class="glyphicon glyphicon-calendar"></i> 2016-01-23</span></figure>
		</div>
	</div>
</div>