<div class="container-fluid actividadBoard">
	<div class="row">
	<div class="container-fluid">
	<h2><span class="slideRight"><i class="fa fa-shopping-basket"></i> Tu tienda</span></h2>
	<?php if(!$tienda){?>
		<button class="abrirTiendaForm btn btn-xl">crear tienda <i class="fa fa-shopping-basket"></i></button>
		<form class="tiendaForm" id="tiendaForm" enctype="multipart/form-data" novalidate>
			<input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $perfil[0]->id_usuario;?>">
			<div class="col-lg-4 col-md-4">
				<div id="dZUploadLogo" class="dropzone">
				    <div class="dz-default dz-message">
				    	<i class="glyphicon glyphicon-picture"></i> Logotipo (arrastra la imagen ó da click aquí)
				    </div>
				</div>
				<input type="hidden" id="logo" name="logo">
			</div>
			<div class="col-lg-8 col-md-8">
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Nombre de la tienda" name="nom_tienda" id="nom_tienda" required data-validation-required-message="Ingresa el nombre.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Enlace Facebook http:// (opcional)" name="facebook" id="facebook">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Calle, número, colonia" name="domicilio" id="domicilio" required data-validation-required-message="Ingresa el domicilio.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Municipio" name="municipio" id="municipio" required data-validation-required-message="Ingresa el municipio.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<select name="id_estado" id="id_estado" class="form-control">
						<?php foreach ($estados as $e) {?>
							<option value="<?php echo $e->id_estado?>"><?php echo $e->nom_estado;?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group half">
					<input type="number" class="form-control" id="telefono" name="telefono" placeholder="Teléfono de la tienda">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" id="url" name="url" placeholder="Enlace página de internet http:// (opcional)">
					<p class="help-block text-danger"></p>
				</div>

				<div class="botones">
					<button class="reset btn btn-default">Limpiar <i class="glyphicon glyphicon-erase"></i></button>
					<button class="btn btn-cancel">Cancelar <i class="glyphicon glyphicon-remove"></i></button>
					<button type="submit" class="btn btn-primary">Crear <i class="fa fa-shopping-basket"></i></button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-center">
					<div id="successTienda"></div>
				</div>
			</div>
		</form>
	<?php }else{ foreach ($tienda as $t) {?>
		<div id="<?php echo $perfil[0]->tienda;?>" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tiendaData">
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
				<img src="<?php echo base_url().'assets/img/tiendas/'.$t->logo?>" alt="<?php echo $t->logo;?>">
			</div>
			<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
				<h4><?php echo $t->nom_tienda;?></h4>
				<b>Domicilio: </b><?php echo $t->domicilio;?><br>
				<b>Municipio: </b><?php echo $t->municipio;?><br>
				<b>Estado: </b><?php echo $t->nom_estado;?><br>
				<b><i class="glyphicon glyphicon-phone"></i> Teléfono: </b><?php echo $t->telefono;?><br>
				<?php if($t->url!=''){?>
					<b><i class="fa fa-globe"></i> Sitio WEB:</b> <a href="<?php echo $t->url;?>" target="_blank"> <?php echo $t->url;?></a><br>
				<?php }?>
				<?php if($t->facebook!=''){?>
					<b><i class="fa fa-facebook-square"></i> Facebook:</b> <a href="<?php echo $t->facebook;?>" target="_blank"> <?php echo $t->facebook;?></a><br>
				<?php }?>
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 tiendaAjustes">
				<h4>Ajustes <i class="glyphicon glyphicon-cog"></i></h4>
				<button class="editTienda btn btn-wildcard">Editar tienda <i class="glyphicon glyphicon-pencil"></i></button>
				<button class="changePlan btn btn-primary">Cambiar plan <i class="glyphicon glyphicon-transfer"></i></button>
				
				<aside class="text-center">
					<h5>Plan actual</h5>
					<?php $plan = 'Cabo'; $ico = 2; if($perfil[0]->tienda==200){$plan = 'Sargento 2do';$ico=4;}else if($perfil[0]->tienda==450){ $plan = 'Teniente';$ico=6;}?>
					<img src="<?php echo base_url().'assets/img/ranks/'.$ico.'.png';?>" alt="<?php echo $ico.'.png';?>">
					<b><?php echo $plan;?></b>
				</aside>
				<div id="anyForm" class="anyForm"></div>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<form class="editTiendaForm" id="editTiendaForm" enctype="multipart/form-data" novalidate>
			<h3><i class="glyphicon glyphicon-pencil"></i> Edición de la tienda</h3>
			<input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $t->id_usuario;?>">
			<div class="col-lg-4 col-md-4">
				<h5>Logo actual (si lo eliminas puedes volver a insertarlo abajo)</h5>
				<figure>
					<img class="oldImg" src="<?php echo base_url().'assets/img/tiendas'.$t->logo;?>" alt="<?php echo str_replace('/', '', $t->logo);?>">
					<figcaption class="text-center"><?php echo str_replace('/', '', $t->logo);?><button class="deleteLogo btn btn-wildcard"><i class="glyphicon glyphicon-trash"></i> Eliminar</button></figcaption>
				</figure>
				<div id="dZUploadLogo" class="dropzone">
				    <div class="dz-default dz-message">
				    	<i class="glyphicon glyphicon-picture"></i> Logotipo (arrastra la imagen ó da click aquí)
				    </div>
				</div>
				<input type="hidden" id="logo" name="logo">
				<input type="hidden" id="oldLogo" name="oldLogo" value="<?php echo $t->logo;?>">
			</div>
			<div class="col-lg-8 col-md-8">
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Nombre de la tienda" name="nom_tienda" id="nom_tienda" value="<?php echo $t->nom_tienda;?>" required data-validation-required-message="Ingresa el nombre.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Enlace Facebook http:// (opcional)" name="facebook" id="facebook" value="<?php echo $t->facebook;?>">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Calle, número, colonia" name="domicilio" id="domicilio" value="<?php echo $t->domicilio;?>" required data-validation-required-message="Ingresa el domicilio.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" placeholder="Municipio" name="municipio" id="municipio" value="<?php echo $t->municipio;?>" required data-validation-required-message="Ingresa el municipio.">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<select name="id_estado" id="id_estado" class="form-control">
						<?php foreach ($estados as $e) {$s = ($e->id_estado==$t->id_estado)?' selected':'';?>
							<option value="<?php echo $e->id_estado?>"<?php echo $s;?>><?php echo $e->nom_estado;?></option>
						<?php }?>
					</select>
				</div>
				<div class="form-group half">
					<input type="number" class="form-control" id="telefono" name="telefono" placeholder="Teléfono de la tienda" value="<?php echo $t->telefono;?>">
					<p class="help-block text-danger"></p>
				</div>
				<div class="form-group half">
					<input type="text" class="form-control" id="url" name="url" placeholder="Enlace página de internet http:// (opcional)" value="<?php echo $t->url;?>">
					<p class="help-block text-danger"></p>
				</div>

				<div class="botones">
					<button class="reset btn btn-default">Limpiar <i class="glyphicon glyphicon-erase"></i></button>
					<button class="btn btn-cancel">Cancelar <i class="glyphicon glyphicon-remove"></i></button>
					<button type="submit" class="btn btn-primary">Editar <i class="glyphicon glyphicon-pencil"></i></button>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 text-center">
					<div id="successEditTienda"></div>
				</div>
			</div>
		</form>
		</div>
	<?php } }?>
		</div>
	</div>
	<div class="row">
		<div class="container-fluid">
			<h2><span class="slideRight"><i class="glyphicon glyphicon-th-list"></i> Inventario</span></h2>
			<?php foreach ($productos as $p) { $pic = explode('/', $p->imagenes);?>
				<figure class="inventario row">
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
						<img src="<?php echo base_url().'assets/img/products/thumbs/'.$pic[1];?>" alt="">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<h4><?php echo $p->nom_producto;?></h4>
						<b>Estado: </b> <?php echo $p->estado;?><br>
						<b>Total: </b> $ <?php echo $p->total;?> MXN<br>
						<b>Vendidos: </b> <?php echo $p->vendidos;?><br>
						<b>Cancelados: </b> <?php echo $p->cancelados;?><br>
						<b>En camino: </b> <?php echo $p->camino;?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 controls">
						<h5>En existencia: <b><?php echo $p->existencia;?></b></h5>
						<button id="<?php echo $p->id_tienda.'-'.$p->id_producto;?>" class="btn btn-primary">Cambiar cantidad</button>
						<?php if($p->estado=='VENTA'){?>
							<button id="<?php echo $p->id_transaccion;?>" class="btn btn-cancel">Ocultar producto <i class="glyphicon glyphicon-remove"></i></button>
						<?php }else if($p->estado=='DESHABILITADO'){?>
							<button id="<?php echo $p->id_transaccion;?>" class="btn btn-success">Mostrar producto <i class="glyphicon glyphicon-refresh"></i></button>
						<?php }?>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 qty"></div>
						
				</figure>
			<?php }?>
		</div>
	</div>
</div>

<script>
	tienda.main.prepararFormulario();
	tienda.main.abrirTiendaForm();
	tienda.main.cancelarFormularioTienda();
	tienda.main.limpiarFormularioTienda();
	tienda.main.procesarTienda();
	tienda.main.refreshTienda();
	tienda.main.abrirEditTiendaForm();
	tienda.main.preEliminarImagenEditar();
	tienda.main.eliminarImagenVieja();
	tienda.main.procesarEditTienda();
	tienda.main.cambiarEstadoProducto();
	tienda.main.abrirExistenciaForm();
	tienda.main.cambiarExistencia();
	tienda.main.changePlan();
	tienda.main.cancelarChangePlan();
	tienda.main.enviarSolicitudPlan();
</script>