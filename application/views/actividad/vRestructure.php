<div class="container-fluid restructBoard">
	<div class="container">
	<h2 class="text-center glow">Ajustes en tu tablero</h2>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 normalPackage">
			<aside>
				<img src="<?php echo base_url().'assets/img/packagenormal.png'?>" alt="" class="moveNormal">
			</aside>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 cogsLoading">
			<span class="cogCenter">
				<i class="fa fa-cog fa-spin fa-5x fa-fw"></i>
				<i class="fa fa-cog fa-spin fa-5x fa-fw"></i>
				<i class="fa fa-cog fa-spin fa-5x fa-fw"></i>
			</span>
			<span class="sr-only">Loading...</span>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 newPackage">
			<aside>
				<img src="<?php echo base_url().'assets/img/packagenuevo.png'?>" alt="" class="moveNew">
			</aside>
		</div>
	</div>
	<h3 class="text-muted text-center">Si tenias productos agregados antes de solicitar un PLAN DE TIENDA, una vez creada tu tienda, los productos serán siendo reacomodados...</h3>
	</div>
</div>