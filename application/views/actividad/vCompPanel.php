<div class="container-fluid actividadBoard">
<?php foreach ($perfil as $p) {
	echo '<input type="hidden" name="myMail" id="myMail" value="'.$p->correo.'">';
	echo '<input type="hidden" name="myWhats" id="myWhats" value="'.$p->whatsapp.'">';
}?>
	<h2><span class="slideRight"><i class="fa fa-shopping-cart"></i> Tus pedidos</span></h2>
	<div class="row pedido fadeUp">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedProducto">
			<img src="<?php echo base_url();?>assets/img/reputacion.png" alt="">
			<aside>
				<b>Producto:</b><br>
				Marcadora Tippmann Sierra One Black<br>
				<b>Precio:</b><br>
				$ 2500 MXN<br>
				<b>Condición:</b><br>
				Nueva
			</aside>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedVendedor">
			<aside>
				<b>Vendedor:</b> Paintball Evolution<br>
				<b>Status:</b> En camino <i class="fa fa-truck running"></i><br>
				<b>No. guía:</b> 2163263514684654<br>
				<b>Paquetería:</b> Fedex
			</aside>
			<aside>
				<button id="correo@dd.com_4578456" class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar pedido</button>
				<button id="correo@dd.com_4578456" class="contact btn btn-default"><i class="glyphicon glyphicon-envelope"></i> Contactar vendedor</button>
			</aside>
			<div class="anyForm"></div>
		</div>
	</div>
	<div class="row pedido fadeUp">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedProducto">
			<img src="<?php echo base_url();?>assets/img/reputacion.png" alt="">
			<aside>
				<b>Producto:</b><br>
				Marcadora Tippmann Sierra One Black<br>
				<b>Precio:</b><br>
				$ 2500 MXN<br>
				<b>Condición:</b><br>
				Nueva
			</aside>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedVendedor">
			<aside>
				<b>Vendedor:</b> Paintball Evolution<br>
				<b>Status:</b> En camino <i class="fa fa-truck running"></i><br>
				<b>No. guía:</b> 2163263514684654<br>
				<b>Paquetería:</b> Fedex
			</aside>
			<aside>
				<button id="correo@dd.com_4578456" class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar pedido</button>
				<button id="correo@dd.com_4578456" class="contact btn btn-default"><i class="glyphicon glyphicon-envelope"></i> Contactar vendedor</button>
			</aside>
			<div class="anyForm"></div>
		</div>
	</div>
	<div class="row pedido fadeUp">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedProducto">
			<img src="<?php echo base_url();?>assets/img/reputacion.png" alt="">
			<aside>
				<b>Producto:</b><br>
				Marcadora Tippmann Sierra One Black<br>
				<b>Precio:</b><br>
				$ 2500 MXN<br>
				<b>Condición:</b><br>
				Nueva
			</aside>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedVendedor">
			<aside>
				<b>Vendedor:</b> Paintball Evolution<br>
				<b>Status:</b> En camino <i class="fa fa-truck running"></i><br>
				<b>No. guía:</b> 2163263514684654<br>
				<b>Paquetería:</b> Fedex
			</aside>
			<aside>
				<button id="correo@dd.com_4578456" class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar pedido</button>
				<button id="correo@dd.com_4578456" class="contact btn btn-default"><i class="glyphicon glyphicon-envelope"></i> Contactar vendedor</button>
			</aside>
			<div class="anyForm"></div>
		</div>
	</div>
	<div class="row pedido fadeUp">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedProducto">
			<img src="<?php echo base_url();?>assets/img/reputacion.png" alt="">
			<aside>
				<b>Producto:</b><br>
				Marcadora Tippmann Sierra One Black<br>
				<b>Precio:</b><br>
				$ 2500 MXN<br>
				<b>Condición:</b><br>
				Nueva
			</aside>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 pedVendedor">
			<aside>
				<b>Vendedor:</b> Paintball Evolution<br>
				<b>Status:</b> En camino <i class="fa fa-truck running"></i><br>
				<b>No. guía:</b> 2163263514684654<br>
				<b>Paquetería:</b> Fedex
			</aside>
			<aside>
				<button id="correo@dd.com_4578456" class="cancel btn btn-cancel"><i class="glyphicon glyphicon-remove"></i> Cancelar pedido</button>
				<button id="correo@dd.com_4578456" class="contact btn btn-default"><i class="glyphicon glyphicon-envelope"></i> Contactar vendedor</button>
			</aside>
			<div class="anyForm"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 moreCompras">Mostrar más</div>
	</div>
</div>

<script>
	compras.main.mostrarFormularioContacto();
	compras.main.mostrarFormularioCancelar();
	compras.main.cancelarFormulario();
</script>